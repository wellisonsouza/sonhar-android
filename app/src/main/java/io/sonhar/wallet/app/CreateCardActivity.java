package io.sonhar.wallet.app;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import io.sonhar.wallet.R;
import io.sonhar.wallet.model.ResultServer;
import io.sonhar.wallet.model.User;
import io.sonhar.wallet.util.Action;
import io.sonhar.wallet.util.ActionTask;
import io.sonhar.wallet.ws.RestServices;

public class CreateCardActivity extends AppCompatActivity {
    EditText edtNumCard;
    TextView lblNumCard;
    Button btnCadastrar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_card);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        edtNumCard = (EditText) findViewById(R.id.edtNumCard);
        lblNumCard = (TextView) findViewById(R.id.lblNumCard);
        btnCadastrar = (Button) findViewById(R.id.btnCadastrar);

        edtNumCard.addTextChangedListener(new CreditCardNumberFormattingTextWatcher());
        edtNumCard.setOnEditorActionListener(new DoneOnEditorActionListener());

        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                saveCardAction.param = edtNumCard.getText().toString().replaceAll("\\s+","");

                ActionTask actionCardsTask = new ActionTask(CreateCardActivity.this, saveCardAction, "Verificando cartão, aguarde..");
                actionCardsTask.execute();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return (super.onOptionsItemSelected(menuItem));
    }


    /**
     * Formatting a credit card number: #### #### #### #######
     */
    class CreditCardNumberFormattingTextWatcher implements TextWatcher {

        private boolean lock;

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            lblNumCard.setText(s.toString());
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (lock || s.length() > 16) {
                return;
            }
            lock = true;
            for (int i = 4; i < s.length(); i += 5) {
                if (s.toString().charAt(i) != ' ') {
                    s.insert(i, " ");
                }

            }
            lock = false;


        }
    }

    class DoneOnEditorActionListener implements TextView.OnEditorActionListener {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                InputMethodManager imm = (InputMethodManager)v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                return true;
            }
            return false;
        }
    }

    Action saveCardAction = new Action() {
        ResultServer result = new ResultServer();
        @Override
        public void onPostExecute() {
            // TODO Auto-generated method stub
            Log.i("RETORNO", "OK");
            try {

                if(result.getSuccess()) {
                    Intent it = new Intent(getApplicationContext(),MainActivity_old.class);
                    it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(it);
                    Toast.makeText(getApplicationContext(),result.getMessage(),Toast.LENGTH_SHORT).show();
                }else
                    Toast.makeText(getApplicationContext(),result.getMessage(),Toast.LENGTH_SHORT).show();

            }catch(Exception ex){
                ex.printStackTrace();
            }

        }
        @Override
        public void run () {
            try {

                RestServices rs = new RestServices();
                User userLogged = App.getUser(getApplicationContext());
                result = rs.saveCard(param,userLogged.getId().toString());


            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };
}

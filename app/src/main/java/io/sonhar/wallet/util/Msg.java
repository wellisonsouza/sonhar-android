package io.sonhar.wallet.util;

/**
 * Created by wellison on 30/01/2018.
 */

public class Msg {
    public static final String ERROR_DEFAULT = "Ocorreu um erro, tente mais tarde.";
    public static final String ERROR_NETWORK= "Não foi possível se comunicar, verifique sua conexão.";
    public static final String USER_DENIED= "Sua sessão foi expirada, entre novamente.";

}

package io.sonhar.wallet.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import io.sonhar.wallet.R;
import io.sonhar.wallet.app.App;
import io.sonhar.wallet.app.FaceValidationActivity;
import io.sonhar.wallet.app.LoginActivity;
import io.sonhar.wallet.app.MainActivity;
import io.sonhar.wallet.app.MainActivity_old;
import io.sonhar.wallet.model.User;
import io.sonhar.wallet.util.Action;
import io.sonhar.wallet.util.ActionTask;
import io.sonhar.wallet.ws.RestServices;

/**
 * Created by PedroPontes1 on 18/06/2018.
 */

public class ProfileFragment extends Fragment implements Fragmento{

    MainActivity main;

    TextView lblNome;
    TextView lblEmail;
    Button btnLogout;
    LinearLayout btnRegFaceRecognize;
    AlertDialog alerta;
    public ProfileFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        lblNome = (TextView) view.findViewById(R.id.lblNome);
        lblEmail = (TextView) view.findViewById(R.id.lblEmail);
        btnLogout = (Button) view.findViewById(R.id.btnLogout);
        btnRegFaceRecognize = (LinearLayout) view.findViewById(R.id.btnRegFaceRecognize);

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("ATENÇÃO");
                builder.setMessage("Você deseja sair?");

                builder.setPositiveButton("SIM", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        alerta.dismiss();
                        App.logout(getActivity().getApplicationContext());
                        Intent intent = new Intent(getActivity().getApplicationContext(),LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        getActivity().finish();
                    }
                });
                builder.setNegativeButton("NÃO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        alerta.dismiss();
                    }
                });
                alerta = builder.create();
                alerta.show();
            }
        });

        btnRegFaceRecognize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("ATENÇÃO");
                builder.setMessage(" - Posicione o o smartphone a sua frente.\n - Certifique-se que esta em um lugar bem iluminado.\n - Não utilize Oculos ou qualquer acessório no rosto.");

                builder.setPositiveButton("CONTINUAR", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        alerta.dismiss();
                        MainActivity_old.type_faceCamera = 0;
                        Intent pickFaceValidation = new Intent(getActivity().getApplicationContext(),FaceValidationActivity.class);
                        startActivityForResult(pickFaceValidation, 17);
                    }
                });
                builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        alerta.dismiss();
                    }
                });
                alerta = builder.create();
                alerta.show();
            }
        });

        return view;
    }

    @Override
    public void reload(Context c) {
        ActionTask actionCardsTask = new ActionTask(c, userDataAction, "Aguarde..");
        userDataAction.param = App.getUser(c).getId().toString();
        actionCardsTask.execute();
    }
    @Override
    public void setMainActivity(MainActivity main) {
        this.main = main;
    }

    Action userDataAction = new Action() {
        User user;
        @Override
        public void onPostExecute() {
            // TODO Auto-generated method stub
            Log.i("RETORNO", "OK");
            try {

                lblNome.setText(user.getNome());
                lblEmail.setText(user.getEmail());

            }catch(Exception ex){
                ex.printStackTrace();
            }

        }
        @Override
        public void run () {
            try {

                RestServices rs = new RestServices();

                user = rs.getUserData(param);


            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };

    @Override
    public String getTittle() {
        return getActivity().getApplicationContext().getResources().getString(R.string.cartoes_txt);
    }
}

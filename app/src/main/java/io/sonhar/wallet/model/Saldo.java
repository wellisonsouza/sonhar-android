package io.sonhar.wallet.model;

/**
 * Created by wellison on 11/07/2018.
 */

public class Saldo {
    private int valor_disponivel_centavos;
    private int valor_receber_centavos;
    private int valor_bloqueado_centavos;

    public int getValor_disponivel_centavos() {
        return valor_disponivel_centavos;
    }

    public void setValor_disponivel_centavos(int valor_disponivel_centavos) {
        this.valor_disponivel_centavos = valor_disponivel_centavos;
    }

    public int getValor_receber_centavos() {
        return valor_receber_centavos;
    }

    public void setValor_receber_centavos(int valor_receber_centavos) {
        this.valor_receber_centavos = valor_receber_centavos;
    }

    public int getValor_bloqueado_centavos() {
        return valor_bloqueado_centavos;
    }

    public void setValor_bloqueado_centavos(int valor_bloqueado_centavos) {
        this.valor_bloqueado_centavos = valor_bloqueado_centavos;
    }

    public Double getValorEmReais(int centavos){
        return (double) centavos/100;
    }
}

package io.sonhar.wallet.util;

/**
 * Created by PedroPontes1 on 18/06/2018.
 */

public class ExtratoLinha {
    private String data;
    private String hora;
    private String statusTransacao;
    private String estabelecimento;
    private String valor;
    private String transacaoAceita;


    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getStatusTransacao() {
        return statusTransacao;
    }

    public void setStatusTransacao(String statusTransacao) {
        this.statusTransacao = statusTransacao;
    }

    public String getEstabelecimento() {
        return estabelecimento;
    }

    public void setEstabelecimento(String estabelecimento) {
        this.estabelecimento = estabelecimento;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getTransacaoAceita() {
        return transacaoAceita;
    }

    public void setTransacaoAceita(String transacaoAceita) {
        this.transacaoAceita = transacaoAceita;
    }
}

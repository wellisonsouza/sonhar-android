package io.sonhar.wallet.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.zxing.WriterException;

import io.sonhar.wallet.R;
import io.sonhar.wallet.app.App;
import io.sonhar.wallet.app.MainActivity;
import io.sonhar.wallet.app.TransferenciaBancariaActivity;
import io.sonhar.wallet.app.TransferenciaWalletActivity;
import io.sonhar.wallet.model.User;
import io.sonhar.wallet.util.Utils;

/**
 * Created by PedroPontes1 on 18/06/2018.
 */

public class TransferFragment extends Fragment implements Fragmento{


    MainActivity main;
    public Context mCtxActivity;
    LinearLayout btnTransferir, btnReceber;
    public TransferFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_transfer, container, false);

        btnTransferir = view.findViewById(R.id.btnTransferir);
        btnReceber = view.findViewById(R.id.btnReceber);
        btnTransferir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showOptionsDialog();
            }
        });
        btnReceber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exibirQrCodeParaReceberTransferencia();
            }
        });

        return view;
    }

    @Override
    public void reload(Context c) {

    }
    @Override
    public void setMainActivity(MainActivity main) {
        this.main = main;
    }


    private void exibirQrCodeParaReceberTransferencia(){
        User userlogged = App.getUser(getActivity().getApplicationContext());
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_receive_money);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.FILL_PARENT;
        dialog.getWindow().setAttributes((WindowManager.LayoutParams) params);

        Button btnCancel = (Button)dialog.findViewById(R.id.btnOk);

        final ImageView imgQrCode = (ImageView)dialog.findViewById(R.id.imgQrCode);
        Bitmap qrcode= null;
        try {
            qrcode = Utils.qrcodeAsBitmap("C:" + userlogged.getCurrent_conta_id(), 256);
        } catch (WriterException e) {
            e.printStackTrace();
        }
        imgQrCode.setImageBitmap(qrcode);

        btnCancel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void showOptionsDialog(){
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_menu_transfers);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes((WindowManager.LayoutParams) params);

        Button btnContaBancaria = (Button)dialog.findViewById(R.id.btnContaBancaria);
        Button btnContaWallet = (Button)dialog.findViewById(R.id.btnContaWallet);

        btnContaBancaria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(getActivity(), TransferenciaBancariaActivity.class);
                startActivity(myIntent);
                dialog.dismiss();
            }
        });

        btnContaWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(getActivity(), TransferenciaWalletActivity.class);
                startActivity(myIntent);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public String getTittle() {
        return getActivity().getApplicationContext().getResources().getString(R.string.transferencia_txt);
    }


}

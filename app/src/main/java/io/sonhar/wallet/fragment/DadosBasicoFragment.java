package io.sonhar.wallet.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import io.sonhar.wallet.R;

/**
 * Created by wellison on 03/08/2018.
 */

public class DadosBasicoFragment extends Fragment {
    private Context context;
    private View view;

    public static DadosBasicoFragment newInstance(){
        DadosBasicoFragment fragment = new DadosBasicoFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        this.context =context;

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup parent, Bundle savedState){
        view =inflater.inflate(R.layout.fragment_dados_basicos, parent, false);
        return view;
    }
}

package io.sonhar.wallet.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import io.sonhar.wallet.R;
import io.sonhar.wallet.model.Card;

/**
 * Created by PedroPontes1 on 17/06/2018.
 */

public class CardRowListAdapter extends ArrayAdapter<Card> {
    private List<Card> cards;
    private Context mCtx;

    public CardRowListAdapter(Context ctx, List<Card> cards) {
        super(ctx, R.layout.row_card);
        this.mCtx = ctx;
        this.cards = cards;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        view = LayoutInflater.from(mCtx).inflate(R.layout.row_card, parent, false);
        //view.setLayoutParams(new Carousel.LayoutParams(70, 70));

        ViewHolder holder = new ViewHolder();
        holder.lblNumCard = (TextView) view.findViewById(R.id.lblNumCard);
        holder.lblHolder = (TextView) view.findViewById(R.id.lblHolder);
        view.setTag(holder);
        final Card card = cards.get(position);

        Editable s = new SpannableStringBuilder(card.getNumero_cartao());
        for (int i = 4; i < s.length(); i += 5) {
            if (s.toString().charAt(i) != ' ') {
                s.insert(i, " ");
            }

        }

        holder.lblNumCard.setText(s.toString());
        holder.lblHolder.setText(card.getNome_embossing());

        return view;
    }


    private class ViewHolder {
        TextView lblNumCard;
        TextView lblHolder;
    }

    @Override
    public Card getItem(int position) {
        return cards.get(position);
    }

    @Override
    public int getCount() {
        return cards.size();
    }

}


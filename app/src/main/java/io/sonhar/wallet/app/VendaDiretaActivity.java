package io.sonhar.wallet.app;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.ArrayList;
import java.util.List;

import io.sonhar.wallet.R;
import io.sonhar.wallet.model.ParcelaCartao;
import io.sonhar.wallet.model.ResponseServer;
import io.sonhar.wallet.model.ResultServer;
import io.sonhar.wallet.model.VendaDireta;
import io.sonhar.wallet.util.Action;
import io.sonhar.wallet.util.ActionTask;
import io.sonhar.wallet.util.ErrorHelper;
import io.sonhar.wallet.util.MaskEdit;
import io.sonhar.wallet.util.MoneyTextWatcher;
import io.sonhar.wallet.util.SessionHelper;
import io.sonhar.wallet.util.Utils;
import io.sonhar.wallet.ws.RestServices;
import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;


public class VendaDiretaActivity extends AppCompatActivity implements Validator.ValidationListener {

    public static int MY_SCAN_REQUEST_CODE = 200;

    Validator validator;
    @NotEmpty(messageResId = R.string.campo_obrigatorio)
    EditText edtValor, edtNomePagador,edtCpf, edtDescricaoVenda;
    @NotEmpty(messageResId = R.string.campo_obrigatorio)
    EditText edtNUmCartao, edtCvv;
    @NotEmpty(messageResId = R.string.campo_obrigatorio)
    @Length(min = 7, max = 7, trim = false, message = "Format invalid")
    EditText edtValidade;

    @NotEmpty(messageResId = R.string.campo_obrigatorio)
    @Length(min = 14, max = 14, trim = false, message = "Phone invalid")
    EditText edtTelefone;

    @Email()
    EditText edtEmail;

    Button btnCadastrar;
    ArrayList<ParcelaCartao> parcelaCartaos;
    private String[] arrayParcelas;

    MaterialSpinner cbbParcelas;
    VendaDireta vendaDireta;
    AlertDialog alerta;
    private String conta_token;
    Dialog dialogSms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venda_direta);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        validator = new Validator(this);
        validator.setValidationListener(this);
        edtValor = (EditText)findViewById(R.id.edtValor);
        edtNomePagador = findViewById(R.id.edtNomePagador);
        edtCpf = findViewById(R.id.edtCpf);
        edtTelefone = findViewById(R.id.edtTelefone);
        edtNUmCartao = findViewById(R.id.edtNumCartao);
        edtValidade = findViewById(R.id.edtValidade);
        edtEmail = findViewById(R.id.edtEmail);
        edtCvv = findViewById(R.id.edtCvv);
        edtDescricaoVenda = findViewById(R.id.edtDescricaoVenda);
        btnCadastrar = findViewById(R.id.btnCadastrar);
        cbbParcelas = findViewById(R.id.cbbParcelas);

        edtValor.addTextChangedListener(new MoneyTextWatcher(edtValor));
        edtCpf.addTextChangedListener(MaskEdit.mask(edtCpf, MaskEdit.FORMAT_CPF));
        edtTelefone.addTextChangedListener(MaskEdit.mask(edtTelefone, MaskEdit.FORMAT_FONE));
        edtNUmCartao.addTextChangedListener(MaskEdit.mask(edtNUmCartao, MaskEdit.FORMAT_CARD));
        edtNUmCartao.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            public void onFocusChange(View v, boolean hasFocus) {
                String valorStr = edtValor.getText().toString().replaceAll("[.]", "");
                if(!hasFocus && !valorStr.isEmpty()) {
                    ActionTask actionParcelas = new ActionTask(VendaDiretaActivity.this, getParcelasCartao, getResources().getString(R.string.aguarde_txt));
                    actionParcelas.execute();
                }
            }
        });

        edtValor.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus && !edtNUmCartao.getText().toString().trim().isEmpty()) {
                    ActionTask actionParcelas = new ActionTask(VendaDiretaActivity.this, getParcelasCartao, getResources().getString(R.string.aguarde_txt));
                    actionParcelas.execute();
                }
            }
        });
        edtValidade.addTextChangedListener(MaskEdit.mask(edtValidade, MaskEdit.EXPIRATION_DATE));
        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validator.validate();
            }
        });

        ActionTask actionContaToken = new ActionTask(VendaDiretaActivity.this, getContaToken, getResources().getString(R.string.aguarde_txt));
        actionContaToken.execute();
    }

    public void onScanPress(View v) {
        Intent scanIntent = new Intent(this, CardIOActivity.class);
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true);
        scanIntent.putExtra(CardIOActivity.EXTRA_SCAN_EXPIRY, false);
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, true);
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CARDHOLDER_NAME, false);

        scanIntent.putExtra(CardIOActivity.EXTRA_SUPPRESS_MANUAL_ENTRY, true);
        scanIntent.putExtra(CardIOActivity.EXTRA_GUIDE_COLOR, getResources().getColor(R.color.colorPrimary));
        scanIntent.putExtra(CardIOActivity.EXTRA_HIDE_CARDIO_LOGO, true);
        scanIntent.putExtra(CardIOActivity.EXTRA_RETURN_CARD_IMAGE, false);
        scanIntent.putExtra(CardIOActivity.EXTRA_SUPPRESS_CONFIRMATION,false);
        scanIntent.putExtra(CardIOActivity.EXTRA_USE_PAYPAL_ACTIONBAR_ICON, false);
        startActivityForResult(scanIntent, MY_SCAN_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MY_SCAN_REQUEST_CODE) {
            String resultDisplayStr;
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);
                edtNUmCartao.setText(scanResult.getFormattedCardNumber());
                resultDisplayStr = "Card Number: " + scanResult.getRedactedCardNumber() + "\n";
                if (scanResult.isExpiryValid()) {
                    if (scanResult.expiryMonth < 10)
                        edtValidade.setText("0" + scanResult.expiryMonth + "/" + scanResult.expiryYear);
                    else
                        edtValidade.setText(scanResult.expiryMonth + "/" + scanResult.expiryYear);
                }
                if (scanResult.cvv != null) {
                    edtCvv.setText(scanResult.cvv);
                }
                if(edtValor.getText().length() > 0) {
                    ActionTask actionParcelas = new ActionTask(VendaDiretaActivity.this, getParcelasCartao, getResources().getString(R.string.aguarde_txt));
                    actionParcelas.execute();
                }
            }
        }
    }

    Action getParcelasCartao = new Action() {

        double valor;
        @Override
        public void onPostExecute() {
            getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
            );
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            if(responseServer.hasError()){
                new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                SessionHelper.getInstance().setResponseServer(null);
            }else {
                arrayParcelas = new String[parcelaCartaos.size()];
                for (int i = 0; i < parcelaCartaos.size(); i++) {
                    arrayParcelas[i] = parcelaCartaos.get(i).getParcelaFormatada();
                }
                cbbParcelas.setItems(arrayParcelas);
            }
        }

        @Override
        public void run() {
            try {
                String valorStr = edtValor.getText().toString().replaceAll("[.]", "");
                valorStr = valorStr.replaceAll(",",".");
                if (valorStr.isEmpty() )
                    valor = 0.0;
                else
                    valor = Double.valueOf(valorStr);
                RestServices rs = new RestServices(mCtx);
                parcelaCartaos = rs.getParcelasCartao(edtNUmCartao.getText().toString().replaceAll(" ", ""), valor);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };

    Action getContaToken = new Action() {

        @Override
        public void onPostExecute() {
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            if(responseServer.hasError()){
                new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                SessionHelper.getInstance().setResponseServer(null);
            }
        }

        @Override
        public void run() {
            try {
                RestServices rs = new RestServices(mCtx);
                conta_token = rs.getTokenByContaId(App.getUser(mCtx).getCurrent_conta_id());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };


    Action salvarVendaDireta = new Action() {

        @Override
        public void onPostExecute() {
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            if(responseServer.hasError() && responseServer.getCode() != 422){
                new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                SessionHelper.getInstance().setResponseServer(null);
            }else if (vendaDireta.getTransaction_code() != null ){
                Intent myIntent = new Intent(VendaDiretaActivity.this, VendaDiretaProtecaoActivity.class);
                myIntent.putExtra("transaction_code", vendaDireta.getTransaction_code());
                startActivity(myIntent);
                finish();
            }else{
                Toast.makeText(getApplicationContext(), vendaDireta.getError_message(), Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void run() {
            try {
                RestServices rs = new RestServices(mCtx);
                vendaDireta = rs.salvarVendaDireta(vendaDireta);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };

    Action gerarCodigoVerificacao = new Action() {
        ResultServer resultServer;
        @Override
        public void onPostExecute() {
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            if(responseServer.hasError() && responseServer.getCode() != 422){
                new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                SessionHelper.getInstance().setResponseServer(null);
            }else if (resultServer.getSuccess()){
                dialogSms = new Dialog(VendaDiretaActivity.this);
                Utils.showConfirmaçãoSms(dialogSms, edtTelefone.getText().toString(), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        EditText edtSmsCode = dialogSms.findViewById(R.id.edtSmsCode);
                        if(edtSmsCode.getText().length()>0){
                            ActionTask action = new ActionTask(VendaDiretaActivity.this, verificarCodigo, getResources().getString(R.string.aguarde_txt));
                            verificarCodigo.param = edtSmsCode.getText().toString();
                            action.execute();
                        }else{
                            edtSmsCode.setError("Insira o código de verificação");
                        }
                    }
                });
            }else{
                Toast.makeText(getApplicationContext(), resultServer.getMessage(), Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void run() {
            try {
                RestServices rs = new RestServices(mCtx);
                resultServer = rs.gerarCodigoVerificacao(param);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };

    Action verificarCodigo = new Action() {
        ResultServer resultServer;
        @Override
        public void onPostExecute() {
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            if(responseServer.hasError() && responseServer.getCode() != 422){
                new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                SessionHelper.getInstance().setResponseServer(null);
            }else if (resultServer.getSuccess()){
                dialogSms.dismiss();
                ActionTask actionSalvarTransferencia = new ActionTask(VendaDiretaActivity.this, salvarVendaDireta, getString(R.string.aguarde_txt));
                actionSalvarTransferencia.execute();
            }else{
                Toast.makeText(getApplicationContext(), resultServer.getMessage(), Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void run() {
            try {
                RestServices rs = new RestServices(mCtx);
                resultServer = rs.verificarCodigo(param);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onValidationSucceeded() {
        vendaDireta = new VendaDireta();
        String valorStr = edtValor.getText().toString().replaceAll("[.]", "");
        valorStr = valorStr.replaceAll(",",".");
        if (valorStr.isEmpty() )
            vendaDireta.setTotal_value(0.0);
        else
            vendaDireta.setTotal_value(Double.valueOf(valorStr));

        vendaDireta.setConta_token(conta_token);
        vendaDireta.setName(edtNomePagador.getText().toString());
        vendaDireta.setCpf(edtCpf.getText().toString().replaceAll("[.-]", ""));
        vendaDireta.setArea_code(edtTelefone.getText().toString().substring(1,3));
        vendaDireta.setPhone(edtTelefone.getText().toString().substring(4,14).replace("-",""));
        vendaDireta.setDescription(edtDescricaoVenda.getText().toString());
        vendaDireta.setCard_number(edtNUmCartao.getText().toString().trim());
        String[] split = edtValidade.getText().toString().split("/");
        vendaDireta.setMonth(split[0]);
        vendaDireta.setYear(split[1]);
        vendaDireta.setCvv(edtCvv.getText().toString());
        vendaDireta.setEmail(edtEmail.getText().toString());
        if (parcelaCartaos != null && !parcelaCartaos.isEmpty()){
            vendaDireta.setInstallments(parcelaCartaos.get(cbbParcelas.getSelectedIndex()).getQuantity());
            vendaDireta.setTotal_value(parcelaCartaos.get(cbbParcelas.getSelectedIndex()).getTotalAmount());
            if(vendaDireta.getTotal_value()>0) {
                ActionTask action = new ActionTask(VendaDiretaActivity.this, gerarCodigoVerificacao, getString(R.string.aguarde_txt));
                gerarCodigoVerificacao.param = edtTelefone.getText().toString();
                action.execute();

            }else{
                Toast.makeText(getApplicationContext(),R.string.informe_o_valor_txt,Toast.LENGTH_LONG).show();
            }
        }else{
            Toast.makeText(getApplicationContext(),"Selecione as parcelas do cartão",Toast.LENGTH_LONG).show();
        }



    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
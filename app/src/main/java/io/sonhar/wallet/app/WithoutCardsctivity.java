package io.sonhar.wallet.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import io.sonhar.wallet.R;
import io.sonhar.wallet.model.User;

public class WithoutCardsctivity extends AppCompatActivity {

    TextView lblNome;
    Button btnNovoCartao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_without_cardsctivity);

        btnNovoCartao = (Button) findViewById(R.id.btnNovoCartao);
        lblNome = (TextView) findViewById(R.id.lblNome);

        User userLogged = App.getUser(getApplicationContext());

        lblNome.setText("Olá, "+userLogged.getNome());

        btnNovoCartao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),CreateCardActivity.class));
            }
        });

    }

}

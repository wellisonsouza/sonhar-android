package io.sonhar.wallet.app;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import io.sonhar.wallet.R;
import io.sonhar.wallet.adapter.EntradaItemAdapter;
import io.sonhar.wallet.model.EmissaoBoleto;
import io.sonhar.wallet.model.Entrada;
import io.sonhar.wallet.model.ResponseServer;
import io.sonhar.wallet.util.Action;
import io.sonhar.wallet.util.ActionTask;
import io.sonhar.wallet.util.ErrorHelper;
import io.sonhar.wallet.util.SessionHelper;
import io.sonhar.wallet.util.Utils;
import io.sonhar.wallet.ws.RestServices;

public class DepositoActivity extends AppCompatActivity implements AbsListView.OnScrollListener {

    FloatingActionButton btnNovoDeposito;
    ListView lvEntradas;
    private boolean loading = false;
    private boolean loadedAllItens = false;
    ArrayList<Entrada> entradas;
    ArrayList<Entrada> todasEntradas;
    EntradaItemAdapter adapter;
//    LinearLayout layoutEmpty;
    TextView textEmpty;
    private int page = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deposito);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        todasEntradas = new ArrayList<Entrada>();
        textEmpty = findViewById(R.id.textEmpty);
        btnNovoDeposito = findViewById(R.id.fabNewDeposito);
        lvEntradas = findViewById(R.id.lvDepositos);
        lvEntradas.setOnScrollListener(this);

        btnNovoDeposito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(DepositoActivity.this, NovoDepositoActivity.class);
                startActivity(myIntent);
            }
        });

        lvEntradas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Entrada entrada = todasEntradas.get(i);
                EmissaoBoleto emissaoBoleto = new EmissaoBoleto();
                emissaoBoleto.setCod_boleto(entrada.getCod_boleto());
                emissaoBoleto.setUrl_boleto(entrada.getLink_boleto());
                emissaoBoleto.setValor(entrada.getValorEmReais());
                emissaoBoleto.setFavorecido(App.getUser(getApplication()).getNome());
                emissaoBoleto.setData_transacao(entrada.getData_transacao());
                emissaoBoleto.setStatus(entrada.getStatus_entrada());
                final Dialog dialog = new Dialog(DepositoActivity.this);
                Utils.showSucessoDepositoBoleto(DepositoActivity.this, emissaoBoleto, dialog, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
            }
        });

    }

    @Override
    protected void onResume() {
        loadedAllItens = false;
        page = 1;
        getEntradasService();
        super.onResume();
    }

    public void getEntradasService(){
        ActionTask actionLoadBancos = new ActionTask(DepositoActivity.this, getEntradas, "Aguarde...");
        actionLoadBancos.execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return (super.onOptionsItemSelected(menuItem));
    }

    Action getEntradas = new Action() {
        @Override
        public void onPostExecute() {
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();

            // TODO Auto-generated method stub
            try {
                if(responseServer.hasError()){
                    new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                    SessionHelper.getInstance().setResponseServer(new ResponseServer());
                }else{
                    if(entradas.size() < 10){
                        loadedAllItens = true;
                    }
                    if (page == 1){
                        todasEntradas.addAll(entradas);
                        adapter = new EntradaItemAdapter(mCtx, entradas);
                        lvEntradas.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }else{
                        todasEntradas.addAll(entradas);
                        adapter.add(entradas);
                        loading = false;
                    }

                    if(todasEntradas.size() == 0)
                        textEmpty.setVisibility(View.VISIBLE);
                    else
                        textEmpty.setVisibility(View.GONE);
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        @Override
        public void run() {
            RestServices rs = new RestServices(mCtx);
            entradas = rs.getEntradas(page, "q[tipo_eq]=5");
        }

    };

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (lvEntradas.getAdapter() == null)
            return ;

        if (lvEntradas.getAdapter().getCount() == 0)
            return ;
        int l = visibleItemCount + firstVisibleItem;
        if (l >= totalItemCount && !loading && !loadedAllItens) {
            loading = true;
            page++;
            getEntradasService();
        }
    }
}

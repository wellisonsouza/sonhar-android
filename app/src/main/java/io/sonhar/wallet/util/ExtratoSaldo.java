package io.sonhar.wallet.util;

import java.util.List;

/**
 * Created by PedroPontes1 on 18/06/2018.
 */

public class ExtratoSaldo {
    private String saldo_cartao;
    private List<ExtratoLinha> extrato;

    public String getSaldo_cartao() {
        return saldo_cartao;
    }

    public void setSaldo_cartao(String saldo_cartao) {
        this.saldo_cartao = saldo_cartao;
    }

    public List<ExtratoLinha> getExtrato() {
        return extrato;
    }

    public void setExtrato(List<ExtratoLinha> extrato) {
        this.extrato = extrato;
    }
}

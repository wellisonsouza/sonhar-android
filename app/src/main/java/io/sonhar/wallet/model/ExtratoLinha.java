package io.sonhar.wallet.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PedroPontes1 on 20/06/2018.
 */

public class ExtratoLinha {
    private String data;
    private String descricao;
    private String valor;
    private Integer tipo;
    // TIPOS
    // 0 - SAIDA
    // 1 - ENTRADA

    public ExtratoLinha(String data, String descricao, String valor, Integer tipo){
        this.data = data;
        this.descricao = descricao;
        this.valor = valor;
        this.tipo = tipo;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public static List<ExtratoLinha> linhasTeste(){
        List<ExtratoLinha> linhas = new ArrayList<ExtratoLinha>();

        linhas.add(new ExtratoLinha("2018/06/06 - 09:40", "EXTRA 1302", "$ 135,20", 0));
        linhas.add(new ExtratoLinha("2018/06/06 - 23:52", "SUMUP  *CARLOSALBERTODEAL", "$ 11,70", 0));
        linhas.add(new ExtratoLinha("2018/06/06 - 19:24", "EL KABONG GRILL", "$ 100,00", 0));
        linhas.add(new ExtratoLinha("2018/06/06 - 13:33", "SANDOUI COM DE ALIM E S", "$ 4,00", 0));
        linhas.add(new ExtratoLinha("2018/06/06 - 21:48", "POC BEER", "$ 28,00", 0));
        linhas.add(new ExtratoLinha("2018/06/06 - 19:20", "PAG*QuiosqueTresIrmao", "$ 10,00", 0));
        linhas.add(new ExtratoLinha("2018/06/06 - 19:13", "QUIOSQUE DA COCAD", "$ 12,00", 0));
        linhas.add(new ExtratoLinha("2018/06/06 - 23:32", "EBANX-PNP065E0D9A9", "$ 7,50", 0));
        linhas.add(new ExtratoLinha("2018/06/06 - 22:27", "CANTINA GRAN ROMA LTDA", "$ 112,10", 0));
        linhas.add(new ExtratoLinha("2018/06/06 - 17:25", "PAO DE ACUCAR 238", "$ 160,18", 0));
        linhas.add(new ExtratoLinha("2018/06/06 - 15:45", "CARGA CARTAO", "$ 1220,00", 1));
        linhas.add(new ExtratoLinha("2018/06/06 - 10:43", "PASTELSHOP GOURM", "$ 24,75", 0));
        linhas.add(new ExtratoLinha("2018/06/06 - 13:21", "BENJAMIN ABRAHAO", "$ 17,40", 0));
        linhas.add(new ExtratoLinha("2018/06/06 - 10:05", "COFFEE ME UP CMU", "$ 9,70", 0));
        linhas.add(new ExtratoLinha("2018/06/06 - 20:13", "TECBAN P ACUCAR ABI", "$ 53,26", 0));
        linhas.add(new ExtratoLinha("2018/06/06 - 10:55", "CALDO DE CANA LEAL", "$ 10,00", 0));
        linhas.add(new ExtratoLinha("2018/06/06 - 16:44", "VIVO SP - LJ L301", "$ 78,90", 0));

        return linhas;
    }
}

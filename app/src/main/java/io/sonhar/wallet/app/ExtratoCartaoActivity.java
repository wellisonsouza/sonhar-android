package io.sonhar.wallet.app;

import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import io.sonhar.wallet.R;
import io.sonhar.wallet.adapter.ExtratoCartaoListAdapter;
import io.sonhar.wallet.model.Cartao;
import io.sonhar.wallet.model.ExtratoCartao;
import io.sonhar.wallet.model.ResponseServer;
import io.sonhar.wallet.util.Action;
import io.sonhar.wallet.util.ActionTask;
import io.sonhar.wallet.util.ErrorHelper;
import io.sonhar.wallet.util.ExtratoSaldo;
import io.sonhar.wallet.util.SessionHelper;
import io.sonhar.wallet.ws.RestServices;

public class ExtratoCartaoActivity extends AppCompatActivity {

    private ExtratoSaldo extratoSaldo;
    private ViewPager vpCarousel;                           // ViewPager for the Carousel view
    private LinearLayout llPageIndicatorContainer;
    private FragmentManager fragmentManager;  // Carousel view item indicator, the little bullets at the bottom of the carousel
    private ArrayList<ImageView> carouselPageIndicators;    // Carousel view item, the little bullet at the bottom of the carousel
    Cartao cartao;
    String saldo;
    ProgressBar pbSaldo;
    ProgressBar pbExtrato;
    TextView lblSaldo, lblNumCard, lblHolder;
    ListView lvExtrato;

    String proxy, numCard, holder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_extrato_cartao);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        proxy = getIntent().getStringExtra("proxy");
        numCard = getIntent().getStringExtra("numCard");
        holder = getIntent().getStringExtra("holder");
        pbSaldo = (ProgressBar) findViewById(R.id.pbSaldo);
        pbExtrato = (ProgressBar) findViewById(R.id.pbExtrato);
        lblSaldo = (TextView) findViewById(R.id.lblSaldo);
        lblNumCard = (TextView) findViewById(R.id.lblNumCard);
        lblHolder = (TextView) findViewById(R.id.lblHolder);
        lblHolder.setText(holder);
        lblNumCard.setText(numCard);
        lvExtrato = (ListView) findViewById(R.id.lvExtrato);

        pbSaldo.setVisibility(View.GONE);
        pbExtrato.setVisibility(View.GONE);

        ActionTask action = new ActionTask(ExtratoCartaoActivity.this, saldoAction, getResources().getString(R.string.aguarde_txt));
        saldoAction.param = proxy;
        action.execute();
        ActionTask actionCardsTask = new ActionTask(ExtratoCartaoActivity.this, extratoAction, getResources().getString(R.string.aguarde_txt));
        extratoAction.param = proxy;
        actionCardsTask.execute();

    }

    Action extratoAction = new Action() {
        ArrayList<ExtratoCartao> extratoCartaos;
        @Override
        public void onPostExecute() {
            // TODO Auto-generated method stub
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            try {
                if(responseServer.hasError()){
                    new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                    SessionHelper.getInstance().setResponseServer(null);
                }else{
//                    lblSaldo.setText(getApplicationContext().getResources().getString(R.string.moeda)+" "+cartao.getSaldo());
                    pbExtrato.setVisibility(View.GONE);
                    //List<ExtratoLinha> linhas = ExtratoLinha.linhasTeste();
                    ExtratoCartaoListAdapter adapter = new ExtratoCartaoListAdapter(getApplicationContext(), extratoCartaos);
                    lvExtrato.setAdapter(adapter);
                }

            }catch(Exception ex){
                ex.printStackTrace();
            }

        }
        @Override
        public void run () {
            try {
                RestServices rs = new RestServices(ExtratoCartaoActivity.this);
                extratoCartaos = rs.getExtratoCartao(param);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };

    Action saldoAction = new Action() {
        Double saldo;
        Locale ptBr = new Locale("pt", "BR");
        NumberFormat formatter = NumberFormat.getCurrencyInstance(ptBr);
        @Override
        public void onPostExecute() {
            // TODO Auto-generated method stub
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            try {
                if(responseServer.hasError()){
                    new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                    SessionHelper.getInstance().setResponseServer(null);
                }else{
                    lblSaldo.setText(formatter.format(saldo));
                }

            }catch(Exception ex){
                ex.printStackTrace();
            }

        }
        @Override
        public void run () {
            try {
                RestServices rs = new RestServices(ExtratoCartaoActivity.this);
                saldo = rs.getSaldoCartao(param);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return (super.onOptionsItemSelected(menuItem));
    }
}

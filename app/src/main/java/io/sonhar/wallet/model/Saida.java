package io.sonhar.wallet.model;

import java.util.Date;

/**
 * Created by wellison on 12/07/2018.
 */

public class Saida {
    private int tipo_favorecido;
    private long id;
    private long banco_id;
    private String agencia;
    private String conta_bancaria;
    private String cpf_cnpj;
    private String tipo_pessoa;
    private String tipo_conta;
    private String operacao;
    private String agencia_dv;
    private String conta_dv;
    private String nome_favorecido;
    private String cod_boleto;
    private Double valor;
    private String tipo_pagamento;
    private long favorecido_id;
    private String conta_favorecido;
    private String numero_cartao;
    private Date data_agendamento;
    private Date data_vencimento_boleto;
    private String error_message;
    private int valor_centavos;

    public Saida() {
        this.data_agendamento = new Date();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getTipo_favorecido() {
        return tipo_favorecido;
    }

    public void setTipo_favorecido(int tipo_favorecido) {
        this.tipo_favorecido = tipo_favorecido;
    }

    public String getNome_favorecido() {
        return nome_favorecido;
    }

    public void setNome_favorecido(String nome_favorecido) {
        this.nome_favorecido = nome_favorecido;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getTipo_pagamento() {
        return tipo_pagamento;
    }

    public void setTipo_pagamento(String tipo_pagamento) {
        this.tipo_pagamento = tipo_pagamento;
    }

    public long getFavorecido_id() {
        return favorecido_id;
    }

    public void setFavorecido_id(long favorecido_id) {
        this.favorecido_id = favorecido_id;
    }

    public String getConta_favorecido() {
        return conta_favorecido;
    }

    public void setConta_favorecido(String conta_favorecido) {
        this.conta_favorecido = conta_favorecido;
    }

    public String getNumero_cartao() {
        return numero_cartao;
    }

    public void setNumero_cartao(String numero_cartao) {
        this.numero_cartao = numero_cartao;
    }

    public Date getData_agendamento() {
        return data_agendamento;
    }

    public void setData_agendamento(Date data_agendamento) {
        this.data_agendamento = data_agendamento;
    }

    public String getError_message() {
        return error_message;
    }

    public void setError_message(String error_message) {
        this.error_message = error_message;
    }

    public long getBanco_id() {
        return banco_id;
    }

    public void setBanco_id(long banco_id) {
        this.banco_id = banco_id;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public String getConta_bancaria() {
        return conta_bancaria;
    }

    public void setConta_bancaria(String conta_bancaria) {
        this.conta_bancaria = conta_bancaria;
    }

    public String getCpf_cnpj() {
        return cpf_cnpj;
    }

    public void setCpf_cnpj(String cpf_cnpj) {
        this.cpf_cnpj = cpf_cnpj;
    }

    public String getTipo_pessoa() {
        return tipo_pessoa;
    }

    public void setTipo_pessoa(String tipo_pessoa) {
        this.tipo_pessoa = tipo_pessoa;
    }

    public String getTipo_conta() {
        return tipo_conta;
    }

    public void setTipo_conta(String tipo_conta) {
        this.tipo_conta = tipo_conta;
    }

    public String getOperacao() {
        return operacao;
    }

    public void setOperacao(String operacao) {
        this.operacao = operacao;
    }

    public String getAgencia_dv() {
        return agencia_dv;
    }

    public void setAgencia_dv(String agencia_dv) {
        this.agencia_dv = agencia_dv;
    }

    public String getConta_dv() {
        return conta_dv;
    }

    public void setConta_dv(String conta_dv) {
        this.conta_dv = conta_dv;
    }

    public int getValor_centavos() {
        return valor_centavos;
    }

    public void setValor_centavos(int valor_centavos) {
        this.valor_centavos = valor_centavos;
    }

    public String getCod_boleto() {
        return cod_boleto;
    }

    public void setCod_boleto(String cod_boleto) {
        this.cod_boleto = cod_boleto;
    }

    public Date getData_vencimento_boleto() {
        return data_vencimento_boleto;
    }

    public void setData_vencimento_boleto(Date data_vencimento_boleto) {
        this.data_vencimento_boleto = data_vencimento_boleto;
    }
}


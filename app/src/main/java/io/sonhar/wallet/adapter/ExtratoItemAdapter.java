package io.sonhar.wallet.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import io.sonhar.wallet.R;
import io.sonhar.wallet.model.Extrato;

/**
 * Created by wellison on 11/07/2018.
 */

public class ExtratoItemAdapter extends ArrayAdapter<Extrato> {
    private List<Extrato> extratos;
    private Context mCtx;

    public ExtratoItemAdapter(Context ctx, List<Extrato> extratos) {
        super(ctx, R.layout.row_extrato);
        this.mCtx = ctx;
        this.extratos = extratos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        view = LayoutInflater.from(mCtx).inflate(R.layout.row_extrato, parent, false);
        //view.setLayoutParams(new Carousel.LayoutParams(70, 70));
        Locale ptBr = new Locale("pt", "BR");
        NumberFormat formatter = NumberFormat.getCurrencyInstance(ptBr);
        ViewHolder holder = new ViewHolder();
        holder.lblDescricao = (TextView) view.findViewById(R.id.lblDescricao);
        holder.lblData = (TextView) view.findViewById(R.id.lblData);
        holder.lblValor = (TextView) view.findViewById(R.id.lblValor);
        holder.img = (ImageView) view.findViewById(R.id.img);
        view.setTag(holder);
        final Extrato extrato = extratos.get(position);

        holder.lblDescricao.setText(extrato.getDescricao());
        holder.lblData.setText(new SimpleDateFormat("yyyy/MM/dd HH:mm").format(extrato.getCreated_at()));
        holder.lblValor.setText(formatter.format(extrato.getValorEmReais()));

        if (extrato.getTipo().equalsIgnoreCase("saida")){
            holder.img.setImageResource(R.drawable.arrow_up);
            holder.lblValor.setTextColor(mCtx.getResources().getColor(R.color.redSaldo));
            holder.lblDescricao.setTextColor(Color.parseColor("#000000"));
            holder.lblData.setTextColor(Color.parseColor("#000000"));
        }else{
            holder.img.setImageResource(R.drawable.arrow_down);
            holder.lblValor.setTextColor(mCtx.getResources().getColor(R.color.greenSaldo));
            holder.lblDescricao.setTextColor(Color.parseColor("#000000"));
            holder.lblData.setTextColor(Color.parseColor("#000000"));
        }

        return view;
    }

    public void add(List<Extrato> data) {
        this.extratos.addAll(data);
        notifyDataSetChanged();
    }


    private class ViewHolder {
        TextView lblDescricao;
        TextView lblData;
        TextView lblValor;
        ImageView img;
    }

    @Override
    public Extrato getItem(int position) {
        return extratos.get(position);
    }

    @Override
    public int getCount() {
        return extratos.size();
    }
}

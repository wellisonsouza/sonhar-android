package io.sonhar.wallet.app;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.sonhar.wallet.model.User;


/**
 * Created by PedroPontes on 18/07/15.
 */
public class App {
    public static Context context;

    public static void saveUser(Context c, User user){

        SharedPreferences prefs = c.getSharedPreferences("USERDATA", 0);
        SharedPreferences.Editor prefsEdit= prefs.edit();
        Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
        String json = gson.toJson(user);
        prefsEdit.putString("strUserData", json);

        prefsEdit.commit();
    }



    public static void logout(Context c){

        SharedPreferences prefs = c.getSharedPreferences("USERDATA", 0);
        SharedPreferences.Editor prefsEdit= prefs.edit();
        prefsEdit.putString("strUserData", "-1");
        prefsEdit.putString("strEmpresaData", "-1");
        prefsEdit.putString("strOptionsData", "-1");

        prefsEdit.commit();
    }

    public static User getUser(Context c){
        User user = new User();
        SharedPreferences prefs = c.getSharedPreferences("USERDATA", 0);
        Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
        String json = prefs.getString("strUserData","-1");

        if(json.equalsIgnoreCase("-1"))
            return null;
        else{
            user = gson.fromJson(json,User.class);
        }

        return user;

    }




}

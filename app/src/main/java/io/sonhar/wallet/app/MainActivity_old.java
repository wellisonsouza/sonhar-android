package io.sonhar.wallet.app;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;


import java.util.ArrayList;
import java.util.List;

import io.sonhar.wallet.R;
import io.sonhar.wallet.fragment.CardsFragment;
import io.sonhar.wallet.fragment.Fragmento;
import io.sonhar.wallet.fragment.HomeFragment;
import io.sonhar.wallet.fragment.TransactionFragment;
import io.sonhar.wallet.fragment.TransferFragment;
import io.sonhar.wallet.fragment.PaymentFragment;
import io.sonhar.wallet.fragment.ProfileFragment;
import io.sonhar.wallet.fragment.RecargaFragment;
import io.sonhar.wallet.fragment.TimeLineFragment;
import io.sonhar.wallet.model.ResultServer;
import io.sonhar.wallet.model.User;
import io.sonhar.wallet.util.Action;
import io.sonhar.wallet.util.ActionFaceTask;
import io.sonhar.wallet.util.Utils;
import io.sonhar.wallet.ws.RestServices;


public class MainActivity_old extends AppCompatActivity {

    static final int PICK_FACE_REQUEST = 12;
    public static int type_faceCamera = 0;
    // types face camera
    // 0 - cadastro
    // 1 - verify
    // 2 - payment boleto
    // 3 - recharge phone

    // FRAGMENTS
    List<Fragment> fragments = new ArrayList<Fragment>();
    TimeLineFragment timeLineFragment;
    CardsFragment cardsFragment;
    TransferFragment transferFragment;
    PaymentFragment paymentFragment;
    RecargaFragment recargaFragment;
    ProfileFragment profileFragment;
    HomeFragment homeFragment;
    TransactionFragment transactionFragment;
    FragmentManager fm;
    Fragmento actualFrag;
    Context mCtx;

    AHBottomNavigation bottomBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String locale = getResources().getConfiguration().locale.getDisplayName();
        //Toast.makeText(getApplicationContext(), locale, Toast.LENGTH_SHORT).show();


        //initBottomNav();
        initFrags();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCtx = this;
    }



    private void initBottomNav(){


    }

    private int fetchColor(@ColorRes int color) {
        return ContextCompat.getColor(this, color);
    }

    private void initFrags(){
        fm = getSupportFragmentManager();
        timeLineFragment = new TimeLineFragment();
        cardsFragment = new CardsFragment();
        transferFragment = new TransferFragment();
        profileFragment = new ProfileFragment();
        paymentFragment = new PaymentFragment();
        recargaFragment = new RecargaFragment();

        homeFragment = new HomeFragment();
        transactionFragment = new TransactionFragment();

        fragments.add(timeLineFragment);
        fragments.add(cardsFragment);
        fragments.add(transferFragment);
        fragments.add(paymentFragment);
        fragments.add(recargaFragment);
        fragments.add(profileFragment);
        fragments.add(homeFragment);
        fragments.add(transactionFragment);

        setFragment(homeFragment);
    }

    private void setFragment(Fragment f){
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.fragmentMain, f);
        transaction.commitAllowingStateLoss();
        fm.executePendingTransactions();

        actualFrag = (Fragmento)f;
        actualFrag.reload(MainActivity_old.this);
//        goToFragmentWithoutAnim(menuFragment);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (type_faceCamera == 1 || type_faceCamera == 2 || type_faceCamera == 3) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                String base64 = data.getStringExtra("face64");
                Log.i("FACE 64", base64);

                verifyPhotoAction.param = base64;


                ActionFaceTask actionLoadGames = new ActionFaceTask(MainActivity_old.this, verifyPhotoAction, "Autenticando..");
                actionLoadGames.execute();
            }
        } else {
            if (resultCode == RESULT_OK) {
                String base64 = data.getStringExtra("face64");
                Log.i("FACE 64", base64);

                sendPhotoAction.param = base64;


                ActionFaceTask actionLoadGames = new ActionFaceTask(MainActivity_old.this, sendPhotoAction, "Aguarde..");
                actionLoadGames.execute();
            }
        }
    }

    Action sendPhotoAction = new Action() {
        User user = new User();
        ResultServer resultServer;
        @Override
        public void onPostExecute() {
            // TODO Auto-generated method stub
            Log.i("RETORNO", "OK");
            try {


                Toast.makeText(mCtx,resultServer.getMessage(),Toast.LENGTH_LONG).show();

            }catch(Exception ex){
                ex.printStackTrace();
            }

        }
        @Override
        public void run () {
            try {

                RestServices rs = new RestServices();
                user = App.getUser(getApplicationContext());
//
                resultServer = rs.send_photo(param,user);

            } catch (Exception ex) {
                ex.printStackTrace();
                user.setId(null);
            }
        }
    };

    Action verifyPhotoAction = new Action() {
        User user = new User();
        ResultServer resultServer;
        @Override
        public void onPostExecute() {
            // TODO Auto-generated method stub
            Log.i("RETORNO", "OK");
            try {


                if (resultServer.getSuccess()) {

                    if (type_faceCamera == 1) {
                        final TransferFragment frag = (TransferFragment) actualFrag;


                        String msg = "Olá, " + App.getUser(getApplicationContext()).getNome();

                        final Dialog dialog = new Dialog(mCtx);
                        Utils.showAlertFaceSuccess(msg, dialog, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
//                                frag.dialogTransfer.dismiss();
                               // Utils.showAlertTransferSuccess(App.getUser(getApplicationContext()).getNome(), mCtx);
                            }
                        });
                    }else if (type_faceCamera == 2){
                        final PaymentFragment frag = (PaymentFragment) actualFrag;


                        String msg = "Olá, " + App.getUser(getApplicationContext()).getNome();

                        final Dialog dialog = new Dialog(mCtx);
                        Utils.showAlertFaceSuccess(msg, dialog, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                                frag.dialogBoleto.dismiss();
                                //Utils.showAlertTransferSuccess(App.getUser(getApplicationContext()).getNome(), mCtx);
                            }
                        });
                    }else if (type_faceCamera == 3){
                        final PaymentFragment frag = (PaymentFragment) actualFrag;


                        String msg = "Olá, " + App.getUser(getApplicationContext()).getNome();

                        final Dialog dialog = new Dialog(mCtx);
                        Utils.showAlertFaceSuccess(msg, dialog, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                                frag.dialogRecarga.dismiss();
                                //Utils.showAlertTransferSuccess(App.getUser(getApplicationContext()).getNome(), mCtx);
                            }
                        });
                    }
                }else{
                    String msg = "Infelizmente você não é "+App.getUser(getApplicationContext()).getNome();
                    Utils.showAlertFaceError(msg,mCtx);
                }

            }catch(Exception ex){
                ex.printStackTrace();
            }

        }
        @Override
        public void run () {
            try {

                RestServices rs = new RestServices();
                user = App.getUser(getApplicationContext());
//
                resultServer = rs.vefify_face(param,user);


            } catch (Exception ex) {
                ex.printStackTrace();
                user.setId(null);
            }
        }
    };

    public void goToTransacation(){
        transactionFragment = new TransactionFragment();
        setFragment(transactionFragment);
    }
}

package io.sonhar.wallet.app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import io.sonhar.wallet.R;
import io.sonhar.wallet.adapter.SolicitacaoAnaliseDocumentoListAdapter;
import io.sonhar.wallet.adapter.SolicitacaoCartaoListAdapter;
import io.sonhar.wallet.model.ResponseServer;
import io.sonhar.wallet.model.SolicitacaoAnaliseDocumento;
import io.sonhar.wallet.model.SolicitacaoCartao;
import io.sonhar.wallet.util.Action;
import io.sonhar.wallet.util.ActionTask;
import io.sonhar.wallet.util.ErrorHelper;
import io.sonhar.wallet.util.SessionHelper;
import io.sonhar.wallet.util.Utils;
import io.sonhar.wallet.ws.RestServices;

public class SolicitacaoAnaliseDocumentoActivity extends AppCompatActivity {
    ListView lvSolicitacoes;
    ArrayList<SolicitacaoAnaliseDocumento> solicitacaoAnaliseDocumentos;
    Button btnEnviarDocumentacao;
    TextView lblStatusConta;
    Boolean documentacaoAprovada = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solicitacao_analise_documento);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        lvSolicitacoes = findViewById(R.id.lvSolicitacoes);
        btnEnviarDocumentacao = findViewById(R.id.btnEnviarDocumentacao);
        lblStatusConta = findViewById(R.id.lblStatusConta);

        btnEnviarDocumentacao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(SolicitacaoAnaliseDocumentoActivity.this, EnviarDocumentacaoActivity.class);
                startActivity(myIntent);
            }
        });

        lvSolicitacoes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Utils.showDetalheSolicitacaoAnaliseDocumento(SolicitacaoAnaliseDocumentoActivity.this, solicitacaoAnaliseDocumentos.get(i));
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        ActionTask action2 = new ActionTask(SolicitacaoAnaliseDocumentoActivity.this, getStatusConta, getResources().getString(R.string.aguarde_txt));
        action2.execute();
        ActionTask action = new ActionTask(SolicitacaoAnaliseDocumentoActivity.this, getSolicitacoes, getResources().getString(R.string.aguarde_txt));
        action.execute();
    }

    Action getSolicitacoes = new Action() {
        Locale ptBr = new Locale("pt", "BR");
        NumberFormat formatter = NumberFormat.getCurrencyInstance(ptBr);

        @Override
        public void onPostExecute() {
            // TODO Auto-generated method stub
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            try {
                if(responseServer.hasError()){
                    new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                    SessionHelper.getInstance().setResponseServer(null);
                }

                SolicitacaoAnaliseDocumentoListAdapter adapter = new SolicitacaoAnaliseDocumentoListAdapter(mCtx, solicitacaoAnaliseDocumentos);
                lvSolicitacoes.setAdapter(adapter);
                adapter.notifyDataSetChanged();
//                if(adapter.getCount() == 0)
//                    layoutEmpty.setVisibility(View.VISIBLE);
//                else
//                    layoutEmpty.setVisibility(View.GONE);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            if (temSolicitacaoAguardandoAnalise() == false){
                btnEnviarDocumentacao.setVisibility(View.VISIBLE);
            }

        }

        @Override
        public void run() {
            RestServices rs = new RestServices(mCtx);
            solicitacaoAnaliseDocumentos = rs.getSolicitacoesAnaliseDocumento();
        }

    };

    Action getStatusConta = new Action() {
        Locale ptBr = new Locale("pt", "BR");
        NumberFormat formatter = NumberFormat.getCurrencyInstance(ptBr);

        @Override
        public void onPostExecute() {
            // TODO Auto-generated method stub
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            try {
                if(responseServer.hasError()){
                    new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                    SessionHelper.getInstance().setResponseServer(null);
                }else if(documentacaoAprovada == null){
                }else if(documentacaoAprovada){
                    lblStatusConta.setVisibility(View.VISIBLE);
                    lblStatusConta.setText("APROVADA");
                }else{
                    lblStatusConta.setVisibility(View.VISIBLE);
                    lblStatusConta.setText("PENDENTE DE DOCUMENTACÃO");
                }

                boolean check = false;
                for (int i = 0; i < solicitacaoAnaliseDocumentos.size(); i++){
                    if (solicitacaoAnaliseDocumentos.get(i).getStatus().contains("aguardando_analise")){
                        check = true;
                    }
                }

                if (documentacaoAprovada != null && documentacaoAprovada == false && check == false){
                    btnEnviarDocumentacao.setVisibility(View.VISIBLE);
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        @Override
        public void run() {
            RestServices rs = new RestServices(mCtx);
            documentacaoAprovada = rs.getStatusDocumentacaoByContaId();
        }

    };

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return (super.onOptionsItemSelected(menuItem));
    }

    private boolean temSolicitacaoAguardandoAnalise(){
        boolean flag = false;
        for(int i = 0; i < solicitacaoAnaliseDocumentos.size(); i++){
            SolicitacaoAnaliseDocumento solicitacaoAnaliseDocumento = solicitacaoAnaliseDocumentos.get(i);
            if (solicitacaoAnaliseDocumento.getStatus().contains("aguardando_analise") || solicitacaoAnaliseDocumento.getStatus().contains("aceito") ){
                flag = true;
            }
        }
        return flag;
    }


}

package io.sonhar.wallet.app;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.sonhar.wallet.R;
import io.sonhar.wallet.adapter.CartaoRowSpinnerAdapter;
import io.sonhar.wallet.model.Cartao;
import io.sonhar.wallet.model.Operadora;
import io.sonhar.wallet.model.Recarga;
import io.sonhar.wallet.model.RecargaTelefone;
import io.sonhar.wallet.model.ResponseServer;
import io.sonhar.wallet.model.ResultServer;
import io.sonhar.wallet.model.User;
import io.sonhar.wallet.model.ValorRecarga;
import io.sonhar.wallet.util.Action;
import io.sonhar.wallet.util.ActionFaceTask;
import io.sonhar.wallet.util.ActionTask;
import io.sonhar.wallet.util.ErrorHelper;
import io.sonhar.wallet.util.MaskEdit;
import io.sonhar.wallet.util.SessionHelper;
import io.sonhar.wallet.util.Utils;
import io.sonhar.wallet.ws.RestServices;

public class RecargaTelefoneActivity extends AppCompatActivity implements Validator.ValidationListener {
    MaterialSpinner spnValor;
    private ArrayList<ValorRecarga> valorRecargas = new ArrayList<ValorRecarga>();
    private String[] arrayValores;
    Validator validator;
    @NotEmpty(messageResId = R.string.campo_obrigatorio)
    @Length(min = 14, max = 14, trim = false, message = "Formato inválido")
    EditText edtNumTelefone, edtConfirmaTelefone;

    @NotEmpty(messageResId = R.string.campo_obrigatorio)
    EditText edtNomeFavorecido;
    Button btnCadastrar;
    Recarga recarga;
    AlertDialog alerta;
    RecargaTelefone recargaTelefone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recarga_telefone);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        spnValor = findViewById(R.id.spnValor);
        edtNumTelefone = findViewById(R.id.edtNumTelefone);
        edtConfirmaTelefone = findViewById(R.id.edtConfirmaTelefone);
        edtNomeFavorecido = findViewById(R.id.edtNomeFavorecido);
        btnCadastrar = findViewById(R.id.btnCadastrar);
        validator = new Validator(this);
        validator.setValidationListener(this);

        edtNumTelefone.addTextChangedListener(MaskEdit.mask(edtNumTelefone, MaskEdit.FORMAT_FONE));
        edtConfirmaTelefone.addTextChangedListener(MaskEdit.mask(edtConfirmaTelefone, MaskEdit.FORMAT_FONE));

        edtNumTelefone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus && edtNumTelefone.getText().length() == 14) {
                    ActionTask actionOperadoras = new ActionTask(RecargaTelefoneActivity.this, getValoresRecarga, getResources().getString(R.string.aguarde_txt));
                    getValoresRecarga.param = edtNumTelefone.getText().toString().replaceAll("[^0-9]", "");
                    actionOperadoras.execute();
                }
            }
        });
        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtNumTelefone.getText().toString().equals(edtConfirmaTelefone.getText().toString()) == false){
                    edtNumTelefone.setError("Número de telefone diferente");
                }else{
                    validator.validate();
                }
            }
        });

        spnValor.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm=(InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edtNomeFavorecido.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(edtNumTelefone.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(edtConfirmaTelefone.getWindowToken(), 0);
                return false;
            }
        }) ;

    }

    Action getValoresRecarga = new Action() {
        @Override
        public void onPostExecute() {
            // TODO Auto-generated method stub
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            try {
                if(responseServer.hasError()){
                    new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                    SessionHelper.getInstance().setResponseServer(null);
                }
                arrayValores = new String[valorRecargas.size()];
                for(int i = 0; i<valorRecargas.size(); i++){
                    arrayValores[i] = String.valueOf(valorRecargas.get(i).getValor());
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            spnValor.setItems(arrayValores);
        }

        @Override
        public void run() {
            RestServices rs = new RestServices(mCtx);
            valorRecargas = rs.getValoresRecarga(param);
        }

    };

    Action salvarRecarga = new Action() {
        ResultServer resultServer;
        @Override
        public void onPostExecute() {
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            try {
                if(responseServer.hasError()){
                    new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                    SessionHelper.getInstance().setResponseServer(null);
                }else if(resultServer.getSuccess() != null && resultServer.getSuccess()){
                    Utils.showAlertTransferSuccess(App.getUser(getApplicationContext()).getNome(), RecargaTelefoneActivity.this, RecargaTelefoneActivity.this);

                }else{
                    Toast.makeText(RecargaTelefoneActivity.this,resultServer.getMessage(), Toast.LENGTH_LONG).show();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        @Override
        public void run() {
            RestServices rs = new RestServices(mCtx);
            resultServer = rs.salvarRecargaTelefone(recargaTelefone);
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onValidationSucceeded() {
        recargaTelefone = new RecargaTelefone();
        recargaTelefone.setNome_favorecido(edtNomeFavorecido.getText().toString());
        recargaTelefone.setNumber(edtNumTelefone.getText().toString());
        recargaTelefone.setOperator_id(valorRecargas.get(spnValor.getSelectedIndex()).getOperator_id());
        recargaTelefone.setPrice(valorRecargas.get(spnValor.getSelectedIndex()).getValor());
        recargaTelefone.setSkuid(valorRecargas.get(spnValor.getSelectedIndex()).getSkuid());
        ActionTask action = new ActionTask(RecargaTelefoneActivity.this, salvarRecarga, getResources().getString(R.string.aguarde_txt));
        action.execute();

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }

    }
}

package io.sonhar.wallet.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import io.sonhar.wallet.R;
import io.sonhar.wallet.model.ExtratoCartao;

/**
 * Created by PedroPontes1 on 17/06/2018.
 */

public class ExtratoCartaoListAdapter extends ArrayAdapter<ExtratoCartao> {
    private List<ExtratoCartao> linhas;
    private Context mCtx;

    public ExtratoCartaoListAdapter(Context ctx, List<ExtratoCartao> linhas) {
        super(ctx, R.layout.row_extrato_linha);
        this.mCtx = ctx;
        this.linhas = linhas;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        view = LayoutInflater.from(mCtx).inflate(R.layout.row_extrato_linha, parent, false);
        //view.setLayoutParams(new Carousel.LayoutParams(70, 70));
        Locale ptBr = new Locale("pt", "BR");
        NumberFormat formatter = NumberFormat.getCurrencyInstance(ptBr);
        ViewHolder holder = new ViewHolder();
        holder.lblDescricao = (TextView) view.findViewById(R.id.lblDescricao);
        holder.lblData = (TextView) view.findViewById(R.id.lblData);
        holder.lblValor = (TextView) view.findViewById(R.id.lblValor);
        holder.img = (ImageView) view.findViewById(R.id.img);
        view.setTag(holder);
        final ExtratoCartao linha = linhas.get(position);

        holder.lblDescricao.setText(linha.getTpTransacao() + "\n" + linha.getDescricao());


        holder.lblData.setText(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(linha.getDataTransacao()));
        holder.lblValor.setText(formatter.format(linha.getValorEmReais()));

        if (linha.getTpTransacao().equalsIgnoreCase("Credito")) {
            holder.img.setImageResource(R.drawable.arrow_down);
            holder.lblValor.setTextColor(mCtx.getResources().getColor(R.color.greenSaldo));
        }else{
            holder.lblValor.setTextColor(mCtx.getResources().getColor(R.color.redSaldo));
            holder.img.setImageResource(R.drawable.arrow_up);

        }

        holder.lblDescricao.setTextColor(Color.parseColor("#000000"));
        holder.lblData.setTextColor(Color.parseColor("#000000"));

        return view;
    }


    private class ViewHolder {
        TextView lblDescricao;
        TextView lblData;
        TextView lblValor;
        ImageView img;
    }

    @Override
    public ExtratoCartao getItem(int position) {
        return linhas.get(position);
    }

    @Override
    public int getCount() {
        return linhas.size();
    }

}


package io.sonhar.wallet.util;

import android.content.Context;

import java.util.ArrayList;

import io.sonhar.wallet.R;
import io.sonhar.wallet.app.CartaoActivity;
import io.sonhar.wallet.app.PagamentoBoletoActivity;
import io.sonhar.wallet.app.TransacaoActivity;
import io.sonhar.wallet.app.TransferenciaActivity;

/**
 * Created by wellison on 11/07/2018.
 */

public class ListaMenu {
    private ArrayList<MenuItem> listaProfile = new ArrayList<MenuItem>();
    private ArrayList<MenuItem> listaWallet = new ArrayList<MenuItem>();
    private ArrayList<MenuItem> listaPay = new ArrayList<MenuItem>();
    private ArrayList<MenuItem> listaBeneficios = new ArrayList<MenuItem>();


    public ListaMenu(Context mCtx) {
        listaProfile.add(new MenuItem(mCtx.getResources().getString(R.string.reconhecimento_facial_txt), mCtx.getResources().getString(R.string.reconhecimento_facial_desc_txt), R.drawable.ic_face_detector, true, false));
        //listaProfile.add(new MenuItem(mCtx.getResources().getString(R.string.reconhecimento_voz_txt), mCtx.getResources().getString(R.string.reconhecimento_voz_desc_txt), R.drawable.ic_audio_detector, CartaoActivity.class));
        listaProfile.add(new MenuItem(mCtx.getResources().getString(R.string.sair_txt), mCtx.getResources().getString(R.string.sair_desc_txt), R.drawable.logout, false, true));

        listaWallet.add(new MenuItem(mCtx.getResources().getString(R.string.historico_transacoes_txt), mCtx.getResources().getString(R.string.historico_transacoes_desc_txt), R.drawable.timeline, TransacaoActivity.class));
        listaWallet.add(new MenuItem(mCtx.getResources().getString(R.string.cartoes_txt), mCtx.getResources().getString(R.string.cartoes_desc_txt), R.drawable.credit_card, CartaoActivity.class));
        listaWallet.add(new MenuItem(mCtx.getResources().getString(R.string.transferencia_txt), mCtx.getResources().getString(R.string.transferencia_desc_txt), R.drawable.transfer, TransferenciaActivity.class));
        listaWallet.add(new MenuItem(mCtx.getResources().getString(R.string.pagamento_boleto_txt), mCtx.getResources().getString(R.string.pagamento_boleto_desc_txt), R.drawable.payment,  PagamentoBoletoActivity.class));
//        listaWallet.add(new MenuItem("Recarga de Telefone", "Gerenciamento dos cartões", R.drawable.smartphone, CartaoActivity.class));

        listaPay.add(new MenuItem("Empresas", "blabla", R.drawable.ic_companies, CartaoActivity.class));
        listaPay.add(new MenuItem("Transações", "blabla", R.drawable.ic_transactions_pay, CartaoActivity.class));
        listaPay.add(new MenuItem("Antecipações", "blabla", R.drawable.ic_antecipation, CartaoActivity.class));

        listaBeneficios.add(new MenuItem("Doctor's appointments", "", R.drawable.ic_call_center, CartaoActivity.class));
        listaBeneficios.add(new MenuItem("Medical exams", "", R.drawable.ic_exam, CartaoActivity.class));
        listaBeneficios.add(new MenuItem("Talk to Dr. Rey", "", R.drawable.ic_star, CartaoActivity.class));
        listaBeneficios.add(new MenuItem("Food aid", "", R.drawable.ic_cutlery, CartaoActivity.class));
        listaBeneficios.add(new MenuItem(
                "24 hour family doctor", "", R.drawable.ic_doctor, CartaoActivity.class));
        listaBeneficios.add(new MenuItem("Medicines", "", R.drawable.ic_medical, CartaoActivity.class));
        listaBeneficios.add(new MenuItem("Hospital Expenses", "", R.drawable.ic_invoice, CartaoActivity.class));
        listaBeneficios.add(new MenuItem("Health and wellness coach", "", R.drawable.ic_care, CartaoActivity.class));
        listaBeneficios.add(new MenuItem("Nutritionist", "", R.drawable.ic_diet, CartaoActivity.class));

        listaBeneficios.add(new MenuItem("Funeral assistance 24h", "", R.drawable.ic_grave, CartaoActivity.class));
        listaBeneficios.add(new MenuItem("Accidental death", "", R.drawable.ic_grave_1, CartaoActivity.class));
        listaBeneficios.add(new MenuItem("Permanent disability", "", R.drawable.ic_walker, CartaoActivity.class));


    }

    public ArrayList<MenuItem> getListaPeloMenu(int position){
        if (position == 0)
            return listaProfile;
        else if (position == 1)
            return listaWallet;
        else
            return listaBeneficios;
    }

    public ArrayList<MenuItem> getListaWallet() {
        return listaWallet;
    }

    public void setListaWallet(ArrayList<MenuItem> listaWallet) {
        this.listaWallet = listaWallet;
    }
}

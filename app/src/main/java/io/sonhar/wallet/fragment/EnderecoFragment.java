package io.sonhar.wallet.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import io.sonhar.wallet.R;

/**
 * Created by wellison on 03/08/2018.
 */

public class EnderecoFragment extends Fragment {
    private Context context;
    private View view;

    public static EnderecoFragment newInstance(){
        EnderecoFragment fragment = new EnderecoFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        this.context =context;

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup parent, Bundle savedState){
        view =inflater.inflate(R.layout.fragment_endereco, parent, false);
        return view;
    }
}

package io.sonhar.wallet.util;

/**
 * Created by wellison on 11/07/2018.
 */

public class MenuItem {
    private String nome;
    private String descricao;
    private int image;
    private Class activity;
    private Boolean isLogout;
    private Boolean isFaceConf;

    public MenuItem(String nome, String descricao, int image, Class activity) {
        this.nome = nome;
        this.descricao = descricao;
        this.image = image;
        this.activity = activity;
        this.isFaceConf = false;
        this.isLogout = false;
    }

    public MenuItem(String nome, String descricao, int image, Boolean isFaceConf, Boolean isLogout) {
        this.nome = nome;
        this.descricao = descricao;
        this.image = image;
        this.activity = null;
        this.isFaceConf = isFaceConf;
        this.isLogout = isLogout;
    }


    public Boolean getLogout() {
        return isLogout;
    }

    public void setLogout(Boolean logout) {
        isLogout = logout;
    }

    public Boolean getFaceConf() {
        return isFaceConf;
    }

    public void setFaceConf(Boolean faceConf) {
        isFaceConf = faceConf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public Class getActivity() {
        return activity;
    }

    public void setActivity(Class activity) {
        this.activity = activity;
    }
}

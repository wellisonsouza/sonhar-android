package io.sonhar.wallet.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import io.sonhar.wallet.R;
import io.sonhar.wallet.model.Entrada;

/**
 * Created by wellison on 11/07/2018.
 */

public class EntradaItemAdapter extends ArrayAdapter<Entrada> {
    private List<Entrada> entradas;
    private Context mCtx;

    public EntradaItemAdapter(Context ctx, List<Entrada> entradas) {
        super(ctx, R.layout.row_entrada);
        this.mCtx = ctx;
        this.entradas = entradas;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        view = LayoutInflater.from(mCtx).inflate(R.layout.row_entrada, parent, false);
        //view.setLayoutParams(new Carousel.LayoutParams(70, 70));
        Locale ptBr = new Locale("pt", "BR");
        NumberFormat formatter = NumberFormat.getCurrencyInstance(ptBr);
        ViewHolder holder = new ViewHolder();
        holder.lblDescricao = (TextView) view.findViewById(R.id.lblDescricao);
        holder.lblData = (TextView) view.findViewById(R.id.lblData);
        holder.lblValor = (TextView) view.findViewById(R.id.lblValor);
        holder.img = (ImageView) view.findViewById(R.id.img);
        final Entrada entrada = entradas.get(position);

        holder.lblDescricao.setText(entrada.getTipo().replaceAll("_", " "));
        holder.lblData.setText(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(entrada.getData_transacao()));
        holder.lblValor.setText(formatter.format(entrada.getValorEmReais()));

        if (entrada.getTipo_transacao().equalsIgnoreCase("presencial")){
            holder.img.setImageResource(R.drawable.ic_presencial);
        }else{
            holder.img.setImageResource(R.drawable.ic_web);
        }
        view.setTag(entrada.getId());


        return view;
    }

    public void add(List<Entrada> data) {
        this.entradas.addAll(data);
        notifyDataSetChanged();
    }

    private class ViewHolder {
        TextView lblDescricao;
        TextView lblData;
        TextView lblValor;
        ImageView img;
    }

    @Override
    public Entrada getItem(int position) {
        return entradas.get(position);
    }

    @Override
    public int getCount() {
        return entradas.size();
    }
}

package io.sonhar.wallet.app;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import info.androidhive.barcode.BarcodeReader;
import io.sonhar.wallet.R;
import io.sonhar.wallet.adapter.CartaoRowSpinnerAdapter;
import io.sonhar.wallet.model.Cartao;
import io.sonhar.wallet.model.PagamentoBoleto;
import io.sonhar.wallet.model.ResponseServer;
import io.sonhar.wallet.model.ResultServer;
import io.sonhar.wallet.model.Saida;
import io.sonhar.wallet.model.User;
import io.sonhar.wallet.util.Action;
import io.sonhar.wallet.util.ActionFaceTask;
import io.sonhar.wallet.util.ActionTask;
import io.sonhar.wallet.util.CameraSourcePreview;
import io.sonhar.wallet.util.ErrorHelper;
import io.sonhar.wallet.util.GraphicOverlay;
import io.sonhar.wallet.util.MaskEdit;
import io.sonhar.wallet.util.MoneyTextWatcher;
import io.sonhar.wallet.util.SessionHelper;
import io.sonhar.wallet.util.Utils;
import io.sonhar.wallet.ws.RestServices;
import me.dm7.barcodescanner.core.IViewFinder;
import me.dm7.barcodescanner.core.ViewFinderView;
import me.dm7.barcodescanner.zxing.ZXingScannerView;


public class PagamentoBoletoActivity extends AppCompatActivity implements Validator.ValidationListener, ZXingScannerView.ResultHandler{
    ArrayList<Cartao> cartoes;
    ImageView btnBarCode;
    Saida saida;
    Button btnCadastrar;
    @NotEmpty(messageResId = R.string.campo_obrigatorio)
    EditText edtNomeFavorecido, edtCodBoleto, edtValor;
    @NotEmpty(messageResId = R.string.campo_obrigatorio)
    @Length(min = 10, trim = false, message = "Formato inválido")
    EditText edtDataVencimento;
    AlertDialog alerta;
    Validator validator;
    List<BarcodeFormat> barcodeFormats = new ArrayList<BarcodeFormat>();
    private BarcodeReader barcodeReader;
    private ZXingScannerView mScannerView;
    private FrameLayout frame;
    private static final String TAG = "BarcodeTracker";
    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pagamento_boleto);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        validator = new Validator(this);
        validator.setValidationListener(this);
        btnBarCode = findViewById(R.id.btnBar);
        btnCadastrar = findViewById(R.id.btnCadastrar);
        edtNomeFavorecido = findViewById(R.id.edtNomeFavorecido);
        edtCodBoleto = findViewById(R.id.edtCodBoleto);

        edtValor = (EditText)findViewById(R.id.edtValor);
        edtValor.addTextChangedListener(new MoneyTextWatcher(edtValor));
        btnBarCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogBarCode();

            }
        });
        edtDataVencimento = findViewById(R.id.edtDataVencimento);
        edtDataVencimento.addTextChangedListener(MaskEdit.mask(edtDataVencimento, MaskEdit.FORMAT_DATE));
        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validator.validate();
            }
        });


    }

    Action salvarTransferencia = new Action() {
        @Override
        public void onPostExecute() {
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            if(responseServer.hasError()){
                new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                SessionHelper.getInstance().setResponseServer(null);
            }else if (saida.getId() > 0){
                Utils.showAlertTransferSuccess(App.getUser(getApplicationContext()).getNome(), PagamentoBoletoActivity.this, PagamentoBoletoActivity.this);
            }else{
                Toast.makeText(getApplicationContext(), saida.getError_message(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void run() {
            try {
                RestServices rs = new RestServices(mCtx);
                saida = rs.salvarSaida(saida);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onValidationSucceeded() {
        saida = new Saida();
        saida.setTipo_pagamento("boleto");
        saida.setTipo_favorecido(3);
        saida.setCod_boleto(edtCodBoleto.getText().toString());
        saida.setNome_favorecido(edtNomeFavorecido.getText().toString());
        try {
            saida.setData_vencimento_boleto(Utils.formataData(edtDataVencimento.getText().toString()));
        } catch (Exception e) {
            saida.setData_vencimento_boleto(null);
        }
        double valor;
        String valorStr = edtValor.getText().toString().replaceAll("[.]", "");
        valorStr = valorStr.replaceAll(",",".");
        if (valorStr.isEmpty() )
            valor = 0.0;
        else
            valor = Double.valueOf(valorStr);

        if(valor > 0) {
            saida.setValor(valor);
            ActionTask actionSalvarPagamentoBoleto = new ActionTask(PagamentoBoletoActivity.this, salvarTransferencia, "Aguarde...");
            actionSalvarPagamentoBoleto.execute();
        }else{
            Toast.makeText(getApplicationContext(),R.string.informe_o_valor_txt,Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    public void showDialogBarCode(){
        setup();
        dialog = new Dialog(PagamentoBoletoActivity.this);
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_barcode);
        dialog.setCancelable(true);

        final WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.FILL_PARENT;

        dialog.getWindow().setAttributes((WindowManager.LayoutParams) params);

        mScannerView = new ZXingScannerView(this);
        mScannerView.setFlash(false);
        mScannerView.setAutoFocus(true);

        //mScannerView.setFlash(true);
        frame = dialog.findViewById(R.id.frameCamera);

        frame.addView(mScannerView);
        mScannerView.setResultHandler(this);



        mScannerView.setFormats(barcodeFormats);
        mScannerView.startCamera();

        Button btnOk = (Button)dialog.findViewById(R.id.btnOk);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
        });
        dialog.show();
    }

    public void setup(){
        barcodeFormats.add(BarcodeFormat.UPC_E);
        barcodeFormats.add(BarcodeFormat.AZTEC);
        barcodeFormats.add(BarcodeFormat.CODABAR);
        barcodeFormats.add(BarcodeFormat.CODE_39);
        barcodeFormats.add(BarcodeFormat.CODE_93);
        barcodeFormats.add(BarcodeFormat.CODE_128);
        barcodeFormats.add(BarcodeFormat.DATA_MATRIX);
        barcodeFormats.add(BarcodeFormat.EAN_8);
        barcodeFormats.add(BarcodeFormat.EAN_13);
        barcodeFormats.add(BarcodeFormat.ITF);
        barcodeFormats.add(BarcodeFormat.MAXICODE);
        barcodeFormats.add(BarcodeFormat.PDF_417);
        barcodeFormats.add(BarcodeFormat.QR_CODE);
        barcodeFormats.add(BarcodeFormat.RSS_14);
        barcodeFormats.add(BarcodeFormat.RSS_EXPANDED);
        barcodeFormats.add(BarcodeFormat.UPC_A);
        barcodeFormats.add(BarcodeFormat.UPC_EAN_EXTENSION);

    }

    public void showDialogBarCodeReader(){

        final Dialog dialog = new Dialog(PagamentoBoletoActivity.this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_barcode_reader);
        dialog.setCancelable(true);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.FILL_PARENT;
        dialog.getWindow().setAttributes((WindowManager.LayoutParams) params);

        final CameraSourcePreview mPreview = (CameraSourcePreview) dialog.findViewById(R.id.preview);
        final GraphicOverlay mGraphicOverlay = (GraphicOverlay) dialog.findViewById(R.id.faceOverlay);
        Button btnOk = (Button)dialog.findViewById(R.id.btnOk);

        final BarcodeDetector detector =
                new BarcodeDetector.Builder(getApplicationContext())
                        .setBarcodeFormats(Barcode.EAN_13 | Barcode.CODE_128)
                        .build();
        detector.setFocus(0);

        detector.setProcessor(new Detector.Processor<Barcode>() { @Override public void release() { }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();

                if (barcodes.size() != 0) {
                    Log.i("WALLLET","aeeeee");
                    edtCodBoleto.post(new Runnable() {
                        // Use the post method of the TextView
                        public void run() {
                            Barcode barcode = (Barcode) barcodes.valueAt(0);
                            edtCodBoleto.setText(barcode.displayValue);
//                            mPreview.stop();
                            dialog.dismiss();
                        }
                    });
                }
            }
        });

        final CameraSource mCameraSource = new CameraSource.Builder(PagamentoBoletoActivity.this, detector)
                .setRequestedPreviewSize(640, 480)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setRequestedFps(15.0f)
                .build();



        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                mCameraSource.stop();
            }
        });

        try {
            int rc = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA);
            if (rc == PackageManager.PERMISSION_GRANTED) {
                if (mCameraSource != null) {
                    try {

                        mPreview.start(mCameraSource, mGraphicOverlay);


                    } catch (IOException e) {
                        Log.e(TAG, "Unable to start camera source.", e);
                        mCameraSource.release();

                    }
                }


            } else {
                dialog.dismiss();
                requestCameraPermission();
            }

        } catch (Exception ie) {
            Log.e("CAMERA SOURCE", ie.getMessage());
        }


        dialog.show();
    }

    private static final int RC_HANDLE_CAMERA_PERM = 2;
    private void requestCameraPermission() {
        Log.w(TAG, "Camera permission is not granted. Requesting permission");

        final String[] permissions = new String[]{Manifest.permission.CAMERA};

        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this, permissions, RC_HANDLE_CAMERA_PERM);
            return;
        }

        final Activity thisActivity = this;

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityCompat.requestPermissions(thisActivity, permissions,
                        RC_HANDLE_CAMERA_PERM);
            }
        };


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode != RC_HANDLE_CAMERA_PERM) {
            Log.d(TAG, "Got unexpected permission result: " + requestCode);
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Camera permission granted - initialize the camera source");
            // we have permission, so create the camerasource
            showDialogBarCode();
            return;
        }

        Log.e(TAG, "Permission not granted: results len = " + grantResults.length +
                " Result code = " + (grantResults.length > 0 ? grantResults[0] : "(empty)"));

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        };

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("Face Tracker sample")
                .setMessage(R.string.no_camera_permission)
                .setPositiveButton(R.string.ok, listener)
                .show();
    }


    @Override
    public void handleResult(Result result) {
        edtCodBoleto.setText(result.getText().toString());
        Toast.makeText(getApplicationContext(), result.getBarcodeFormat().toString(), Toast.LENGTH_SHORT).show();
        dialog.dismiss();
    }
}

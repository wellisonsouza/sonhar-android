package io.sonhar.wallet.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import io.sonhar.wallet.R;
import io.sonhar.wallet.model.Cartao;
import io.sonhar.wallet.model.SolicitacaoCartao;

/**
 * Created by wellison on 11/07/2018.
 */

public class SolicitacaoCartaoListAdapter extends ArrayAdapter<SolicitacaoCartao> {
    private List<SolicitacaoCartao> solicitacaoCartaos;
    private Context mCtx;

    public SolicitacaoCartaoListAdapter(Context ctx, List<SolicitacaoCartao> solicitacaoCartaos) {
        super(ctx, R.layout.row_cartao);
        this.mCtx = ctx;
        this.solicitacaoCartaos = solicitacaoCartaos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        Locale ptBr = new Locale("pt", "BR");
        NumberFormat formatter = NumberFormat.getCurrencyInstance(ptBr);
        final SolicitacaoCartao solicitacaoCartao = solicitacaoCartaos.get(position);
        view = LayoutInflater.from(mCtx).inflate(R.layout.row_solicitacao_cartao, parent, false);

        TextView lblStatus = (TextView) view.findViewById(R.id.lblStatus);
        TextView lblData = (TextView) view.findViewById(R.id.lblData);

        lblStatus.setText(solicitacaoCartao.getStatus().replace("_"," ").toUpperCase());
        lblData.setText(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(solicitacaoCartao.getCreated_at()));

        return view;
    }

    @Override
    public SolicitacaoCartao getItem(int position) {
        return solicitacaoCartaos.get(position);
    }

    @Override
    public int getCount() {
        return solicitacaoCartaos.size();
    }

}

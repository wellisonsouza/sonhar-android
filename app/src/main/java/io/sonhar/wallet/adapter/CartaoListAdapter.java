package io.sonhar.wallet.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import io.sonhar.wallet.R;
import io.sonhar.wallet.model.Cartao;

/**
 * Created by wellison on 11/07/2018.
 */

public class CartaoListAdapter extends ArrayAdapter<Cartao> {
    private List<Cartao> cartoes;
    private Context mCtx;

    public CartaoListAdapter(Context ctx, List<Cartao> cartoes) {
        super(ctx, R.layout.row_cartao);
        this.mCtx = ctx;
        this.cartoes = cartoes;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        Locale ptBr = new Locale("pt", "BR");
        NumberFormat formatter = NumberFormat.getCurrencyInstance(ptBr);
        final Cartao cartao = cartoes.get(position);
        view = LayoutInflater.from(mCtx).inflate(R.layout.row_cartao, parent, false);
        ViewHolder holder = new ViewHolder();
        holder.txtCartaoNomeTitular = (TextView) view.findViewById(R.id.txtCartaoNomeTitular);
        holder.txtCartaoNumero = (TextView) view.findViewById(R.id.txtCartaoNumero);
        //holder.txtSaldo = (TextView) view.findViewById(R.id.txtSaldo);
        view.setTag(holder);
       // holder.txtSaldo.setText(formatter.format(Double.parseDouble(cartao.getSaldo())));
        holder.txtCartaoNumero.setText(cartao.getNumero());
        holder.txtCartaoNomeTitular.setText(cartao.getNome_impresso());
//        if (Double.parseDouble(cartao.getSaldo()) < 0){
//            holder.txtSaldo.setTextColor(Color.parseColor("#f67f72"));
//        }

        return view;
    }

    @Override
    public Cartao getItem(int position) {
        return cartoes.get(position);
    }

    @Override
    public int getCount() {
        return cartoes.size();
    }

    private class ViewHolder {
        TextView txtCartaoNumero;
        TextView txtCartaoNomeTitular;
        TextView txtSaldo;
    }
}

package io.sonhar.wallet.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.edwardvanraak.materialbarcodescanner.MaterialBarcodeScanner;
import com.edwardvanraak.materialbarcodescanner.MaterialBarcodeScannerBuilder;
import com.google.android.gms.vision.barcode.Barcode;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import io.sonhar.wallet.R;
import io.sonhar.wallet.adapter.CardRowSpinnerAdapter;
import io.sonhar.wallet.app.App;
import io.sonhar.wallet.app.FaceValidationActivity;
import io.sonhar.wallet.app.MainActivity;
import io.sonhar.wallet.app.MainActivity_old;
import io.sonhar.wallet.model.Card;
import io.sonhar.wallet.util.Action;
import io.sonhar.wallet.util.ActionTask;
import io.sonhar.wallet.util.DateTextWatcher;
import io.sonhar.wallet.util.MoneyTextWatcher;
import io.sonhar.wallet.util.PhoneTextWatcher;
import io.sonhar.wallet.ws.RestServices;

/**
 * Created by PedroPontes1 on 18/06/2018. 5496 3000 0007 6392
 */

public class PaymentFragment extends Fragment implements Fragmento{

    Boolean verifingFace = false;
    public Dialog dialogBoleto;
    public Dialog dialogRecarga;
    LinearLayout btnRecarga;
    LinearLayout btnBoleto;
    MainActivity main;
    AlertDialog alerta;
    public Context mCtxActivity;
    List<Card> cards = new ArrayList<Card>();
    Fragment f;
    private Card cardSelected;
    public PaymentFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment, container, false);

        f = this;

        btnRecarga = (LinearLayout) view.findViewById(R.id.btnRecarga);
        btnBoleto = (LinearLayout) view.findViewById(R.id.btnBoleto);

        btnRecarga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActionTask actionCardsTask = new ActionTask(getActivity(), cardAction, "Aguarde..");
                cardAction.param = App.getUser(getActivity().getApplicationContext()).getId().toString();
                cardAction.param2 = "recarga";
                actionCardsTask.execute();
            }
        });
        btnBoleto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActionTask actionCardsTask = new ActionTask(getActivity(), cardAction, "Aguarde..");
                cardAction.param = App.getUser(getActivity().getApplicationContext()).getId().toString();
                cardAction.param2 = "boleto";
                actionCardsTask.execute();
            }
        });
        return view;
    }

    @Override
    public void reload(Context c) {
//        ActionTask actionCardsTask = new ActionTask(c, cardAction, "Aguarde..");
//        cardAction.param = App.getUser(c).getId().toString();
//        actionCardsTask.execute();
    }
    @Override
    public void setMainActivity(MainActivity main) {
        this.main = main;
    }

    Action cardAction = new Action() {
        @Override
        public void onPostExecute() {
            // TODO Auto-generated method stub
            Log.i("RETORNO", "OK");
            try {

                if(param2.equalsIgnoreCase("boleto")){
                    showBoletoDialog();
                }else{
                    showRecargaDialog();
                }
            }catch(Exception ex){
                ex.printStackTrace();
            }

        }
        @Override
        public void run () {
            try {

                RestServices rs = new RestServices();

                cards = rs.getCards(param);


            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };


    private void showBoletoDialog(){
        dialogBoleto = new Dialog(getActivity());
        cardSelected = new Card();
        dialogBoleto.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogBoleto.setContentView(R.layout.dialog_pay_boleto);
        WindowManager.LayoutParams params = dialogBoleto.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.FILL_PARENT;
        dialogBoleto.getWindow().setAttributes((WindowManager.LayoutParams) params);

        Button btnCancel = (Button)dialogBoleto.findViewById(R.id.btnCancel);
        Button btnOk = (Button)dialogBoleto.findViewById(R.id.btnOk);
        final EditText edtBarcode = (EditText)dialogBoleto.findViewById(R.id.edtBarCode);
        final EditText edtVencimento = (EditText)dialogBoleto.findViewById(R.id.edtVencimento);
        ImageView btnBarcode = (ImageView)dialogBoleto.findViewById(R.id.btnBarCode);

        final ImageView imgQrCode = (ImageView)dialogBoleto.findViewById(R.id.imgQrCode);
        Spinner spnCards = (Spinner) dialogBoleto.findViewById(R.id.spnCards);

        edtVencimento.addTextChangedListener(new DateTextWatcher(edtVencimento));


        CardRowSpinnerAdapter adapter = new CardRowSpinnerAdapter(getActivity(),cards);
        spnCards.setAdapter(adapter);

        spnCards.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                try{
                    cardSelected = cards.get(pos);
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        btnBarcode.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startScan(edtBarcode);
            }
        });



        btnCancel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                dialogBoleto.dismiss();
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                // AEEEEE

                if (edtBarcode.getText().toString().length() > 5){
                    String valor = edtBarcode.getText().toString();
                    if (valor.length() > 0) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle("ATENÇÃO");
                        builder.setMessage(" - Posicione o o smartphone a sua frente.\n - Certifique-se que esta em um lugar bem iluminado.\n - Não utilize Oculos ou qualquer acessório no rosto.");

                        builder.setPositiveButton("CONTINUAR", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                                alerta.dismiss();
                                MainActivity_old.type_faceCamera = 2;
                                Intent pickFaceValidation = new Intent(getActivity().getApplicationContext(), FaceValidationActivity.class);
                                startActivityForResult(pickFaceValidation, 12);
                            }
                        });
                        builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                                alerta.dismiss();
                            }
                        });
                        alerta = builder.create();
                        alerta.show();
                    }else{
                        Toast.makeText(getActivity(),"Informe o codigo de barras",Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getActivity(),"Informe um cartão de destino valido",Toast.LENGTH_SHORT).show();
                }
            }
        });



        dialogBoleto.show();
    }

    private void startScan(final EditText edtBarCode) {
        /**
         * Build a new MaterialBarcodeScanner
         */
        final MaterialBarcodeScanner materialBarcodeScanner = new MaterialBarcodeScannerBuilder()
                .withActivity(main)
                .withEnableAutoFocus(true)
                .withBleepEnabled(false)
                .withBackfacingCamera()
                .withText("Posicione o código de barras")
                .withTrackerColor(Color.WHITE)
                .withResultListener(new MaterialBarcodeScanner.OnResultListener() {
                    @Override
                    public void onResult(Barcode barcode) {
//                        barcodeResult = barcode;
                        edtBarCode.setText(barcode.rawValue);
                    }
                })
                .build();
        materialBarcodeScanner.startScan();
    }
    private void showRecargaDialog(){
        dialogRecarga = new Dialog(getActivity());
        cardSelected = new Card();
        dialogRecarga.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogRecarga.setContentView(R.layout.dialog_recharge_phone);
        WindowManager.LayoutParams params = dialogRecarga.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.FILL_PARENT;
        dialogRecarga.getWindow().setAttributes((WindowManager.LayoutParams) params);

        Button btnCancel = (Button)dialogRecarga.findViewById(R.id.btnCancel);
        Button btnOk = (Button)dialogRecarga.findViewById(R.id.btnOk);
        final EditText edtPhone = (EditText)dialogRecarga.findViewById(R.id.edtPhone);
        final EditText edtValor = (EditText)dialogRecarga.findViewById(R.id.edtValor);

        Spinner spnCards = (Spinner) dialogRecarga.findViewById(R.id.spnCards);

        edtPhone.addTextChangedListener(new PhoneTextWatcher(new WeakReference<EditText>(edtPhone)));
        edtValor.addTextChangedListener(new MoneyTextWatcher(edtValor));

        CardRowSpinnerAdapter adapter = new CardRowSpinnerAdapter(getActivity(),cards);
        spnCards.setAdapter(adapter);

        spnCards.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                try{
                    cardSelected = cards.get(pos);
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                dialogRecarga.dismiss();
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                // AEEEEE

                if (edtPhone.getText().toString().length() > 14){
                    String valor = edtValor.getText().toString();
                    if (valor.length() > 0) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle("ATENÇÃO");
                        builder.setMessage(" - Posicione o o smartphone a sua frente.\n - Certifique-se que esta em um lugar bem iluminado.\n - Não utilize Oculos ou qualquer acessório no rosto.");

                        builder.setPositiveButton("CONTINUAR", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                                alerta.dismiss();
                                MainActivity_old.type_faceCamera = 3;
                                Intent pickFaceValidation = new Intent(getActivity().getApplicationContext(), FaceValidationActivity.class);
                                startActivityForResult(pickFaceValidation, 12);
                            }
                        });
                        builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                                alerta.dismiss();
                            }
                        });
                        alerta = builder.create();
                        alerta.show();
                    }else{
                        Toast.makeText(getActivity(),"Informe o valor da recarga",Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getActivity(),"Informe um celular valido",Toast.LENGTH_SHORT).show();
                }
            }
        });



        dialogRecarga.show();
    }

    @Override
    public String getTittle() {
        return getActivity().getApplicationContext().getResources().getString(R.string.cartoes_txt);
    }
}

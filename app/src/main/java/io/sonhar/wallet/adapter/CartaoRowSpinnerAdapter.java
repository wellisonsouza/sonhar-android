package io.sonhar.wallet.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import io.sonhar.wallet.R;
import io.sonhar.wallet.model.Cartao;

/**
 * Created by PedroPontes1 on 17/06/2018.
 */

public class CartaoRowSpinnerAdapter extends ArrayAdapter<Cartao> {
    private List<Cartao> cartaos;
    private Context mCtx;

    public CartaoRowSpinnerAdapter(Context ctx, List<Cartao> cartaos) {
        super(ctx, R.layout.row_spinner_card);
        this.mCtx = ctx;
        this.cartaos = cartaos;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public @NonNull View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent){
        View view = convertView;

        view = LayoutInflater.from(mCtx).inflate(R.layout.row_spinner_card, parent, false);
        //view.setLayoutParams(new Carousel.LayoutParams(70, 70));

        ViewHolder holder = new ViewHolder();
        holder.lblNumCard = (TextView) view.findViewById(R.id.lblNumCard);
        holder.lblHolder = (TextView) view.findViewById(R.id.lblHolder);
        view.setTag(holder);
        final Cartao cartao = cartaos.get(position);

        Editable s = new SpannableStringBuilder(cartao.getNumero());
        for (int i = 4; i < s.length(); i += 5) {
            if (s.toString().charAt(i) != ' ') {
                s.insert(i, " ");
            }

        }

        holder.lblNumCard.setText(s.toString());
        holder.lblHolder.setText(cartao.getNome_impresso());

        return view;
    }


    private class ViewHolder {
        TextView lblNumCard;
        TextView lblHolder;
    }

    @Override
    public Cartao getItem(int position) {
        return cartaos.get(position);
    }

    @Override
    public int getCount() {
        return cartaos.size();
    }

}


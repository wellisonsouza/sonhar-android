package io.sonhar.wallet.fragment;

import android.content.Context;

import io.sonhar.wallet.app.MainActivity;

/**
 * Created by PedroPontes1 on 15/06/16.
 */
public interface Fragmento {

    public String getTittle();

    public void reload(Context c);

    public void setMainActivity(MainActivity main);

}

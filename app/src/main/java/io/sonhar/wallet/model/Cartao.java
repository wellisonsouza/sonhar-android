package io.sonhar.wallet.model;

import java.util.ArrayList;

/**
 * Created by wellison on 11/07/2018.
 */

public class Cartao {
    private long id;
    private String tipo, numero_conta_corrente, digito, nome_impresso, numero, saldo, saldo_cartao, proxy;
    private ArrayList<ExtratoCartao> extratoCartaos;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNumero_conta_corrente() {
        return numero_conta_corrente;
    }

    public void setNumero_conta_corrente(String numero_conta_corrente) {
        this.numero_conta_corrente = numero_conta_corrente;
    }

    public String getDigito() {
        return digito;
    }

    public void setDigito(String digito) {
        this.digito = digito;
    }

    public String getNome_impresso() {
        return nome_impresso;
    }

    public void setNome_impresso(String nome_impresso) {
        this.nome_impresso = nome_impresso;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    public String getSaldo_cartao() {
        return saldo_cartao;
    }

    public void setSaldo_cartao(String saldo_cartao) {
        this.saldo_cartao = saldo_cartao;
    }

    public String getProxy() {
        return proxy;
    }

    public void setProxy(String proxy) {
        this.proxy = proxy;
    }

    public ArrayList<ExtratoCartao> getExtratoCartaos() {
        return extratoCartaos;
    }

    public void setExtratoCartaos(ArrayList<ExtratoCartao> extratoCartaos) {
        this.extratoCartaos = extratoCartaos;
    }
}

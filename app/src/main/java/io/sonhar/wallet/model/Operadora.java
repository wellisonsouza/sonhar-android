package io.sonhar.wallet.model;

/**
 * Created by wellison on 25/07/2018.
 */

public class Operadora {
    private String nome_operadora, codigo_operadora;

    public String getNome_operadora() {
        return nome_operadora;
    }

    public void setNome_operadora(String nome_operadora) {
        this.nome_operadora = nome_operadora;
    }

    public String getCodigo_operadora() {
        return codigo_operadora;
    }

    public void setCodigo_operadora(String codigo_operadora) {
        this.codigo_operadora = codigo_operadora;
    }
}

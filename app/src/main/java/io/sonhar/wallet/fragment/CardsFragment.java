package io.sonhar.wallet.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.sonhar.wallet.R;
import io.sonhar.wallet.adapter.CartaoListAdapter;
import io.sonhar.wallet.app.AdicionarCartaoActivity;
import io.sonhar.wallet.app.CartaoActivity;
import io.sonhar.wallet.app.ExtratoCartaoActivity;
import io.sonhar.wallet.app.MainActivity;
import io.sonhar.wallet.app.TransferenciaCartaoActivity;
import io.sonhar.wallet.model.Cartao;
import io.sonhar.wallet.model.ResponseServer;
import io.sonhar.wallet.model.ResultServer;
import io.sonhar.wallet.util.Action;
import io.sonhar.wallet.util.ActionTask;
import io.sonhar.wallet.util.DoneOnEditorActionListener;
import io.sonhar.wallet.util.ErrorHelper;
import io.sonhar.wallet.util.MaskEdit;
import io.sonhar.wallet.util.SessionHelper;
import io.sonhar.wallet.ws.RestServices;

/**
 * Created by PedroPontes1 on 18/06/2018. 5496 3000 0007 6392
 */

public class CardsFragment extends Fragment implements Fragmento{

    ListView lvCartoes;
    Cartao cartao;
    MainActivity main;
    ArrayList<Cartao> cartoes;
    FloatingActionButton btnNovoCartao;
//    LinearLayout layoutEmpty;
    TextView textEmpty;

    @BindView(R.id.bottom_sheet) LinearLayout layoutBottomSheet;

    BottomSheetBehavior sheetBehavior;
    EditText edtNumCard, edtValidade, edtCvv;
    TextView lblNumCard;
    Button btnCadastrar;
    String num_cartao, cvv, validade;
    public CardsFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cards, container, false);
        ButterKnife.bind(this,view);
        edtNumCard = view.findViewById(R.id.edtNumCard);
        lblNumCard = (TextView) view.findViewById(R.id.lblNumCard);
        textEmpty = view.findViewById(R.id.textEmpty);
        edtValidade = view.findViewById(R.id.edtValidade);
        edtValidade.addTextChangedListener(MaskEdit.mask(edtValidade, MaskEdit.EXPIRATION_DATE2));

        edtCvv = view.findViewById(R.id.edtCvv);
        edtNumCard.clearFocus();
        edtNumCard.addTextChangedListener(new TextWatcher() {
            private boolean lock;
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                lblNumCard.setText(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (lock || s.length() > 16) {
                    return;
                }
                lock = true;
                for (int i = 4; i < s.length(); i += 5) {
                    if (s.toString().charAt(i) != ' ') {
                        s.insert(i, " ");
                    }

                }
                lock = false;
            }
        });
        edtNumCard.setOnEditorActionListener(new DoneOnEditorActionListener());
        btnCadastrar = (Button) view.findViewById(R.id.btnCadastrar);
        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);

        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        btnNovoCartao = view.findViewById(R.id.fabNewCard);
        btnNovoCartao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), AdicionarCartaoActivity.class));

//                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                num_cartao = edtNumCard.getText().toString().replaceAll("\\s+","");
                validade = edtValidade.getText().toString();
                cvv = edtCvv.getText().toString();
            }
        });

        lvCartoes = (ListView) view.findViewById(R.id.lvCartoes);
        ActionTask actionLoadCartoes = new ActionTask(getActivity(), getCartoes, getResources().getString(R.string.aguarde_txt));
        actionLoadCartoes.execute();
        lvCartoes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Cartao c = cartoes.get(i);
                showOptionsDialog(c.getNumero(), c.getNome_impresso(), c.getProxy());
            }
        });

        return view;
    }

    @Override
    public void reload(Context c) {
        ActionTask actionLoadCartoes = new ActionTask(getActivity(), getCartoes, getResources().getString(R.string.aguarde_txt));
        actionLoadCartoes.execute();
    }
    @Override
    public void setMainActivity(MainActivity main) {
        this.main = main;
    }

    private void showOptionsDialog(final String numCard, final String holder, final String proxy){
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_menu_cards);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes((WindowManager.LayoutParams) params);

        TextView lblNumCard = (TextView)dialog.findViewById(R.id.lblNumCard);
        TextView lblHolder = (TextView)dialog.findViewById(R.id.lblHolder);
        Button btnRecarga = (Button)dialog.findViewById(R.id.btnRecarga);
        Button btnExtrato = (Button)dialog.findViewById(R.id.btnExtrato);

        lblHolder.setText(holder);
        lblNumCard.setText(numCard);

        btnExtrato.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(getActivity(), ExtratoCartaoActivity.class);
                myIntent.putExtra("proxy", proxy);
                myIntent.putExtra("numCard", numCard);
                myIntent.putExtra("holder", holder);
                startActivity(myIntent);
            }
        });

        btnRecarga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(getActivity(), TransferenciaCartaoActivity.class);
                myIntent.putExtra("proxy", proxy);
                myIntent.putExtra("numCard", numCard);
                myIntent.putExtra("holder", holder);
                startActivity(myIntent);
//                Toast.makeText(getContext(), "Essa opção estará disponível em breve", Toast.LENGTH_SHORT).show();
            }
        });

        dialog.show();

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                ActionTask actionLoadCartoes = new ActionTask(getActivity(), getCartoes, getResources().getString(R.string.aguarde_txt));
                actionLoadCartoes.execute();
            }
        });
    }

    Action getCartoes = new Action() {
        Locale ptBr = new Locale("pt", "BR");
        NumberFormat formatter = NumberFormat.getCurrencyInstance(ptBr);

        @Override
        public void onPostExecute() {
            // TODO Auto-generated method stub
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            try {
                if(responseServer.hasError()){
                    new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                    SessionHelper.getInstance().setResponseServer(null);
                }

                CartaoListAdapter adapter = new CartaoListAdapter(mCtx, cartoes);
                lvCartoes.setAdapter(adapter);
                adapter.notifyDataSetChanged();

                if(cartoes.size() == 0)
                    textEmpty.setVisibility(View.VISIBLE);
                else
                    textEmpty.setVisibility(View.GONE);


            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        @Override
        public void run() {
            RestServices rs = new RestServices(mCtx);
            cartoes = rs.getCartoes();
        }

    };


    @Override
    public String getTittle() {
        return getActivity().getApplicationContext().getResources().getString(R.string.cartoes_txt);
    }
}

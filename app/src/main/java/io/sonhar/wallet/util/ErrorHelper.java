package io.sonhar.wallet.util;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import io.sonhar.wallet.app.App;
import io.sonhar.wallet.app.EnviarDocumentacaoActivity;
import io.sonhar.wallet.app.LoginActivity;
import io.sonhar.wallet.app.SolicitacaoAnaliseDocumentoActivity;

/**
 * Created by wellison on 30/01/2018.
 */

public class ErrorHelper {
    public void showErrorMessage(Context context, int code){
        String message = null;
        if (code == 0 ) {
            message = Msg.ERROR_NETWORK;
        }else if(code == 401){
            message = Msg.USER_DENIED;
            App.logout(context);
            Intent intent = new Intent(context, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(intent);
        }else if(code == 412){
            message = "É necessário enviar documentos de comprovação da conta";
            Intent intent = new Intent(context, SolicitacaoAnaliseDocumentoActivity.class);
            context.startActivity(intent);
        }
        else {
            message = Msg.ERROR_DEFAULT;
        }
        Toast.makeText(context, message +" #" +code, Toast.LENGTH_SHORT).show();
    }
}

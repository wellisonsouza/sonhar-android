package io.sonhar.wallet.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import io.sonhar.wallet.R;
import io.sonhar.wallet.adapter.ExtratoItemAdapter;
import io.sonhar.wallet.app.MainActivity;
import io.sonhar.wallet.model.Extrato;
import io.sonhar.wallet.model.ResponseServer;
import io.sonhar.wallet.model.Saldo;
import io.sonhar.wallet.util.Action;
import io.sonhar.wallet.util.ActionTask;
import io.sonhar.wallet.util.ErrorHelper;
import io.sonhar.wallet.util.SessionHelper;
import io.sonhar.wallet.ws.RestServices;

/**
 * Created by PedroPontes1 on 18/06/2018.
 */

public class TimeLineFragment extends Fragment implements Fragmento, AbsListView.OnScrollListener{

    MainActivity main;
    ListView lvExtratos;
    Saldo saldo;
    ArrayList<Extrato> extratos;
    TextView txtSaldoLiberado;
    ExtratoItemAdapter adapter;
    private boolean loading = false;
    private boolean loadedAllItens = false;
    private int page = 1;
    private Handler handler;
//    LinearLayout layoutEmpty;
    TextView textEmpty;

    public TimeLineFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_timeline, container, false);

        lvExtratos = (ListView) view.findViewById(R.id.lvExtratos);
        lvExtratos.setOnScrollListener(this);
        textEmpty = view.findViewById(R.id.textEmpty);
        txtSaldoLiberado = (TextView) view.findViewById(R.id.txtSaldoLiberado);
        getExtratosServices();
        return view;
    }

    @Override
    public void reload(Context c) {
        ActionTask actionLoadSaldo = new ActionTask(getActivity().getApplicationContext(), getSaldo, getResources().getString(R.string.aguarde_txt));
        actionLoadSaldo.execute();
    }

    public void getExtratosServices(){
        ActionTask actionLoadExtratos = new ActionTask(getActivity(), getExtratos, getResources().getString(R.string.aguarde_txt));
        actionLoadExtratos.execute();
    }

    @Override
    public void setMainActivity(MainActivity main) {
        this.main = main;
    }

    Action getExtratos = new Action() {
        Locale ptBr = new Locale("pt", "BR");
        NumberFormat formatter = NumberFormat.getCurrencyInstance(ptBr);

        @Override
        public void onPostExecute() {
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();

            // TODO Auto-generated method stub
            try {
                if(responseServer.hasError()){
                    new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                    SessionHelper.getInstance().setResponseServer(new ResponseServer());
                }else{
                    if(extratos.size() < 10){
                        loadedAllItens = true;
                    }
                    if(page == 1){
                        adapter = new ExtratoItemAdapter(mCtx, extratos);
                        lvExtratos.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }else{
                        adapter.add(extratos);
                        loading = false;
                    }

                    if(adapter.getCount() == 0)
                        textEmpty.setVisibility(View.VISIBLE);
                    else
                        textEmpty.setVisibility(View.GONE);
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        @Override
        public void run() {
            RestServices rs = new RestServices(mCtx);
            extratos = rs.getExtratoWallet(page);
        }

    };

    Action getSaldo = new Action() {
        Locale ptBr = new Locale("pt", "BR");
        NumberFormat formatter = NumberFormat.getCurrencyInstance(ptBr);

        @Override
        public void onPostExecute() {
            // TODO Auto-generated method stub
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            try {
                if(responseServer.hasError()){
                    new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                    SessionHelper.getInstance().setResponseServer(new ResponseServer());
                }else if (saldo != null){
                    txtSaldoLiberado.setText(formatter.format(saldo.getValorEmReais(saldo.getValor_disponivel_centavos())));
                }else{
                    Toast.makeText(mCtx, "Não foi possível carregar o saldo", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        @Override
        public void run() {
            RestServices rs = new RestServices(mCtx);
            saldo = rs.getSaldo();
        }

    };

    @Override
    public String getTittle() {
        return getActivity().getApplicationContext().getResources().getString(R.string.historico_transacoes_txt);
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (lvExtratos.getAdapter() == null)
            return ;

        if (lvExtratos.getAdapter().getCount() == 0)
            return ;
        int l = visibleItemCount + firstVisibleItem;
        if (l >= totalItemCount && !loading && !loadedAllItens) {
            loading = true;
            page++;
            getExtratosServices();
        }
    }
}

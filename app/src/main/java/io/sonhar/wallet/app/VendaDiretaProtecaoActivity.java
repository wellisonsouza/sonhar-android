package io.sonhar.wallet.app;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import io.sonhar.wallet.R;
import io.sonhar.wallet.model.ResponseServer;
import io.sonhar.wallet.model.ResultServer;
import io.sonhar.wallet.model.VendaDireta;
import io.sonhar.wallet.util.Action;
import io.sonhar.wallet.util.ActionTask;
import io.sonhar.wallet.util.ErrorHelper;
import io.sonhar.wallet.util.ImageUtil;
import io.sonhar.wallet.util.SessionHelper;
import io.sonhar.wallet.util.Utils;
import io.sonhar.wallet.ws.RestServices;

public class VendaDiretaProtecaoActivity extends AppCompatActivity {
    static final int REQUEST_IMAGE_CAPTURE = 1;
    Button btnFoto, btnAssinatura, btnCadastrar;
    SignaturePad mSignaturePad;
    Bitmap imgAssinatura, imgFoto;
    String transaction_code;
    VendaDireta vendaDireta;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venda_direta_protecao);
        transaction_code = getIntent().getStringExtra("transaction_code");
        btnFoto = findViewById(R.id.btnFoto);
        btnFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkCameraPermission();
            }
        });

        btnAssinatura = findViewById(R.id.btnAssinatura);
        btnAssinatura.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showModalAssinatura();
            }
        });

        btnCadastrar = findViewById(R.id.btnCadastrar);
        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkImagens();
            }
        });
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private void checkImagens(){
        if (imgAssinatura == null){
            Toast.makeText( VendaDiretaProtecaoActivity.this, getResources().getString(R.string.assinatura_pagador), Toast.LENGTH_SHORT).show();
        }else if(imgFoto == null){
            Toast.makeText( VendaDiretaProtecaoActivity.this, getResources().getString(R.string.foto_pagador), Toast.LENGTH_SHORT).show();
        }else{
            enviarImagens();
       }
    }

    private void enviarImagens(){
        vendaDireta = new VendaDireta();
        vendaDireta.setTransaction_code(transaction_code);
        vendaDireta.setAssinatura_pagador(ImageUtil.convert(imgAssinatura));
        vendaDireta.setFoto_pagador(ImageUtil.convert(imgFoto));
        ActionTask actionSalvarTransferencia = new ActionTask(VendaDiretaProtecaoActivity.this, enviarAssinaturaFoto, getString(R.string.aguarde_txt));
        actionSalvarTransferencia.execute();
    }

    Action enviarAssinaturaFoto = new Action() {
        ResultServer resultServer;
        @Override
        public void onPostExecute() {
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            if(responseServer.hasError()){
                new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                SessionHelper.getInstance().setResponseServer(null);
            }else if (resultServer.getSuccess()){
                Utils.showAlertTransferSuccess(App.getUser(getApplicationContext()).getNome(), VendaDiretaProtecaoActivity.this, VendaDiretaProtecaoActivity.this);
            }else{
                Toast.makeText(getApplicationContext(), resultServer.getMessage(), Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void run() {
            try {
                RestServices rs = new RestServices(mCtx);
                resultServer = rs.enviarFotosProtecao(vendaDireta);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };

    public void checkCameraPermission(){
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.CAMERA)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        dispatchTakePictureIntent();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        if (response.isPermanentlyDenied()) {
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void showModalAssinatura(){
        final Dialog dialog = new Dialog(VendaDiretaProtecaoActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_assinatura);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes((WindowManager.LayoutParams) params);
        final Button btnSalvar = dialog.findViewById(R.id.btnSalvar);
        Button btnCancelar = dialog.findViewById(R.id.btnCancelar);
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgAssinatura = mSignaturePad.getSignatureBitmap();
                dialog.dismiss();
                String base64String = ImageUtil.convert(imgAssinatura);
                Log.d("base", base64String);
            }
        });

        mSignaturePad = (SignaturePad) dialog.findViewById(R.id.signature_pad);
        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {

            @Override
            public void onStartSigning() {
                //Event triggered when the pad is touched
            }

            @Override
            public void onSigned() {
                //Event triggered when the pad is signed
                btnAssinatura.setCompoundDrawablesWithIntrinsicBounds(getDrawable(R.drawable.ic_signature_ok), null, null, null);
                btnSalvar.setEnabled(true);
            }

            @Override
            public void onClear() {
                //Event triggered when the pad is cleared
            }
        });

        dialog.show();

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            imgFoto = (Bitmap) extras.get("data");
            btnFoto.setCompoundDrawablesWithIntrinsicBounds(getDrawable(R.drawable.ic_take_a_photo_ok), null, null, null);
            String base64String = ImageUtil.convert(imgFoto);
            Log.d("base", base64String);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            Toast.makeText( VendaDiretaProtecaoActivity.this, getResources().getString(R.string.finalize_a_venda_para_voltar), Toast.LENGTH_SHORT).show();

            return false;
        }
        return super.onKeyDown(keyCode, event);
    }
}


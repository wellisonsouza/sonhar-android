package io.sonhar.wallet.app;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.zxing.WriterException;

import io.sonhar.wallet.R;
import io.sonhar.wallet.model.User;
import io.sonhar.wallet.util.Utils;

public class TransferenciaActivity extends AppCompatActivity {

    LinearLayout btnTransferir, btnReceber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transferencia);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        btnTransferir = findViewById(R.id.btnTransferir);
        btnReceber = findViewById(R.id.btnReceber);
        btnTransferir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showOptionsDialog();
            }
        });
        btnReceber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exibirQrCodeParaReceberTransferencia();
            }
        });
    }

    private void exibirQrCodeParaReceberTransferencia(){
        User userlogged = App.getUser(this);
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_receive_money);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.FILL_PARENT;
        dialog.getWindow().setAttributes((WindowManager.LayoutParams) params);

        Button btnCancel = (Button)dialog.findViewById(R.id.btnOk);

        final ImageView imgQrCode = (ImageView)dialog.findViewById(R.id.imgQrCode);
        Bitmap qrcode= null;
        try {
            qrcode = Utils.qrcodeAsBitmap("C:" + userlogged.getCurrent_conta_id(), 256);
        } catch (WriterException e) {
            e.printStackTrace();
        }
        imgQrCode.setImageBitmap(qrcode);

        btnCancel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void showOptionsDialog(){
        final Dialog dialog = new Dialog(TransferenciaActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_menu_transfers);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes((WindowManager.LayoutParams) params);

        Button btnContaBancaria = (Button)dialog.findViewById(R.id.btnContaBancaria);
        Button btnContaWallet = (Button)dialog.findViewById(R.id.btnContaWallet);

        btnContaBancaria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(TransferenciaActivity.this, TransferenciaBancariaActivity.class);
                startActivity(myIntent);
                dialog.dismiss();
            }
        });

        btnContaWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(TransferenciaActivity.this, TransferenciaWalletActivity.class);
                startActivity(myIntent);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return (super.onOptionsItemSelected(menuItem));
    }
}

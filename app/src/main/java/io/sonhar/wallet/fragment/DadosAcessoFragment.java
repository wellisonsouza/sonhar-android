package io.sonhar.wallet.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import io.sonhar.wallet.R;

/**
 * Created by wellison on 03/08/2018.
 */

public class DadosAcessoFragment extends Fragment {
    private Context context;
    private View view;

    public static DadosAcessoFragment newInstance(){
        DadosAcessoFragment fragment = new DadosAcessoFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        this.context =context;

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup parent, Bundle savedState){
        view =inflater.inflate(R.layout.fragment_dados_acesso, parent, false);
        return view;
    }
}

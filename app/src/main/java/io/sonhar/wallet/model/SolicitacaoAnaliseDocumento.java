package io.sonhar.wallet.model;

import java.util.ArrayList;
import java.util.Date;

public class SolicitacaoAnaliseDocumento {
    private int id;
    private String status;
    private Date created_at;

    ArrayList<Documento> documentos = new ArrayList<Documento>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public ArrayList<Documento> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(ArrayList<Documento> documentos) {
        this.documentos = documentos;
    }

    public void addDocumento(Documento documento){
        this.documentos.add(documento);
    }
}

package io.sonhar.wallet.ws;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import io.sonhar.wallet.app.App;
import io.sonhar.wallet.model.Banco;
import io.sonhar.wallet.model.Card;
import io.sonhar.wallet.model.Cartao;
import io.sonhar.wallet.model.Documento;
import io.sonhar.wallet.model.EmissaoBoleto;
import io.sonhar.wallet.model.Entrada;
import io.sonhar.wallet.model.EntradaParcela;
import io.sonhar.wallet.model.Extrato;
import io.sonhar.wallet.model.ExtratoCartao;
import io.sonhar.wallet.model.Operadora;
import io.sonhar.wallet.model.PagamentoBoleto;
import io.sonhar.wallet.model.ParcelaCartao;
import io.sonhar.wallet.model.Recarga;
import io.sonhar.wallet.model.RecargaTelefone;
import io.sonhar.wallet.model.RechargePoint;
import io.sonhar.wallet.model.ResultServer;
import io.sonhar.wallet.model.Saida;
import io.sonhar.wallet.model.Saldo;
import io.sonhar.wallet.model.SolicitacaoAnaliseDocumento;
import io.sonhar.wallet.model.SolicitacaoCartao;
import io.sonhar.wallet.model.User;
import io.sonhar.wallet.model.ValorRecarga;
import io.sonhar.wallet.model.VendaDireta;
import io.sonhar.wallet.util.ApiCep;
import io.sonhar.wallet.util.DateDeserializer;
import io.sonhar.wallet.util.ExtratoLinha;
import io.sonhar.wallet.util.ExtratoSaldo;
import io.sonhar.wallet.util.Utils;


public class RestServices {
    private String urlServer = "https://apiwallet.vivipay.io:3001/api/v1";
    private String urlGateway = "http://ne-vivipay-sub-gateway.us-east-1.elasticbeanstalk.com/api/v1";
//    private String urlServer = "http://192.168.10.21:3000/api/v1";
//    private String urlGateway = "http://192.168.10.21:3001/api/v1";
    boolean network = false;

    Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
    private Context mCtx;
    public RestServices(){

    }
    public RestServices(Context context){
        mCtx=context;
    }

    public ResultServer gerarCodigoVerificacao(String numeroTelefone){
        String host = urlServer+"/sms_verification_codes";
        ResultServer result = new ResultServer();
        Rest webService = new Rest(host);
        User userlogged = App.getUser(mCtx);
        try {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("sms_verification_code[numero_telefone]", "+55"+numeroTelefone.replaceAll("[^0-9]", "")));
            String response = webService.doPostAuth(host,params,userlogged);
            result = gson.fromJson(response,ResultServer.class);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return result;
    }

    public ResultServer verificarCodigo(String codigo){
        String host = urlServer+"/sms_verification_codes/"+codigo+"/verify";
        ResultServer result = new ResultServer();
        Rest webService = new Rest(host);
        User userlogged = App.getUser(mCtx);
        try {
            String response = webService.webGetAuth (host,userlogged);
            result = gson.fromJson(response,ResultServer.class);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return result;
    }

    public ResultServer gerarCodigoVerificacaoConta(String numeroTelefone, String conta_token){
        String host = urlServer+"/sms/confirm_account";
        ResultServer result = new ResultServer();
        Rest webService = new Rest(host);
        try {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("sms_verification_code[numero_telefone]", "+55"+numeroTelefone.replaceAll("[^0-9]", "")));
            params.add(new BasicNameValuePair("sms_verification_code[conta_token]", conta_token));
            String response = webService.doPost(host,params);
            result = gson.fromJson(response,ResultServer.class);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return result;
    }

    public ResultServer verificarCodigoEhAtivarConta(String codigo, String telefone){
        String host = urlServer+"/sms_verification_codes/verify_and_activate_account";
        ResultServer result = new ResultServer();
        Rest webService = new Rest(host);
        User userlogged = App.getUser(mCtx);
        try {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("sms_verification_code[numero_telefone]", "+55"+telefone.replaceAll("[^0-9]", "")));
            params.add(new BasicNameValuePair("sms_verification_code[codigo]", codigo));
            String response = webService.doPutNonAuth (host,params);
            result = gson.fromJson(response,ResultServer.class);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return result;
    }


    public ResultServer salvarRecargaTelefone(RecargaTelefone recarga){
        String host = urlServer+"/api_acesso/transfer_to/recarregar_telefone";
        ResultServer result = new ResultServer();
        Rest webService = new Rest(host);
        User userlogged = App.getUser(mCtx);
        DecimalFormat df = new DecimalFormat("0.00");
        try {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("number", "55"+recarga.getNumber().replaceAll("[^0-9]", "")));
            params.add(new BasicNameValuePair("operator_id", recarga.getOperator_id()));
            params.add(new BasicNameValuePair("skuid", recarga.getSkuid()));
            params.add(new BasicNameValuePair("price", df.format(recarga.getPrice()).replace(",", ".")));
            params.add(new BasicNameValuePair("nome_favorecido", recarga.getNome_favorecido()));
            String response = webService.doPostAuth(host,params,userlogged);
            result = gson.fromJson(response,ResultServer.class);

        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return result;
    }

    public ArrayList<ValorRecarga> getValoresRecarga(String numeroTelefone){
        ArrayList<ValorRecarga> valorRecargas = new ArrayList<ValorRecarga>();
        Gson gsonCustom =new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();
        String host = urlServer+"/api_acesso/transfer_to/consultar_numero?number=55"+numeroTelefone;
        Rest webService = new Rest(host);
        User userlogged = App.getUser(mCtx);
        String response = webService.webGet(host, userlogged);
        try {
            JSONObject json = new JSONObject(response);
            if(json.has("success")){
                JSONArray jsonValores = json.getJSONObject("valores").getJSONArray("valores_recargas");
                for(int i = 0; i < jsonValores.length(); i++){
                    JSONObject obj = jsonValores.getJSONObject(i);
                    ValorRecarga valorRecarga = gsonCustom.fromJson(obj.toString(),ValorRecarga.class);
                    valorRecarga.setOperator_id(json.getJSONObject("valores").getString("operator_id"));
                    valorRecargas.add(valorRecarga);
                }
            }

        }catch (Exception exception){

        }
        return valorRecargas;
    }

    public ResultServer salvarRecarga(Recarga recarga){
        String host = urlServer+"/mastercard/recarga_celular";
        ResultServer result = new ResultServer();
        Rest webService = new Rest(host);
        User userlogged = App.getUser(mCtx);

        try {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("recarga[card_number]", recarga.getCard_number()));
            params.add(new BasicNameValuePair("recarga[codigo_operadora]", recarga.getCodigo_operadora()));
            params.add(new BasicNameValuePair("recarga[valor_recarga]", recarga.getValor_recarga()));
            params.add(new BasicNameValuePair("recarga[ddd]", recarga.getDdd()));
            params.add(new BasicNameValuePair("recarga[telefone]", recarga.getTelefone()));
            params.add(new BasicNameValuePair("recarga[data_validade]", recarga.getData_validade()));
            params.add(new BasicNameValuePair("recarga[cvv]", recarga.getCvv()));
            params.add(new BasicNameValuePair("recarga[senha]", recarga.getSenha()));
            String response = webService.doPostAuth(host,params,userlogged);

            result = gson.fromJson(response,ResultServer.class);

        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return result;
    }

    public ResultServer salvarPagamentoBoleto(PagamentoBoleto pagamentoBoleto){
        String host = urlServer+"/mastercard/pagamento/boleto";
        ResultServer result = new ResultServer();
        Rest webService = new Rest(host);
        User userlogged = App.getUser(mCtx);

        try {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("pagamento[card_number]", pagamentoBoleto.getCard_number()));
            params.add(new BasicNameValuePair("pagamento[linha_digitavel]", pagamentoBoleto.getLinha_digitavel()));
            params.add(new BasicNameValuePair("pagamento[data_validade]", pagamentoBoleto.getData_validade()));
            params.add(new BasicNameValuePair("pagamento[data_vencimento]", pagamentoBoleto.getData_vencimento()));
            params.add(new BasicNameValuePair("pagamento[cvv]", pagamentoBoleto.getCvv()));
            params.add(new BasicNameValuePair("pagamento[senha]", pagamentoBoleto.getSenha()));
            String response = webService.doPostAuth(host,params,userlogged);

            result = gson.fromJson(response,ResultServer.class);

        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return result;
    }



    public ArrayList<Operadora> getOperadorasByDDD(String ddd){
        ArrayList<Operadora> operadoras = new ArrayList<Operadora>();
        Gson gsonCustom =new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();
        String host = urlServer+"/mastercard/recarga_celular/operadoras?ddd="+ddd;
        Rest webService = new Rest(host);
        User userlogged = App.getUser(mCtx);
        String response = webService.webGet(host, userlogged);
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.has("operadoras_disponiveis")){
                JSONArray jsonOperadoras = jsonObject.getJSONArray("operadoras_disponiveis");
                for(int i = 0 ; i < jsonOperadoras.length() ; i++){
                    JSONObject obj = jsonOperadoras.getJSONObject(i);
                    Operadora operadora = gsonCustom.fromJson(obj.toString(),Operadora.class);
                    operadoras.add(operadora);
                }
            }
        }catch (Exception exception){

        }
        return operadoras;
    }

    public List<String> getValoresByOperadoraEhDDD(String ddd, String operadora){
        List<String> valores_disponiveis = new ArrayList<>();

        Gson gsonCustom =new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();
        String host = urlServer+"/mastercard/recarga_celular/valores_operadoras?ddd="+ddd+"&codigo_operadora="+operadora;
        Rest webService = new Rest(host);
        User userlogged = App.getUser(mCtx);
        String response = webService.webGet(host, userlogged);
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.has("valores_disponiveis")){
                JSONArray jsonValores = jsonObject.getJSONArray("valores_disponiveis");
                for(int i = 0 ; i < jsonValores.length() ; i++){
                    JSONObject obj = jsonValores.getJSONObject(i);
                    valores_disponiveis.add(obj.getString("valor_fixo"));
                }
            }
        }catch (Exception exception){

        }
        return valores_disponiveis;
    }

    public ArrayList<ParcelaCartao> getParcelasCartao(String num_cartao, Double valor){
        ArrayList<ParcelaCartao> parcelaCartaos = new ArrayList<ParcelaCartao>();
        Gson gsonCustom =new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();
        DecimalFormat df = new DecimalFormat("0.00");
        String host = urlGateway+"/card/installments?maxInstallments=1&num_cartao="+num_cartao+"&valor="+df.format(valor).replace(",", ".");
        Rest webService = new Rest(host);
        User userlogged = App.getUser(mCtx);
        String response = webService.webGet(host, userlogged);
        Log.i("responseInstallments", response);
        try{
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.has("installments")){
                JSONObject jsonInstallments = jsonObject.getJSONObject("installments");
                Iterator<?> keys = jsonInstallments.keys();
                while( keys.hasNext() ) {
                    String key = (String)keys.next();
                    if ( jsonInstallments.get(key) instanceof JSONArray ) {
                        JSONArray jsonLinhas = (JSONArray) jsonInstallments.get(key);
                        for(int i = 0 ; i < jsonLinhas.length() ; i++){
                            JSONObject obj = jsonLinhas.getJSONObject(i);
                            ParcelaCartao parcelaCartao = gsonCustom.fromJson(obj.toString(),ParcelaCartao.class);
                            parcelaCartaos.add(parcelaCartao);
                        }
                        return parcelaCartaos;
                    }
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return parcelaCartaos;
    }

    public ApiCep getDadosByCep(String cep){
        ApiCep apiCep = new ApiCep();
        Gson gsonCustom =new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();
        String host = "http://viacep.com.br/ws/"+cep+"/json/unicode/";
        Rest webService = new Rest(host);
        User userlogged = App.getUser(mCtx);
        String response = webService.webGet(host, userlogged);
        Log.i("response", response);

        apiCep = gsonCustom.fromJson(response,ApiCep.class);
        return apiCep;
    }

    public User auth(String email, String senha){
        User usuario = new User();
        String host = urlServer+"/users/sign-in";
        Rest webService = new Rest(host);

        try {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("[user][email]",email));
            params.add(new BasicNameValuePair("[user][password]", senha));

            String response = webService.doPost(host,params);
            JSONObject objResp = new JSONObject(response);
            Log.d("DEBUG", response);
            if (objResp.has("user")){
                usuario = gson.fromJson(objResp.getJSONObject("user").toString(),User.class);
                usuario.setNum_conta(objResp.getJSONObject("user").getJSONObject("conta").getString("numero"));
                usuario.setStatus_conta(0);
            }else if(objResp.has("conta_token")){
                usuario.setStatus_conta(3);
                usuario.setConta_token(objResp.getString("conta_token"));
            }else if (objResp.has("message")){
                usuario.setMessage(objResp.getString("message"));
            }else{
                usuario.setMessage("Verifique sua conexão");
            }


        } catch (Exception exception) {
            usuario.setId(null);
            exception.printStackTrace();
        }

        return usuario;
    }


    public ResultServer recuperarSenha(String email){
        ResultServer result = new ResultServer();
        String host = urlServer+"/users/solicitar_recuperacao_senha";
        Rest webService = new Rest(host);

        try {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("[user][email]",email));
            String response = webService.doPost(host,params);
            JSONObject objResp = new JSONObject(response);
            Log.d("DEBUG", response);
            result = gson.fromJson(response,ResultServer.class);

        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return result;
    }

    public User getUserData(String user_id){
        User userlogged = App.getUser(mCtx);
        User usuario = new User();
        String host = urlServer+"/user/" + user_id;
        Rest webService = new Rest(host);

        try {
            String response = webService.webGet(host, userlogged);

            Log.i("response", response);


            usuario = gson.fromJson(response,User.class);

        } catch (Exception exception) {
            usuario.setId(null);
            exception.printStackTrace();
        }

        return usuario;
    }

    public ResultServer vefify_face(String face64, User user){
        ResultServer result = new ResultServer();
        String host = urlServer+"/face_recognize/recognize";
        Rest webService = new Rest(host);

        try {

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("face_base64", face64));
            params.add(new BasicNameValuePair("user_id", user.getId().toString()));

//            String response = webService.doPostAuth(host,params,userlogged);
            String response = webService.doPost(host,params,user);

            result = gson.fromJson(response,ResultServer.class);

        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return result;
    }

    public ResultServer saveCard(String numcard, String user_id) {
        User userlogged = App.getUser(mCtx);
        ResultServer result = new ResultServer();
        Gson gsonCustom =new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();

        try {
            String host=urlServer+"/mastercard/card/" + numcard + "/user/" + user_id + "/save";


            Rest webService = new Rest(host);
            String response = webService.webGet(host, userlogged);

            Log.i("response", response);


            result = gson.fromJson(response,ResultServer.class);

        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return result;

    }

    public ResultServer salvarCartao(String numcard, String validade, String cvv, String nomeImpresso) {
        User userlogged = App.getUser(mCtx);
        ResultServer result = new ResultServer();
        Gson gsonCustom =new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();

        try {
            String host=urlServer+"/api_acesso/cartao/salvar_cartao";
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("numero_cartao", numcard));
            params.add(new BasicNameValuePair("validade", validade));
            params.add(new BasicNameValuePair("cvv", cvv));
            params.add(new BasicNameValuePair("nome_impresso", nomeImpresso));
            Rest webService = new Rest(host);
            String response = webService.doPostAuth(host, params, userlogged);
            Log.i("response", response);
            result = gson.fromJson(response,ResultServer.class);

        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return result;

    }

    public ResultServer recarregarCartao(String proxy, double valor) {
        User userlogged = App.getUser(mCtx);
        ResultServer result = new ResultServer();
        Gson gsonCustom =new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();

        try {
            String host=urlServer+"/api_acesso/operacional/solicitar_carga_cartao";
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("proxy", proxy));
            params.add(new BasicNameValuePair("valor", Double.toString(valor)));
            Rest webService = new Rest(host);
            String response = webService.doPostAuth(host, params, userlogged);
            Log.i("response", response);
            result = gson.fromJson(response,ResultServer.class);

        } catch (Exception exception) {
            exception.printStackTrace();
            result.setMessage("Ocorreu um erro inesperado!");
        }

        return result;

    }

    public Saida salvarSaida(Saida saida) {
        User userlogged = App.getUser(mCtx);
        ResultServer result = new ResultServer();
        Gson gsonCustom =new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();

        try {
            String host=urlServer+"/saidas";
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            Rest webService = new Rest(host);
            if (saida.getTipo_favorecido() == 0){
                params.add(new BasicNameValuePair("saida[conta_bancaria]", saida.getConta_bancaria()));
                params.add(new BasicNameValuePair("saida[agencia]", saida.getAgencia()));
                params.add(new BasicNameValuePair("saida[tipo_pagamento]", saida.getTipo_pagamento()));
                params.add(new BasicNameValuePair("saida[valor]", Double.toString(saida.getValor())));
                params.add(new BasicNameValuePair("saida[nome_favorecido]", saida.getNome_favorecido().toString()));
                params.add(new BasicNameValuePair("saida[data_agendamento]", saida.getData_agendamento().toString()));

                params.add(new BasicNameValuePair("favorecido[banco_id]", Long.toString(saida.getBanco_id())));
                params.add(new BasicNameValuePair("favorecido[cpf_cnpj]", saida.getCpf_cnpj()));
                params.add(new BasicNameValuePair("favorecido[tipo_pessoa]", saida.getTipo_pessoa()));
                params.add(new BasicNameValuePair("favorecido[tipo_conta]", saida.getTipo_conta()));
                params.add(new BasicNameValuePair("favorecido[agencia_dv]", saida.getAgencia_dv().toString()));
                params.add(new BasicNameValuePair("favorecido[conta_dv]", saida.getConta_dv().toString()));
            }else if(saida.getTipo_favorecido() == 1){
                params.add(new BasicNameValuePair("saida[conta_favorecido]", saida.getConta_favorecido()));
                params.add(new BasicNameValuePair("saida[tipo_pagamento]", saida.getTipo_pagamento()));
                params.add(new BasicNameValuePair("saida[valor]", Double.toString(saida.getValor())));
                params.add(new BasicNameValuePair("saida[nome_favorecido]", saida.getNome_favorecido().toString()));
                params.add(new BasicNameValuePair("saida[data_agendamento]", saida.getData_agendamento().toString()));
            }else if(saida.getTipo_favorecido() == 2){
                params.add(new BasicNameValuePair("saida[numero_cartao]", saida.getNumero_cartao()));
                params.add(new BasicNameValuePair("saida[tipo_pagamento]", saida.getTipo_pagamento()));
                params.add(new BasicNameValuePair("saida[valor]", Double.toString(saida.getValor())));
                params.add(new BasicNameValuePair("saida[nome_favorecido]", saida.getNome_favorecido().toString()));
                params.add(new BasicNameValuePair("saida[data_agendamento]", saida.getData_agendamento().toString()));
            }else if(saida.getTipo_favorecido() == 3){
                params.add(new BasicNameValuePair("saida[cod_boleto]", saida.getCod_boleto()));
                params.add(new BasicNameValuePair("saida[tipo_pagamento]", saida.getTipo_pagamento()));
                params.add(new BasicNameValuePair("saida[valor]", Double.toString(saida.getValor())));
                params.add(new BasicNameValuePair("saida[nome_favorecido]", saida.getNome_favorecido().toString()));
                params.add(new BasicNameValuePair("saida[data_agendamento]", saida.getData_agendamento().toString()));
                params.add(new BasicNameValuePair("saida[data_vencimento_boleto]", saida.getData_vencimento_boleto().toString()));
            }

            String response = webService.doPostAuth(host,params,userlogged);

            JSONObject json = new JSONObject(response);
            if (json.has("message")){
                saida.setError_message(json.getString("message"));
            }
            if (json.has("success") && json.getBoolean("success") == true){
                saida.setId(666); //paulo noob alterou o retorno de ultima hora
            }


        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return saida;

    }


    public ArrayList<Card> getCards(String user_id) {
        User userlogged = App.getUser(mCtx);
        ArrayList<Card> cards = new ArrayList<Card>();
        Gson gsonCustom =new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();

        try {
            String host=urlServer+"/mastercard/card/user/"+ user_id;


            Rest webService = new Rest(host);
            String response = webService.webGet(host, userlogged);

            Log.i("response", response);

            JSONArray jsonCards = new JSONArray(response);
            Log.i("response", response);
            for(int i = 0 ; i < jsonCards.length() ; i++){
                JSONObject obj = jsonCards.getJSONObject(i);
                Card card = gsonCustom.fromJson(obj.toString(),Card.class);
                cards.add(card);
            }

        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return cards;

    }


    public ArrayList<Extrato> getExtratoWallet(int page){
        User userlogged = App.getUser(mCtx);
        ArrayList<Extrato> extratos = new ArrayList<Extrato>();
        Gson gsonCustom =new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();
        try {
            String host=urlServer+"/lista_extratos?page="+page;
            Rest webService = new Rest(host);
            String response = webService.webGet(host, userlogged);
            Log.i("response", response);
            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonExtratos = jsonObject.getJSONArray("extratos");
            for(int i = 0 ; i < jsonExtratos.length() ; i++){
                JSONObject obj = jsonExtratos.getJSONObject(i);
                Extrato extrato = gsonCustom.fromJson(obj.toString(),Extrato.class);
                extratos.add(extrato);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return extratos;
    }

    public String getTokenByContaId(int conta_id){
        User userlogged = App.getUser(mCtx);
        String token = null;
        Gson gsonCustom =new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();

        try {
            String host=urlServer+"/contas/"+conta_id;
            Rest webService = new Rest(host);
            String response = webService.webGet(host, userlogged);
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.has("conta")){
                token = jsonObject.getJSONObject("conta").getString("token");
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return token;
    }

    public ArrayList<Banco> getBancos(){
        User userlogged = App.getUser(mCtx);
        ArrayList<Banco> bancos = new ArrayList<Banco>();
        Gson gsonCustom =new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();

        try {
            String host=urlServer+"/bancos";
            Rest webService = new Rest(host);
            String response = webService.webGet(host, userlogged);
            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonBancos = jsonObject.getJSONArray("bancos");
            Log.i("response", response);
            for(int i = 0 ; i < jsonBancos.length() ; i++){
                JSONObject obj = jsonBancos.getJSONObject(i);
                Banco banco = gsonCustom.fromJson(obj.toString(),Banco.class);
                bancos.add(banco);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return bancos;
    }

    public ArrayList<Cartao> getCartoes(){
        User userlogged = App.getUser(mCtx);
        ArrayList<Cartao> cartoes = new ArrayList<Cartao>();
        Gson gsonCustom =new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();

        try {
            String host=urlServer+"/contas/"+userlogged.getCurrent_conta_id();

            Rest webService = new Rest(host);
            String response = webService.webGet(host, userlogged);
            JSONObject jsonObject = new JSONObject(response);
            JSONObject objConta = jsonObject.getJSONObject("conta");
            JSONArray jsonCartoes = objConta.getJSONArray("cartoes");
            Log.i("response_cartao", response);
            for(int i = 0 ; i < jsonCartoes.length() ; i++){
                JSONObject obj = jsonCartoes.getJSONObject(i);
                Cartao cartao = gsonCustom.fromJson(obj.toString(),Cartao.class);
                cartoes.add(cartao);
            }

        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return cartoes;
    }

    public ArrayList<SolicitacaoCartao> getSolicitacoesCartoes(){
        User userlogged = App.getUser(mCtx);
        ArrayList<SolicitacaoCartao> solicitacaoCartaos = new ArrayList<SolicitacaoCartao>();
        Gson gsonCustom =new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();

        try {
            String host=urlServer+"/solicitacoes_cartao/";

            Rest webService = new Rest(host);
            String response = webService.webGet(host, userlogged);
            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonSolicitacoes = jsonObject.getJSONArray("solicitacao_cartoes");
            Log.i("response_cartao", response);
            for(int i = 0 ; i < jsonSolicitacoes.length() ; i++){
                JSONObject obj = jsonSolicitacoes.getJSONObject(i);
                SolicitacaoCartao solicitacaoCartao = gsonCustom.fromJson(obj.toString(),SolicitacaoCartao.class);
                solicitacaoCartaos.add(solicitacaoCartao);
            }

        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return solicitacaoCartaos;
    }

    public ArrayList<ExtratoCartao> getExtratoCartao(String proxy){
        Date today = new Date();
        Calendar cal = new GregorianCalendar();
        cal.setTime(today);
        cal.add(Calendar.DAY_OF_MONTH, -60);
        Date today60 = cal.getTime();
        String data_inicio = Utils.formatDate(today60, "dd/MM/yyyy");
        String data_fim = Utils.formatDate(today, "dd/MM/yyyy");
        User userlogged = App.getUser(mCtx);
        ArrayList<ExtratoCartao> extratos = new ArrayList<ExtratoCartao>();
        Gson gsonCustom =new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();
        try {
            String host = urlServer + "/api_acesso/operacional/consultar_extrato_cartao?data_inicial="+data_inicio+"&data_final="+data_fim+"&proxy="+proxy;
            Rest webService = new Rest(host);
            String response = webService.webGet(host, userlogged);
            Log.i("response", response);
            JSONObject jsonObject = new JSONObject(response);
            if(jsonObject.has("extrato")){
                JSONArray jsonExtrato = jsonObject.getJSONArray("extrato");
                for(int i = 0 ; i < jsonExtrato.length() ; i++){
                    JSONObject obj = jsonExtrato.getJSONObject(i);
                    ExtratoCartao extratoCartao = gsonCustom.fromJson(obj.toString(),ExtratoCartao.class);
                    extratos.add(extratoCartao);
                }
            }
        }catch (Exception exception){
            exception.printStackTrace();
        }

        return extratos;
    }

    public Double getSaldoCartao(String proxy){
        User userlogged = App.getUser(mCtx);
        Gson gsonCustom =new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();
        Double saldo = 0.0;
        try {
            String host = urlServer + "/api_acesso/operacional/consultar_saldo_cartao?proxy="+proxy;
            Rest webService = new Rest(host);
            String response = webService.webGet(host, userlogged);
            Log.i("response", response);
            JSONObject jsonObject = new JSONObject(response);
            if(jsonObject.has("result")){
                saldo = jsonObject.getJSONObject("result").getDouble("Saldo");
            }
        }catch (Exception exception){
            exception.printStackTrace();
        }

        return saldo;
    }

    public Saldo getSaldo(){
        User userlogged = App.getUser(mCtx);
        Gson gsonCustom =new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();
        Saldo saldo = null;
        try {
            String host=urlServer+"/contas/"+userlogged.getCurrent_conta_id()+"/saldo";

            Rest webService = new Rest(host);
            String response = webService.webGet(host, userlogged);

            Log.i("response", response);

            JSONObject jsonObject = new JSONObject(response);
            JSONObject objSaldo = jsonObject.getJSONObject("saldo");
            saldo = gsonCustom.fromJson(objSaldo.toString(),Saldo.class);


        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return saldo;
    }

    public Cartao getCartao(String cartao_id){
        User userlogged = App.getUser(mCtx);
        Gson gsonCustom =new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();
        Cartao cartao = null;
        try {
            String host=urlServer+"/cartoes/"+cartao_id;

            Rest webService = new Rest(host);
            String response = webService.webGet(host, userlogged);

            Log.i("response", response);

            JSONObject jsonObject = new JSONObject(response);
            JSONObject objCartao = jsonObject.getJSONObject("cartao");
            cartao = gsonCustom.fromJson(objCartao.toString(),Cartao.class);


        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return cartao;
    }

    public EmissaoBoleto emitirBoletoDeposito(String conta_token, double valor){
        String host = urlServer+"/contas/recarga/boleto";
        EmissaoBoleto emissaoBoleto = new EmissaoBoleto();
        Rest webService = new Rest(host);
        User userlogged = App.getUser(mCtx);
        DecimalFormat df = new DecimalFormat("0.00");
        try {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("recarga[conta_token]", conta_token));
            params.add(new BasicNameValuePair("recarga[valor]", df.format(valor).replace(",", ".")));
            String response = webService.doPostAuth(host,params,userlogged);
            Log.d("Boleto", response);
            emissaoBoleto = gson.fromJson(response,EmissaoBoleto.class);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return emissaoBoleto;
    }

//    public ResultServer solicitarCartao(){
//        ResultServer result = new ResultServer();
//        List<NameValuePair> params = new ArrayList<NameValuePair>();
//        User userlogged = App.getUser(mCtx);
//        Gson gsonCustom =new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();
//        Cartao cartao = null;
//        try {
//            String host=urlServer+"/contas/"+userlogged.getCurrent_conta_id()+"/solicitar_cartao";
//
//            Rest webService = new Rest(host);
//
//            String response = webService.doPutAuth(host, params, userlogged);
//
//            result = gson.fromJson(response,ResultServer.class);
//
//
//        } catch (Exception exception) {
//            exception.printStackTrace();
//        }
//        return result;
//    }

    public EmissaoBoleto solicitarCartao(){
        User userlogged = App.getUser(mCtx);
        String host=urlServer+"/contas/"+userlogged.getCurrent_conta_id()+"/solicitar_cartao";
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        EmissaoBoleto emissaoBoleto = new EmissaoBoleto();
        Rest webService = new Rest(host);
        DecimalFormat df = new DecimalFormat("0.00");
        try {
            String response = webService.doPutAuth(host, params, userlogged);
            emissaoBoleto = gson.fromJson(response,EmissaoBoleto.class);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return emissaoBoleto;
    }

    public ArrayList<Entrada> getEntradas(int page, String filtro) {
        User userlogged = App.getUser(mCtx);
        ArrayList<Entrada> entradas = new ArrayList<Entrada>();
        Gson gsonCustom =new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();
        String host=urlServer+"/entradas?page="+page;
        if (filtro!=null)
            host=urlServer+"/entradas?"+filtro+"&page="+page;
        try {


            Rest webService = new Rest(host);
            String response = webService.webGet(host, userlogged);

            Log.i("response", response);

            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonEntradas = jsonObject.getJSONArray("entradas");
            Log.i("response", response);
            for(int i = 0 ; i < jsonEntradas.length() ; i++){
                JSONObject obj = jsonEntradas.getJSONObject(i);
                Entrada entrada = gsonCustom.fromJson(obj.toString(),Entrada.class);
                entrada.setPeriodo_valor_bruto_centavos(jsonObject.getJSONObject("meta").getJSONObject("total_periodo").getInt("valor_bruto_centavos"));
                JSONArray jsonParcelas = obj.getJSONArray("entrada_parcelas");
                ArrayList<EntradaParcela> parcelas = new ArrayList<EntradaParcela>();
                for(int j = 0; j < jsonParcelas.length(); j++){
                    JSONObject obj1 = jsonParcelas.getJSONObject(j);
                    EntradaParcela parcela = gsonCustom.fromJson(obj1.toString(),EntradaParcela.class);
                    parcelas.add(parcela);
                }
                entrada.setParcelas(parcelas);
                entradas.add(entrada);
            }

        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return entradas;
    }

    public Saida getFavorecidoByConta(String conta_id){
        User userlogged = App.getUser(mCtx);
        Gson gsonCustom =new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();
        Saida saida = null;
        try {
            String host=urlServer+"/contas/"+conta_id+"/conta_by_id";

            Rest webService = new Rest(host);
            String response = webService.webGet(host, userlogged);

            Log.i("response", response);

            JSONObject json = new JSONObject(response);
            if (json.has("message")){
                saida.setError_message(json.getString("message"));
            }
            if (json.has("saida")){
                saida = gsonCustom.fromJson(json.getJSONObject("saida").toString(),Saida.class);
            }

        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return saida;
    }

    public ExtratoSaldo getExtrato(String numcard) {
        User userlogged = App.getUser(mCtx);
        ExtratoSaldo es = new ExtratoSaldo();
        Gson gsonCustom =new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();

        try {
            String host=urlServer+"/mastercard/card/" + numcard + "/saldo";


            Rest webService = new Rest(host);
            String response = webService.webGet(host, userlogged);
            Log.i("response", response);

            es = gsonCustom.fromJson(response,ExtratoSaldo.class);


            JSONObject jsonExtratoSaldo = new JSONObject(response);
            ArrayList<ExtratoLinha> els = new ArrayList<ExtratoLinha>();
            JSONArray jsonLinhas = new JSONArray(jsonExtratoSaldo.getString("extrato"));
            Log.i("response", response);
            for(int i = 0 ; i < jsonLinhas.length() ; i++){
                JSONObject obj = jsonLinhas.getJSONObject(i);
                ExtratoLinha linha = gsonCustom.fromJson(obj.toString(),ExtratoLinha.class);
                els.add(linha);
            }
            es.setExtrato(els);

        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return es;

    }

    public ResultServer send_photo(String face64, User user){
        ResultServer result = new ResultServer();
        String host = urlServer+"/face_recognize/update_photo_s3";
        Rest webService = new Rest(host);

        try {

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("face_base64", face64));
            params.add(new BasicNameValuePair("user_id", user.getId().toString()));

//            String response = webService.doPostAuth(host,params,userlogged);
            String response = webService.doPost(host,params, user);

            result = gson.fromJson(response,ResultServer.class);

        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return result;
    }

    public ResultServer enviarFotosProtecao(VendaDireta vendaDireta){
        User userlogged = App.getUser(mCtx);
        ResultServer resultServer = new ResultServer();
        Gson gsonCustom =new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();
        String host=urlServer+"/entradas/"+vendaDireta.getTransaction_code()+"/salvar_documentos";
        try {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            Rest webService = new Rest(host);
            params.add(new BasicNameValuePair("entrada[document_data][0][tipo]", "assinatura_pagador"));
            params.add(new BasicNameValuePair("entrada[document_data][0][filename]", "assinatura_pagador.png"));
            params.add(new BasicNameValuePair("entrada[document_data][0][anexo_base64]", "data:image/png;base64," + vendaDireta.getAssinatura_pagador()));

            params.add(new BasicNameValuePair("entrada[document_data][1][tipo]", "foto_pagador"));
            params.add(new BasicNameValuePair("entrada[document_data][1][filename]", "foto_pagador.png"));
            params.add(new BasicNameValuePair("entrada[document_data][1][anexo_base64]", "data:image/png;base64," + vendaDireta.getFoto_pagador()));
            String response = webService.doPutAuth(host, params, userlogged);
            JSONObject json = new JSONObject(response);
            if (json.has("entrada")){
                resultServer.setSuccess(true);
            }else{
                resultServer.setSuccess(false);
                resultServer.setMessage("Ocorreu um erro");
            }
        }catch (Exception exception) {
            exception.printStackTrace();
        }
        return resultServer;
    }

    public ResultServer enviarFotosSolicitacaoAnaliseDocumento(SolicitacaoAnaliseDocumento solicitacao, ArrayList<String> documentosPendentes){
        User userlogged = App.getUser(mCtx);
        ResultServer resultServer = new ResultServer();
        Gson gsonCustom =new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();
        String host=urlServer+"/solicitacao_analise_documentos";
        try {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            Rest webService = new Rest(host);
            for(int i = 0; i < solicitacao.getDocumentos().size(); i++){
                Documento documento = solicitacao.getDocumentos().get(i);
                if (documento != null && documentosPendentes.contains(documento.getTipo())){
                    params.add(new BasicNameValuePair("solicitacao_analise_documento[document_data][][tipo]", documento.getTipo()));
                    params.add(new BasicNameValuePair("solicitacao_analise_documento[document_data][][filename]", documento.getTipo()+".png"));
                    params.add(new BasicNameValuePair("solicitacao_analise_documento[document_data][][anexo_base64]", "data:image/png;base64," + documento.getBase64()));
                }
            }
            String response = webService.doPostAuth(host, params, userlogged);
            JSONObject json = new JSONObject(response);
            if (json.has("solicitacao_analise_documento")){
                resultServer.setSuccess(true);
            }else{
                resultServer.setSuccess(false);
                resultServer.setMessage("Ocorreu um erro");
            }
        }catch (Exception exception) {
            exception.printStackTrace();
            return resultServer;
        }
        return resultServer;
    }

    public VendaDireta salvarVendaDireta(VendaDireta vendaDireta) {
        User userlogged = App.getUser(mCtx);
        ResultServer result = new ResultServer();
        Gson gsonCustom =new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();
        try {
            String host=urlGateway+"/new_checkout";
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            DecimalFormat df = new DecimalFormat("0.00");
            Rest webService = new Rest(host);
            params.add(new BasicNameValuePair("conta_token", vendaDireta.getConta_token()));
            params.add(new BasicNameValuePair("checkout_data[name]", vendaDireta.getName()));
            params.add(new BasicNameValuePair("checkout_data[cpf]", vendaDireta.getCpf()));
            params.add(new BasicNameValuePair("checkout_data[area_code]", vendaDireta.getArea_code()));
            params.add(new BasicNameValuePair("checkout_data[phone]", vendaDireta.getPhone()));
            params.add(new BasicNameValuePair("checkout_data[description]", vendaDireta.getDescription()));
            params.add(new BasicNameValuePair("checkout_data[email]", vendaDireta.getEmail()));

            params.add(new BasicNameValuePair("checkout_data[payment][installments]", Integer.toString(vendaDireta.getInstallments())));
            params.add(new BasicNameValuePair("checkout_data[payment][total_value]", df.format(vendaDireta.getTotal_value()).replace(",", ".")));
            params.add(new BasicNameValuePair("checkout_data[payment][method_payment]", "creditCard"));

            params.add(new BasicNameValuePair("checkout_data[payment][card][number]", vendaDireta.getCard_number().replaceAll(" ", "" )));
            params.add(new BasicNameValuePair("checkout_data[payment][card][brand]", vendaDireta.getCard_brand()));
            params.add(new BasicNameValuePair("checkout_data[payment][card][cvv]", vendaDireta.getCvv()));
            params.add(new BasicNameValuePair("checkout_data[payment][card][month]", vendaDireta.getMonth()));
            params.add(new BasicNameValuePair("checkout_data[payment][card][year]", vendaDireta.getYear()));

            String response = webService.doPostAuth(host,params,userlogged);

            JSONObject json = new JSONObject(response);
            if (json.has("success") && json.getBoolean("success") == false){
                vendaDireta.setError_message(json.getString("message"));
            }else{
                vendaDireta.setError_message(null);
                vendaDireta.setTransaction_code(json.getString("transaction_code"));
            }

        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return vendaDireta;

    }

    public ArrayList<RechargePoint> getRechargePoints(){
        ArrayList<RechargePoint> rechargePoints = new ArrayList<RechargePoint>();

        Gson gsonCustom =new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();
        String host = "https://www.acessocard.com.br/MinhaConta/CarregarCartao/consultar-lojas-carga";
        Rest webService = new Rest(host);
        String response = webService.webGetNonAuth(host);
        try {
            JSONArray jsonPoints = new JSONArray(response);
            for(int i = 0 ; i < jsonPoints.length() ; i++){
                JSONObject obj = jsonPoints.getJSONObject(i);

                RechargePoint rp = new RechargePoint();
                rp.setNome(obj.getString("Nome"));
                rp.setEndereco(obj.getString("Endereco") + ", " + obj.getString("Bairro") + ", " + obj.getString("Cidade") + "/" + obj.getString("UF") + " - " + obj.getString("CEP") );
                rp.setLatitude(Float.parseFloat(obj.getString("Latitude")));
                rp.setLongitude(Float.parseFloat(obj.getString("Longitude")));

                rechargePoints.add(rp);
            }
        }catch (Exception exception){

        }
        return rechargePoints;
    }

    public ArrayList<String> getDocumentosPendentes(){
        User userlogged = App.getUser(mCtx);
        ArrayList<String> documentosPendentes = new ArrayList<String>();
        try {
            String host=urlServer+"/contas/"+userlogged.getCurrent_conta_id()+"/documentos_pendentes";
            Rest webService = new Rest(host);
            String response = webService.webGet(host, userlogged);
            JSONArray json = new JSONArray(response);
            for(int i = 0 ; i < json.length() ; i++){
                documentosPendentes.add(json.getString(i));
            }

        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return documentosPendentes;
    }

    public ArrayList<SolicitacaoAnaliseDocumento> getSolicitacoesAnaliseDocumento(){
        User userlogged = App.getUser(mCtx);
        ArrayList<SolicitacaoAnaliseDocumento> solicitacoes = new ArrayList<SolicitacaoAnaliseDocumento>();
        Gson gsonCustom =new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();

        try {
            String host=urlServer+"/solicitacao_analise_documentos/";

            Rest webService = new Rest(host);
            String response = webService.webGet(host, userlogged);
            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonSolicitacoes = jsonObject.getJSONArray("solicitacao_analise_documentos");
            for(int i = 0 ; i < jsonSolicitacoes.length() ; i++){
                JSONObject obj = jsonSolicitacoes.getJSONObject(i);
                SolicitacaoAnaliseDocumento solicitacaoAnaliseDocumento = gsonCustom.fromJson(obj.toString(),SolicitacaoAnaliseDocumento.class);
                JSONArray jsonDocumentos = obj.getJSONArray("documentos");
                ArrayList<Documento> documentos = new ArrayList<Documento>();
                for(int j = 0; j < jsonDocumentos.length(); j++){
                    JSONObject obj1 = jsonDocumentos.getJSONObject(j);
                    Documento doc = gsonCustom.fromJson(obj1.toString(),Documento.class);
                    documentos.add(doc);
                }
                solicitacaoAnaliseDocumento.setDocumentos(documentos);
                solicitacoes.add(solicitacaoAnaliseDocumento);
            }

        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return solicitacoes;
    }

    public Boolean getStatusDocumentacaoByContaId(){
        User userlogged = App.getUser(mCtx);
        Boolean token = null;
        Gson gsonCustom =new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();
        try {
            String host=urlServer+"/contas/"+userlogged.getCurrent_conta_id();
            Rest webService = new Rest(host);
            String response = webService.webGet(host, userlogged);
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.has("conta")){
                token = jsonObject.getJSONObject("conta").getBoolean("documentacao_aprovada");
            }
        } catch (Exception exception) {
            exception.printStackTrace();
            return null;
        }
        return token;
    }



}


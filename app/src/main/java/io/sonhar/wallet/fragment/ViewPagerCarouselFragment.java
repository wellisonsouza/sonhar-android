package io.sonhar.wallet.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import io.sonhar.wallet.R;

/**
 * Created by PedroPontes1 on 17/06/2018.
 */

public class ViewPagerCarouselFragment extends Fragment {

    private TextView lblNumCard;
    private TextView lblHolder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.view_pager_carousel_fragment, container, false);
        lblNumCard = (TextView) v.findViewById(R.id.lblNumCard);
        lblHolder = (TextView) v.findViewById(R.id.lblHolder);

        String numCard = getArguments().getString("num_card", "");
        String holder = getArguments().getString("holder", "");


        Editable s = new SpannableStringBuilder(numCard);
        for (int i = 4; i < s.length(); i += 5) {
            if (s.toString().charAt(i) != ' ') {
                s.insert(i, " ");
            }

        }

        lblNumCard.setText(s.toString());
        lblHolder.setText(holder);

        return v;
    }
}
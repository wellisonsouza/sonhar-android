package io.sonhar.wallet.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import io.sonhar.wallet.R;
import io.sonhar.wallet.model.Documento;
import io.sonhar.wallet.model.EntradaParcela;

/**
 * Created by wellison on 11/07/2018.
 */

public class DocumentoAdapter extends ArrayAdapter<Documento> {
    private List<Documento> documentos;
    private Context mCtx;

    public DocumentoAdapter(Context ctx, List<Documento> documentos) {
        super(ctx, R.layout.row_detalhe_analise);
        this.mCtx = ctx;
        this.documentos = documentos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        view = LayoutInflater.from(mCtx).inflate(R.layout.row_detalhe_analise, parent, false);
        //view.setLayoutParams(new Carousel.LayoutParams(70, 70));
        Locale ptBr = new Locale("pt", "BR");
        NumberFormat formatter = NumberFormat.getCurrencyInstance(ptBr);

        TextView lblTipo = (TextView) view.findViewById(R.id.lblTipo);
        TextView lblStatus = (TextView) view.findViewById(R.id.lblStatus);
        TextView lblJustificativa = (TextView) view.findViewById(R.id.lblJustificativa);
        final Documento documento = documentos.get(position);

        lblTipo.setText(documento.getTipo().replaceAll("_"," ").toUpperCase());
        lblStatus.setText(documento.getStatus().replaceAll("_"," "));
        lblJustificativa.setText(documento.getJustificativa());

        return view;
    }

    @Override
    public Documento getItem(int position) {
        return documentos.get(position);
    }

    @Override
    public int getCount() {
        return documentos.size();
    }
}

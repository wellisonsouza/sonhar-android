package io.sonhar.wallet.app;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.util.ArrayList;

import io.sonhar.wallet.R;
import io.sonhar.wallet.model.Documento;
import io.sonhar.wallet.model.ResponseServer;
import io.sonhar.wallet.model.ResultServer;
import io.sonhar.wallet.model.SolicitacaoAnaliseDocumento;
import io.sonhar.wallet.util.Action;
import io.sonhar.wallet.util.ActionTask;
import io.sonhar.wallet.util.ErrorHelper;
import io.sonhar.wallet.util.ImageUtil;
import io.sonhar.wallet.util.SessionHelper;
import io.sonhar.wallet.util.Utils;
import io.sonhar.wallet.ws.RestServices;

public class EnviarDocumentacaoActivity extends AppCompatActivity {
    Button btnFotoCpf, btnFotoComprovanteResidencia, btnFotoSelfie;
    static final int REQUEST_IMAGE_CPF = 1;
    static final int REQUEST_IMAGE_COMPROVANTE_RESIDENCIA = 2;
    static final int REQUEST_IMAGE_SELFIE= 3;
    SolicitacaoAnaliseDocumento solicitacao;
    Button btnOk;

    ArrayList<String> documentosPendentes;

    Documento docCpf;
    Documento docComprovante;
    Documento docSelfie;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enviar_documentacao);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        btnFotoCpf = findViewById(R.id.btnFotoCpf);
        btnFotoComprovanteResidencia = findViewById(R.id.btnFotoComprovanteResidencia);
        btnFotoSelfie = findViewById(R.id.btnFotoSelfie);
        solicitacao = new SolicitacaoAnaliseDocumento();

        btnFotoCpf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkCameraPermission(REQUEST_IMAGE_CPF);
            }
        });

        btnFotoComprovanteResidencia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkCameraPermission(REQUEST_IMAGE_COMPROVANTE_RESIDENCIA);
            }
        });

        btnFotoSelfie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkCameraPermission(REQUEST_IMAGE_SELFIE);
            }
        });

        btnOk = findViewById(R.id.btnOk);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkImagens();
            }
        });

        ActionTask action = new ActionTask(EnviarDocumentacaoActivity.this, getDocumentosPendentes, getString(R.string.aguarde_txt));
        action.execute();


    }

    public void init(){
        if (documentosPendentes.contains("documento_com_foto")){
            btnFotoCpf.setVisibility(View.VISIBLE);
        }
        if (documentosPendentes.contains("comprovante_residencia")){
            btnFotoComprovanteResidencia.setVisibility(View.VISIBLE);
        }
        if (documentosPendentes.contains("selfie_com_documento")){
            btnFotoSelfie.setVisibility(View.VISIBLE);
        }
    }

    public void checkCameraPermission(final int tipo){
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.CAMERA)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        dispatchTakePictureIntent(tipo);
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        if (response.isPermanentlyDenied()) {
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void dispatchTakePictureIntent(int tipo) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, tipo);
        }
    }

    private void checkImagens(){
        if (documentosPendentes.contains("documento_com_foto") && docCpf == null){
            Toast.makeText( EnviarDocumentacaoActivity.this, "É necessário adicionar a foto do RG ou CNH", Toast.LENGTH_SHORT).show();
        }else if(documentosPendentes.contains("comprovante_residencia") && docComprovante == null){
            Toast.makeText( EnviarDocumentacaoActivity.this, "É necessário adicionar a foto do comprovante de residência", Toast.LENGTH_SHORT).show();
        }else if(documentosPendentes.contains("selfie_com_documento") && docSelfie == null){
            Toast.makeText( EnviarDocumentacaoActivity.this, "É necessário adicionar uma selfie em posse do documento", Toast.LENGTH_SHORT).show();
        }else{
            solicitacao.addDocumento(docCpf);
            solicitacao.addDocumento(docComprovante);
            solicitacao.addDocumento(docSelfie);
            ActionTask action = new ActionTask(EnviarDocumentacaoActivity.this, enviarDocumentos, getString(R.string.aguarde_txt));
            action.execute();
        }
    }

    Action enviarDocumentos = new Action() {
        ResultServer resultServer;
        @Override
        public void onPostExecute() {
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            if(responseServer.hasError()){
                new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                SessionHelper.getInstance().setResponseServer(null);
            }else if (resultServer.getSuccess()){
                Toast.makeText( EnviarDocumentacaoActivity.this, "Documentos enviados com sucesso!", Toast.LENGTH_LONG).show();
                finish();
            }else{
                Toast.makeText(getApplicationContext(), resultServer.getMessage(), Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void run() {
            try {
                RestServices rs = new RestServices(mCtx);
                resultServer = rs.enviarFotosSolicitacaoAnaliseDocumento(solicitacao, documentosPendentes);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CPF && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            docCpf = new Documento();
            docCpf.setTipo("documento_com_foto");
            docCpf.setBase64(ImageUtil.convert((Bitmap) extras.get("data")));
            btnFotoCpf.setCompoundDrawablesWithIntrinsicBounds(getDrawable(R.drawable.ic_take_a_photo_ok), null, null, null);
        }else if (requestCode == REQUEST_IMAGE_COMPROVANTE_RESIDENCIA && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            docComprovante = new Documento();
            docComprovante.setTipo("comprovante_residencia");
            docComprovante.setBase64(ImageUtil.convert((Bitmap) extras.get("data")));
            btnFotoComprovanteResidencia.setCompoundDrawablesWithIntrinsicBounds(getDrawable(R.drawable.ic_take_a_photo_ok), null, null, null);
        }else if (requestCode == REQUEST_IMAGE_SELFIE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            docSelfie = new Documento();
            docSelfie.setTipo("selfie_com_documento");
            docSelfie.setBase64(ImageUtil.convert((Bitmap) extras.get("data")));
            btnFotoSelfie.setCompoundDrawablesWithIntrinsicBounds(getDrawable(R.drawable.ic_take_a_photo_ok), null, null, null);
        }
    }

    Action getDocumentosPendentes = new Action() {

        @Override
        public void onPostExecute() {
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            if(responseServer.hasError()){
                new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                SessionHelper.getInstance().setResponseServer(null);
            }else{
                init();
            }
        }

        @Override
        public void run() {
            try {
                RestServices rs = new RestServices(mCtx);
                documentosPendentes = rs.getDocumentosPendentes();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return (super.onOptionsItemSelected(menuItem));
    }

}

package io.sonhar.wallet.fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import io.sonhar.wallet.R;
import io.sonhar.wallet.app.MainActivity;
import io.sonhar.wallet.model.Cartao;
import io.sonhar.wallet.model.RechargePoint;
import io.sonhar.wallet.util.Action;
import io.sonhar.wallet.util.ActionTask;
import io.sonhar.wallet.ws.RestServices;

/**
 * Created by PedroPontes1 on 18/06/2018. 5496 3000 0007 6392
 */

public class PontosRecargaFragment extends Fragment implements Fragmento{

    ListView lvCartoes;
    Cartao cartao;
    MainActivity main;
    ArrayList<Cartao> cartoes;
    FloatingActionButton btnNovoCartao;
    //    LinearLayout layoutEmpty;
    MapView mMapView;
    LatLng location = null;
    private GoogleMap googleMap;


    public PontosRecargaFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pontos_recarga, container, false);

        mMapView= (MapView) view.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume();

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }



        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;


                if (checkLocationPermission()) {
                    if (ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        //Request location updates:
                        googleMap.setMyLocationEnabled(true);
                    }
                }


                googleMap.getUiSettings().setCompassEnabled(true);
                googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                googleMap.getUiSettings().setRotateGesturesEnabled(true);

                if (ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    googleMap.setMyLocationEnabled(true);
                }
                // Check if we were successful in obtaining the map.


                googleMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {

                    @Override
                    public void onMyLocationChange(Location arg0) {
                        // TODO Auto-generated method stub

                        if(location == null) {
                            location = new LatLng(arg0.getLatitude(), arg0.getLongitude());

                            CameraPosition cameraPosition = new CameraPosition.Builder().target(location).zoom(16).build();
                            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        }
                    }
                });

            }
        });


        return view;
    }

    @Override
    public void reload(Context c) {
        ActionTask actionLoadCartoes = new ActionTask(getActivity(), getRechargePoints, getResources().getString(R.string.aguarde_txt));
        actionLoadCartoes.execute();
    }
    @Override
    public void setMainActivity(MainActivity main) {
        this.main = main;
    }



    Action getRechargePoints = new Action() {
        ArrayList<RechargePoint> rps;

        @Override
        public void onPostExecute() {
            // TODO Auto-generated method stub
            try {
                for(RechargePoint rp : rps){
                    LatLng point = new LatLng(rp.getLatitude(),rp.getLongitude());
                    googleMap.addMarker(new MarkerOptions().position(point).title(rp.getNome()).snippet(rp.getEndereco()));


                }


            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        @Override
        public void run() {
            RestServices rs = new RestServices(mCtx);
            rps = rs.getRechargePoints();
        }

    };

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
        location=null;
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
        location=null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
        location=null;
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public String getTittle() {
        return getActivity().getApplicationContext().getResources().getString(R.string.ponto_recarga);
    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(getActivity())
                        .setTitle("")
                        .setMessage("")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(getActivity(),new String[]
                                        {Manifest.permission.ACCESS_FINE_LOCATION},1);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        1);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {


                        googleMap.setMyLocationEnabled(true);
                    }

                } else {


                }
                return;
            }

        }
    }
}

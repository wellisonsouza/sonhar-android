package io.sonhar.wallet.model;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by wellison on 02/08/2018.
 */

public class Entrada {
    private int id, valor_centavos, qtd_parcelas, vl_gp_centavos, periodo_valor_bruto_centavos;
    private String tipo, status_entrada, operacao, tipo_transacao, cod_boleto, link_boleto;
    private Date data_transacao;

    public int getPeriodo_valor_bruto_centavos() {
        return periodo_valor_bruto_centavos;
    }

    public void setPeriodo_valor_bruto_centavos(int periodo_valor_bruto_centavos) {
        this.periodo_valor_bruto_centavos = periodo_valor_bruto_centavos;
    }

    private ArrayList<EntradaParcela> parcelas = new ArrayList<EntradaParcela>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getValor_centavos() {
        return valor_centavos;
    }

    public void setValor_centavos(int valor_centavos) {
        this.valor_centavos = valor_centavos;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getStatus_entrada() {
        return status_entrada;
    }

    public void setStatus_entrada(String status_entrada) {
        this.status_entrada = status_entrada;
    }

    public Date getData_transacao() {
        return data_transacao;
    }

    public void setData_transacao(Date data_transacao) {
        this.data_transacao = data_transacao;
    }

    public String getOperacao() {
        return operacao;
    }

    public void setOperacao(String operacao) {
        this.operacao = operacao;
    }

    public int getQtd_parcelas() {
        return qtd_parcelas;
    }

    public void setQtd_parcelas(int qtd_parcelas) {
        this.qtd_parcelas = qtd_parcelas;
    }

    public String getTipo_transacao() {
        return tipo_transacao;
    }

    public void setTipo_transacao(String tipo_transacao) {
        this.tipo_transacao = tipo_transacao;
    }

    public ArrayList<EntradaParcela> getParcelas() {
        return parcelas;
    }

    public int getVl_gp_centavos() {
        return vl_gp_centavos;
    }

    public void setVl_gp_centavos(int vl_gp_centavos) {
        this.vl_gp_centavos = vl_gp_centavos;
    }

    public void setParcelas(ArrayList<EntradaParcela> parcelas) {
        this.parcelas = parcelas;
    }

    public String getCod_boleto() {
        return cod_boleto;
    }

    public void setCod_boleto(String cod_boleto) {
        this.cod_boleto = cod_boleto;
    }

    public String getLink_boleto() {
        return link_boleto;
    }

    public void setLink_boleto(String link_boleto) {
        this.link_boleto = link_boleto;
    }

    public Double getValorEmReais(){
        return (double) valor_centavos/100;
    }

    public Double getTaxaEmReais(){
        return (double) vl_gp_centavos/100;
    }

    public Double getValorAntecipacao(){
        int antecipacao_centavos = 0;
        for(EntradaParcela parcela : parcelas){
            antecipacao_centavos += parcela.getValor_antecipacao_centavos();
        }
        return (double) antecipacao_centavos/100;
    }

    public Double getValorLiquido(){
        int antecipacao_centavos = 0;
        for(EntradaParcela parcela : parcelas){
            antecipacao_centavos += parcela.getValor_antecipacao_centavos();
        }
        int valor_liquido = valor_centavos - (vl_gp_centavos + antecipacao_centavos);
        return (double) valor_liquido/100;
    }

    public Double getTotalPeriodo(){
        return (double) periodo_valor_bruto_centavos/100;
    }
}

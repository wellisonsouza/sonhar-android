package io.sonhar.wallet.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import io.sonhar.wallet.R;
import io.sonhar.wallet.app.MainActivity;

/**
 * Created by wellison on 11/07/2018.
 */

public class TransactionFragment extends Fragment implements Fragmento{
    MainActivity main;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_transactions, container, false);

        return view;
    }


    @Override
    public void reload(Context c) {

    }
    @Override
    public void setMainActivity(MainActivity main) {
        this.main = main;
    }

    @Override
    public String getTittle() {
        return getActivity().getApplicationContext().getResources().getString(R.string.cartoes_txt);
    }


}

package io.sonhar.wallet.util;


import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffColorFilter;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.hardware.usb.UsbConstants;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.text.Html;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.charset.Charset;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.text.DateFormat;
import java.text.Normalizer;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.InputMismatchException;
import java.util.Locale;
import java.util.UUID;

import javax.crypto.Cipher;

import io.sonhar.wallet.R;
import io.sonhar.wallet.adapter.DocumentoAdapter;
import io.sonhar.wallet.adapter.ParcelaItemAdapter;
import io.sonhar.wallet.app.SolicitacaoCartaoActivity;
import io.sonhar.wallet.model.EmissaoBoleto;
import io.sonhar.wallet.model.Entrada;
import io.sonhar.wallet.model.ResultServer;
import io.sonhar.wallet.model.SolicitacaoAnaliseDocumento;


public class Utils {

    public static final Double DISTANCE_100M =9.136706189890503E-4;
    public static final String[] ESTADOS_STR={"Selecione o Estado","Acre","Alagoas","Amazonas","Amapá","Bahia","Ceará","Distrito Federal","Espírito Santo","Goiás","Maranhão","Minas Gerais","Mato Grosso do Sul","Mato Grosso","Pará","Paraíba","Pernambuco","Piauí","Paraná","Rio de Janeiro","Rio Grande do Norte","Rondônia","Roraima","Rio Grande do Sul","Santa Catarina","Sergipe","São Paulo","Tocantins"};
    private static final Charset ASCII = Charset.forName("US-ASCII");
    static final String cipher_type = "AES/CBC/PKCS5Padding";
    private static final long MILLIS_PER_DAY = 24 * 60 * 60 * 1000;


    public static String generateUUID() {
        String uuid = UUID.randomUUID().toString();
        return uuid;
    }
    public static int dpToPx(int dp, Context ctx) {
        DisplayMetrics displayMetrics = ctx.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }
    public static Bitmap getResizedBitmap(Bitmap bm, int newWidth, Boolean proporcional) {
        int width = bm.getWidth();
        int height = bm.getHeight();

        int percentChange = (newWidth*100)/width;
        int newHeight=0;
        if(proporcional)
            newHeight = (height*percentChange)/100;
        else
            newHeight = newWidth;

        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    public static String makeDottedLineWithValue(String title, String value, Integer sizeLine){
        String result="";

        Integer qtdDots = sizeLine - title.length() - value.length();

        String dots = "";
        for(int i = 0 ; i < qtdDots ; i++ ){
            dots+=".";
        }

        result = title + " " + dots + " " + value;

        return result;
    }


    public static String makeSpaceddLineWithValue(String title, String value, Integer sizeLine){
        String result="";

        Integer qtdDots = sizeLine - title.length() - value.length();

        String dots = "";
        for(int i = 0 ; i < qtdDots ; i++ ){
            dots+=" ";
        }

        result = title + " " + dots + " " + value;

        return result;
    }

    public static String MD5(String s){
        try{
            MessageDigest m= MessageDigest.getInstance("MD5");
            m.update(s.getBytes(),0,s.length());
            return new BigInteger(1,m.digest()).toString(16);
        }catch (Exception ex){
            ex.printStackTrace();
            return "";
        }

    }





    public static String centerString(String line, int maxCharacter){
        String newString="";
        Integer sizeLine = line.length();
        int qtdSpaces=(maxCharacter/2)-(sizeLine/2);

        for(int i=0;i<qtdSpaces;i++)
            newString+=" ";

        newString+=line;

        for(int i=0;i<qtdSpaces;i++)
            newString+=" ";

        return newString;
    }

    public static String translateDeviceClass(int deviceClass) {
        switch (deviceClass) {
            case UsbConstants.USB_CLASS_APP_SPEC:
                return "Application specific USB class";
            case UsbConstants.USB_CLASS_AUDIO:
                return "USB class for audio devices";
            case UsbConstants.USB_CLASS_CDC_DATA:
                return "USB class for CDC devices (communications device class)";
            case UsbConstants.USB_CLASS_COMM:
                return "USB class for communication devices";
            case UsbConstants.USB_CLASS_CONTENT_SEC:
                return "USB class for content security devices";
            case UsbConstants.USB_CLASS_CSCID:
                return "USB class for content smart card devices";
            case UsbConstants.USB_CLASS_HID:
                return "USB class for human interface devices (for example, mice and keyboards)";
            case UsbConstants.USB_CLASS_HUB:
                return "USB class for USB hubs";
            case UsbConstants.USB_CLASS_MASS_STORAGE:
                return "USB class for mass storage devices";
            case UsbConstants.USB_CLASS_MISC:
                return "USB class for wireless miscellaneous devices";
            case UsbConstants.USB_CLASS_PER_INTERFACE:
                return "USB class indicating that the class is determined on a per-interface basis";
            case UsbConstants.USB_CLASS_PHYSICA:
                return "USB class for physical devices";
            case UsbConstants.USB_CLASS_PRINTER:
                return "USB class for printers";
            case UsbConstants.USB_CLASS_STILL_IMAGE:
                return "USB class for still image devices (digital cameras)";
            case UsbConstants.USB_CLASS_VENDOR_SPEC:
                return "Vendor specific USB class";
            case UsbConstants.USB_CLASS_VIDEO:
                return "USB class for video devices";
            case UsbConstants.USB_CLASS_WIRELESS_CONTROLLER:
                return "USB class for wireless controller devices";
            default:
                return "Unknown USB class!";

        }
    }


    public static String cryptRSA (String keystr, String text) throws Exception {

        String pubKeyPEM = keystr.replace("-----BEGIN PUBLIC KEY-----\n", "");
        pubKeyPEM = pubKeyPEM.replace("-----END PUBLIC KEY-----", "");

        // Base64 decode the data

        byte [] encoded = Base64.decode(pubKeyPEM, Base64.DEFAULT);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(encoded);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        PublicKey pubkey = kf.generatePublic(keySpec);

        Cipher cipher = Cipher.getInstance("RSA/None/PKCS1Padding", "BC");
        cipher.init(Cipher.ENCRYPT_MODE, pubkey);
        byte[] cipherText = cipher.doFinal(text.getBytes());
        System.out.println("cipher: " + new String(cipherText));
        return new String(cipherText);
    }

    public static Double convertMetersIntegerToDistance(int meters){
        Double distance = (DISTANCE_100M*meters)/100;

        return distance;
    }
    public static Boolean yesOrNotToBoolean(String str){
        if(str.toUpperCase().equals("S"))
            return true;
        else
            return false;
    }
    public static String trueOrFalseToStr(Boolean val){
        if(val)
            return "S";
        else
            return "N";
    }
    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = 1000;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public static byte[] convertInputStreamToByteArray(InputStream inputStream)
    {
        byte[] bytes= null;

        try
        {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            byte data[] = new byte[1024];
            int count;

            while ((count = inputStream.read(data)) != -1)
            {
                bos.write(data, 0, count);
            }

            bos.flush();
            bos.close();
            inputStream.close();

            bytes = bos.toByteArray();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return bytes;
    }
    public static boolean isNetworkAvailable(Context context) {
        try {
            int timeoutMs = 1500;
            Socket sock = new Socket();
            SocketAddress sockaddr = new InetSocketAddress("8.8.8.8", 53);

            sock.connect(sockaddr, timeoutMs);
            sock.close();

            return true;
        } catch (IOException e) { return false; }
    }


    public static boolean isCPF(String CPF) {
// considera-se erro CPF's formados por uma sequencia de numeros iguais
        if (CPF.equals("00000000000") || CPF.equals("11111111111") ||
                CPF.equals("22222222222") || CPF.equals("33333333333") ||
                CPF.equals("44444444444") || CPF.equals("55555555555") ||
                CPF.equals("66666666666") || CPF.equals("77777777777") ||
                CPF.equals("88888888888") || CPF.equals("99999999999") ||
                (CPF.length() != 11))
            return(false);

        char dig10, dig11;
        int sm, i, r, num, peso;

// "try" - protege o codigo para eventuais erros de conversao de tipo (int)
        try {
// Calculo do 1o. Digito Verificador
            sm = 0;
            peso = 10;
            for (i=0; i<9; i++) {
// converte o i-esimo caractere do CPF em um numero:
// por exemplo, transforma o caractere '0' no inteiro 0
// (48 eh a posicao de '0' na tabela ASCII)
                num = (int)(CPF.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso - 1;
            }

            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11))
                dig10 = '0';
            else dig10 = (char)(r + 48); // converte no respectivo caractere numerico

// Calculo do 2o. Digito Verificador
            sm = 0;
            peso = 11;
            for(i=0; i<10; i++) {
                num = (int)(CPF.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso - 1;
            }

            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11))
                dig11 = '0';
            else dig11 = (char)(r + 48);

// Verifica se os digitos calculados conferem com os digitos informados.
            if ((dig10 == CPF.charAt(9)) && (dig11 == CPF.charAt(10)))
                return(true);
            else return(false);
        } catch (InputMismatchException erro) {
            return(false);
        }
    }

    public static String imprimeCPF(String CPF) {
        return(CPF.substring(0, 3) + "." + CPF.substring(3, 6) + "." +
                CPF.substring(6, 9) + "-" + CPF.substring(9, 11));
    }

    public static Integer getIdade(Date dataNascInput){


        Calendar dateOfBirth = new GregorianCalendar();

        dateOfBirth.setTime(dataNascInput);


        Calendar today = Calendar.getInstance();


        int age = today.get(Calendar.YEAR) - dateOfBirth.get(Calendar.YEAR);



        dateOfBirth.add(Calendar.YEAR, age);

        if (today.before(dateOfBirth)) {

            age--;

        }

        return age;

    }

    public static void showSucessoDepositoBoleto(final Context mCtx, final EmissaoBoleto emissaoBoleto, Dialog dialog, View.OnClickListener onClick){
        Locale ptBr = new Locale("pt", "BR");
        NumberFormat formatter = NumberFormat.getCurrencyInstance(ptBr);



        dialog.setContentView(R.layout.dialog_success_deposito);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes((WindowManager.LayoutParams) params);

        Button btnCopiarCodigo = dialog.findViewById(R.id.btnCopiarCodigo);
        Button btnVisualizarBoleto = dialog.findViewById(R.id.btnVisualizarBoleto);
        TextView lblFavorecido = dialog.findViewById(R.id.lblFavorecido);
        TextView lblBoletoStatus = dialog.findViewById(R.id.lblBoletoStatus);
        TextView lblDataTransacao = dialog.findViewById(R.id.lblDataTransacao);
        TextView lblCodigoBarras = dialog.findViewById(R.id.lblCodigoBarras);
        TextView lblValor = dialog.findViewById(R.id.lblValor);
        Button btnOk = (Button)dialog.findViewById(R.id.btnOk);
        if(emissaoBoleto.getStatus() != null)
            lblBoletoStatus.setText(emissaoBoleto.getStatus().replace("_", " ").toUpperCase());
        lblFavorecido.setText(emissaoBoleto.getFavorecido());
        lblCodigoBarras.setText(emissaoBoleto.getCod_boleto());
        if(emissaoBoleto.getData_transacao() == null)
            lblDataTransacao.setText(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(new Date()));
        else
            lblDataTransacao.setText(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(emissaoBoleto.getData_transacao()));
        lblValor.setText(formatter.format(emissaoBoleto.getValor()));

        btnCopiarCodigo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipboard = (ClipboardManager) mCtx.getSystemService(mCtx.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", emissaoBoleto.getCod_boleto());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(mCtx, "Código de barras copiado.",Toast.LENGTH_SHORT).show();
            }
        });

        btnVisualizarBoleto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i2=new Intent(Intent.ACTION_VIEW, Uri.parse(emissaoBoleto.getUrl_boleto()));
                mCtx.startActivity(i2);
            }
        });

        btnOk.setOnClickListener(onClick);
        dialog.show();

    }

    public static void showDetalheSolicitacaoAnaliseDocumento(final Context mCtx, SolicitacaoAnaliseDocumento solicitacao){
        Locale ptBr = new Locale("pt", "BR");
        NumberFormat formatter = NumberFormat.getCurrencyInstance(ptBr);

        final Dialog dialog = new Dialog(mCtx);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_detalhe_solicitacao_analise_documento);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes((WindowManager.LayoutParams) params);


        ListView lvDocumentos = dialog.findViewById(R.id.lvDocumentos);

        DocumentoAdapter adapter = new DocumentoAdapter(mCtx, solicitacao.getDocumentos());
        lvDocumentos.setAdapter(adapter);
        dialog.show();

    }

    public static void showDetalheEntrada(final Context mCtx, Entrada entrada){
        Locale ptBr = new Locale("pt", "BR");
        NumberFormat formatter = NumberFormat.getCurrencyInstance(ptBr);

        final Dialog dialog = new Dialog(mCtx);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_detalhe_entrada);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes((WindowManager.LayoutParams) params);


        ListView lvParcelas = dialog.findViewById(R.id.lvParcelas);
        TextView lblStatusEntrada = dialog.findViewById(R.id.lblStatusEntrada);
        TextView lblDataTransacao = dialog.findViewById(R.id.lblDataTransacao);
        TextView lblValorBruto = dialog.findViewById(R.id.lblValorBruto);
        TextView lblValorTaxa = dialog.findViewById(R.id.lblValorTaxa);
        TextView lblValorAntecipacao = dialog.findViewById(R.id.lblValorAntecipacao);
        TextView lblValorLiquido = dialog.findViewById(R.id.lblValorLiquido);

        lblStatusEntrada.setText(entrada.getStatus_entrada().replace("_", " ").toUpperCase());
        lblDataTransacao.setText(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(entrada.getData_transacao()));
        lblValorBruto.setText(formatter.format(entrada.getValorEmReais()));
        lblValorTaxa.setText(formatter.format(entrada.getTaxaEmReais()));
        lblValorAntecipacao.setText(formatter.format(entrada.getValorAntecipacao()));
        lblValorLiquido.setText(formatter.format(entrada.getValorLiquido()));
        ParcelaItemAdapter adapter = new ParcelaItemAdapter(mCtx, entrada.getParcelas());
        lvParcelas.setAdapter(adapter);
        dialog.show();

    }

    public static void showRecuperarSenha(Dialog dialog, View.OnClickListener onClick){

        dialog.setContentView(R.layout.dialog_recuperar_senha);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes((WindowManager.LayoutParams) params);
        Button btnOk = (Button)dialog.findViewById(R.id.btnOk);
        btnOk.setOnClickListener(onClick);
        dialog.show();

    }


    public static void showSucessoSolicitacaoCartao(final Context mCtx, final EmissaoBoleto emissaoBoleto, Dialog dialog, View.OnClickListener onClick){

        dialog.setContentView(R.layout.dialog_success_solicitacao_cartao);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes((WindowManager.LayoutParams) params);

        Button btnCopiarCodigo = dialog.findViewById(R.id.btnCopiarCodigo);
        Button btnVisualizarBoleto = dialog.findViewById(R.id.btnVisualizarBoleto);
        TextView lblFavorecido = dialog.findViewById(R.id.lblFavorecido);
        TextView lblBoletoStatus = dialog.findViewById(R.id.lblBoletoStatus);
        TextView lblDataTransacao = dialog.findViewById(R.id.lblDataTransacao);
        TextView lblCodigoBarras = dialog.findViewById(R.id.lblCodigoBarras);
        Button btnOk = (Button)dialog.findViewById(R.id.btnOk);
        if(emissaoBoleto.getStatus() != null)
            lblBoletoStatus.setText(emissaoBoleto.getStatus().replace("_", " ").toUpperCase());
        lblFavorecido.setText(emissaoBoleto.getFavorecido());
        lblCodigoBarras.setText(emissaoBoleto.getCod_boleto());
        if(emissaoBoleto.getData_transacao() == null)
            lblDataTransacao.setText(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(new Date()));
        else
            lblDataTransacao.setText(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(emissaoBoleto.getData_transacao()));

        btnCopiarCodigo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipboard = (ClipboardManager) mCtx.getSystemService(mCtx.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", emissaoBoleto.getCod_boleto());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(mCtx, "Código de barras copiado.",Toast.LENGTH_SHORT).show();
            }
        });

        btnVisualizarBoleto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i2=new Intent(Intent.ACTION_VIEW, Uri.parse(emissaoBoleto.getUrl_boleto()));
                mCtx.startActivity(i2);
            }
        });

        btnOk.setOnClickListener(onClick);
        dialog.show();

    }

    public static void showAlert(String titulo, String mensagem, Context mCtx){
        final Dialog dialog = new Dialog(mCtx);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_info);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.FILL_PARENT;
        dialog.getWindow().setAttributes((WindowManager.LayoutParams) params);

        TextView lblMsg = (TextView) dialog.findViewById(R.id.lblMsg);
        TextView lblTitulo = (TextView) dialog.findViewById(R.id.lblTitulo);
        Button btnOk = (Button)dialog.findViewById(R.id.btnOk);

        lblMsg.setText(Html.fromHtml(mensagem));
        lblTitulo.setText(titulo);
        btnOk.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public static void showAlertError(String titulo, String mensagem, Context mCtx){

        final Dialog dialog = new Dialog(mCtx);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_error);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.FILL_PARENT;
        dialog.getWindow().setAttributes((WindowManager.LayoutParams) params);

        TextView lblMsg = (TextView) dialog.findViewById(R.id.lblMsg);
        Button btnOk = (Button)dialog.findViewById(R.id.btnOk);

        lblMsg.setText(Html.fromHtml(mensagem));

        btnOk.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });



        dialog.show();

    }

    public static void showSendMensagemDoctor(final Context mCtx){

        final Dialog dialog = new Dialog(mCtx);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_doctor_rey);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.FILL_PARENT;
        dialog.getWindow().setAttributes((WindowManager.LayoutParams) params);

        TextView lblMsg = (TextView) dialog.findViewById(R.id.lblMsg);
        Button btnOk = (Button)dialog.findViewById(R.id.btnOk);


        btnOk.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Toast.makeText(mCtx, "Message sent successfully!", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });



        dialog.show();

    }

    public static void showAlertError(String mensagem, Dialog dialog, View.OnClickListener onClick){


        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_error);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.FILL_PARENT;
        dialog.getWindow().setAttributes((WindowManager.LayoutParams) params);

        TextView lblMsg = (TextView) dialog.findViewById(R.id.lblMsg);
        Button btnOk = (Button)dialog.findViewById(R.id.btnOk);

        lblMsg.setText(Html.fromHtml(mensagem));

        btnOk.setOnClickListener(onClick);



        dialog.show();

    }

    public static void showConfirmaçãoSms(Dialog dialog, String telefone, View.OnClickListener onClick){

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_sms_verification);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        TextView txtTelefone = dialog.findViewById(R.id.txtTelefone);
        params.width = WindowManager.LayoutParams.FILL_PARENT;
        dialog.getWindow().setAttributes((WindowManager.LayoutParams) params);
        Button btnOk = (Button)dialog.findViewById(R.id.btnOk);
        txtTelefone.setText("Insira o número de confirmação que foi enviado para o telefone "+telefone);

        btnOk.setOnClickListener(onClick);

        dialog.show();

    }

    public static void showAlertSuccess(String mensagem, Dialog dialog, View.OnClickListener onClick){


        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_success);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.FILL_PARENT;
        dialog.getWindow().setAttributes((WindowManager.LayoutParams) params);

        TextView lblMsg = (TextView) dialog.findViewById(R.id.lblMsg);
        Button btnOk = (Button)dialog.findViewById(R.id.btnOk);

        lblMsg.setText(Html.fromHtml(mensagem));

        btnOk.setOnClickListener(onClick);



        dialog.show();

    }

    public static void showAlertFaceSuccess(String mensagem, Dialog dialog, View.OnClickListener onClick){


        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_success_face);
        dialog.setCancelable(false);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.FILL_PARENT;
        dialog.getWindow().setAttributes((WindowManager.LayoutParams) params);

        TextView lblMsg = (TextView) dialog.findViewById(R.id.lblNome);
        Button btnOk = (Button)dialog.findViewById(R.id.btnOk);

        lblMsg.setText(mensagem);

        btnOk.setOnClickListener(onClick);



        dialog.show();

    }

    public static void showAlertTransferSuccess(String nome, final Context mCtx, final Activity ac){
        final Dialog dialog = new Dialog(mCtx);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_success_transfer);
        dialog.setCancelable(false);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.FILL_PARENT;
        dialog.getWindow().setAttributes((WindowManager.LayoutParams) params);

        TextView lblMsg = (TextView) dialog.findViewById(R.id.lblNome);
        Button btnOk = (Button)dialog.findViewById(R.id.btnOk);

        lblMsg.setText(nome);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                ac.finish();

            }
        });



        dialog.show();

    }

    public static void showBeneficio(final BeneficioItem beneficioItem, final Context mCtx, final Activity ac){
        final Dialog dialog = new Dialog(mCtx);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_beneficios);
        dialog.setCancelable(true);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.FILL_PARENT;
        dialog.getWindow().setAttributes((WindowManager.LayoutParams) params);

        TextView txtBeneficio = (TextView) dialog.findViewById(R.id.txtBeneficio);
        TextView txtBeneficioDesc = (TextView) dialog.findViewById(R.id.txtBeneficioDesc);
        ImageView imgBeneficio = dialog.findViewById(R.id.imgBeneficio);

        Button btnOk = (Button)dialog.findViewById(R.id.btnOk);

        txtBeneficio.setText(beneficioItem.getNome());
        txtBeneficioDesc.setText(beneficioItem.getDescricao());
        imgBeneficio.setImageResource(beneficioItem.getImage());
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent i2=new Intent(Intent.ACTION_VIEW, Uri.parse(beneficioItem.getLink()));
                mCtx.startActivity(i2);
                //ac.finish();
            }
        });
        dialog.show();
    }

    public static void showSolicitarCartao(final Context mCtx){
        final Dialog dialog = new Dialog(mCtx);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_solicitar_cartao);
        dialog.setCancelable(true);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.FILL_PARENT;
        dialog.getWindow().setAttributes((WindowManager.LayoutParams) params);


        Button btnOk = (Button)dialog.findViewById(R.id.btnOk);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent i2=new Intent(mCtx.getApplicationContext(), SolicitacaoCartaoActivity.class);
                mCtx.startActivity(i2);
                //ac.finish();
            }
        });
        dialog.show();
    }

    public static void showAlertFaceError(String mensagem, Context mCtx){
        final Dialog dialog = new Dialog(mCtx);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_error_face);
        dialog.setCancelable(false);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.FILL_PARENT;
        dialog.getWindow().setAttributes((WindowManager.LayoutParams) params);

        TextView lblMsg = (TextView) dialog.findViewById(R.id.lblNome);
        Button btnOk = (Button)dialog.findViewById(R.id.btnOk);

        lblMsg.setText(mensagem);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });



        dialog.show();

    }



    public static void showAlert(ResultServer rs, Context mCtx, View.OnClickListener onClick){

        final Dialog dialog = new Dialog(mCtx);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(rs.getSuccess())
            dialog.setContentView(R.layout.dialog_success);
        else
            dialog.setContentView(R.layout.dialog_error);

        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes((WindowManager.LayoutParams) params);

        TextView lblMsg = (TextView) dialog.findViewById(R.id.lblMsg);
        Button btnOk = (Button)dialog.findViewById(R.id.btnOk);

        lblMsg.setText(Html.fromHtml(rs.getMessage()));

        btnOk.setOnClickListener(onClick);



        dialog.show();

    }
    public static String bitmapToBase64(Bitmap image)
    {
        Bitmap immagex=image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

        return imageEncoded;
    }
    public static Bitmap base64ToBitmap(String input)
    {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }



    public static Bitmap changeBitmapColor(Bitmap bitmap, int color) {
        Paint paint = new Paint();
        paint.setColorFilter(new PorterDuffColorFilter(color, Mode.SRC_IN));
        Bitmap bitmapResult = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmapResult);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, 0, 0, paint);
        return bitmapResult;
    }

    public static Bitmap qrcodeAsBitmap(String str, int size) throws WriterException {
        BitMatrix result;
        int WHITE = 0xFFFFFFFF;
        int BLACK = 0xFF000000;
        try {
            result = new MultiFormatWriter().encode(str,
                    BarcodeFormat.QR_CODE, size, size, null);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int w = result.getWidth();
        int h = result.getHeight();
        int[] pixels = new int[w * h];
        for (int y = 0; y < h; y++) {
            int offset = y * w;
            for (int x = 0; x < w; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(w, h, Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, size, 0, 0, w, h);
        return bitmap;
    }
    public static String removeAcentos(String str) {

        str = Normalizer.normalize(str, Normalizer.Form.NFD);
        str = str.replaceAll("[^\\p{ASCII}]", "");
        return str;

    }
    public static Bitmap addPaddingLeftForBitmap(Bitmap bitmap, int paddingLeft) {
        Bitmap outputBitmap = Bitmap.createBitmap(bitmap.getWidth() + paddingLeft, bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(outputBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, paddingLeft, 0, null);
        return outputBitmap;
    }
    public static Bitmap trimBitmap(Bitmap bmp, int color) {
        // TODO Auto-generated method stub


        long dtMili = System.currentTimeMillis();
        int MTop = 0, MBot = 0, MLeft = 0, MRight = 0;
        boolean found1 = false, found2 = false;

        int[] bmpIn = new int[bmp.getWidth() * bmp.getHeight()];
        int[][] bmpInt = new int[bmp.getWidth()][bmp.getHeight()];

        bmp.getPixels(bmpIn, 0, bmp.getWidth(), 0, 0, bmp.getWidth(),
                bmp.getHeight());

        for (int ii = 0, contX = 0, contY = 0; ii < bmpIn.length; ii++) {
            bmpInt[contX][contY] = bmpIn[ii];
            contX++;
            if (contX >= bmp.getWidth()) {
                contX = 0;
                contY++;
                if (contY >= bmp.getHeight()) {
                    break;
                }
            }
        }

        for (int hP = 0; hP < bmpInt[0].length && !found2; hP++) {
            // looking for MTop
            for (int wP = 0; wP < bmpInt.length && !found2; wP++) {
                if (bmpInt[wP][hP] != color) {
//                    Log.e("MTop 2", "Pixel found @" + hP);
                    MTop = hP;
                    found2 = true;
                    break;
                }
            }
        }
        found2 = false;

        for (int hP = bmpInt[0].length - 1; hP >= 0 && !found2; hP--) {
            // looking for MBot
            for (int wP = 0; wP < bmpInt.length && !found2; wP++) {
                if (bmpInt[wP][hP] != color) {
//                    Log.e("MBot 2", "Pixel found @" + hP);
                    MBot = bmp.getHeight() - hP;
                    found2 = true;
                    break;
                }
            }
        }
        found2 = false;

        for (int wP = 0; wP < bmpInt.length && !found2; wP++) {
            // looking for MLeft
            for (int hP = 0; hP < bmpInt[0].length && !found2; hP++) {
                if (bmpInt[wP][hP] != color) {
//                    Log.e("MLeft 2", "Pixel found @" + wP);
                    MLeft = wP;
                    found2 = true;
                    break;
                }
            }
        }
        found2 = false;

        for (int wP = bmpInt.length - 1; wP >= 0 && !found2; wP--) {
            // looking for MRight
            for (int hP = 0; hP < bmpInt[0].length && !found2; hP++) {
                if (bmpInt[wP][hP] != color) {
//                    Log.e("MRight 2", "Pixel found @" + wP);
                    MRight = bmp.getWidth() - wP;
                    found2 = true;
                    break;
                }
            }

        }
        found2 = false;

        int sizeY = bmp.getHeight() - MBot - MTop, sizeX = bmp.getWidth()
                - MRight - MLeft;

        Bitmap bmp2 = Bitmap.createBitmap(bmp, MLeft, MTop, sizeX, sizeY);
        dtMili = (System.currentTimeMillis() - dtMili);

        return bmp2;
    }
    public static boolean isDateValid(String date)
    {
        String DATE_FORMAT = "dd/MM/yyyy";
        try {
            DateFormat df = new SimpleDateFormat(DATE_FORMAT);
            df.setLenient(false);
            df.parse(date);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    public static String formatDate(Date date, String format){
        return new SimpleDateFormat(format).format(date);
    }

    public static Date removeDays(Date d, int numDays)
    {
        return new Date(d.getTime() - numDays * MILLIS_PER_DAY);
    }

    public static Date formataData(String data) throws Exception {
        if (data == null || data.equals(""))
            return null;
        Date date = null;
        try {
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            date = (java.util.Date)formatter.parse(data);
        } catch (ParseException e) {
            throw e;
        }
        return date;
    }




}

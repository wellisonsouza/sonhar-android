package io.sonhar.wallet.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import io.sonhar.wallet.R;
import io.sonhar.wallet.model.ResponseServer;
import io.sonhar.wallet.model.ResultServer;
import io.sonhar.wallet.util.Action;
import io.sonhar.wallet.util.ActionTask;
import io.sonhar.wallet.util.ErrorHelper;
import io.sonhar.wallet.util.MaskEdit;
import io.sonhar.wallet.util.SessionHelper;
import io.sonhar.wallet.ws.RestServices;

public class ConfirmarContaActivity extends AppCompatActivity {
    EditText edtNumTelefone;
    String conta_token;
    Button btnOk;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmar_conta);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        conta_token = getIntent().getStringExtra("conta_token");
        edtNumTelefone = findViewById(R.id.edtNumTelefone);
        edtNumTelefone.addTextChangedListener(MaskEdit.mask(edtNumTelefone, MaskEdit.FORMAT_FONE));
        btnOk = findViewById(R.id.btnOk);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edtNumTelefone.getText().length() != MaskEdit.FORMAT_FONE.length()){
                    edtNumTelefone.setError("Formtado do número é inválido");
                }else{
//                    Intent myIntent = new Intent(ConfirmarContaActivity.this, ConfirmarContaSmsAcitivity.class);
//                    myIntent.putExtra("telefone", edtNumTelefone.getText().toString());
//                    startActivity(myIntent);
                    ActionTask action = new ActionTask(ConfirmarContaActivity.this, gerarCodigoVerificacao, getString(R.string.aguarde_txt));
                    gerarCodigoVerificacao.param = edtNumTelefone.getText().toString();
                    action.execute();
                }
            }
        });

    }

    Action gerarCodigoVerificacao = new Action() {
        ResultServer resultServer;
        @Override
        public void onPostExecute() {
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            if(responseServer.hasError() && responseServer.getCode() != 422){
                new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                SessionHelper.getInstance().setResponseServer(null);
            }else if (resultServer.getSuccess()){
                Intent myIntent = new Intent(ConfirmarContaActivity.this, ConfirmarContaSmsAcitivity.class);
                myIntent.putExtra("telefone", edtNumTelefone.getText().toString());
                startActivity(myIntent);
            }else{
                Toast.makeText(getApplicationContext(), resultServer.getMessage(), Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void run() {
            try {
                RestServices rs = new RestServices(mCtx);
                resultServer = rs.gerarCodigoVerificacaoConta(param, conta_token);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return (super.onOptionsItemSelected(menuItem));
    }
}

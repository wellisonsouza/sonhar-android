package io.sonhar.wallet.app;

import android.content.res.Resources;
import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import io.sonhar.wallet.R;
import io.sonhar.wallet.fragment.DadosAcessoFragment;
import io.sonhar.wallet.fragment.DadosBasicoFragment;
import io.sonhar.wallet.fragment.EnderecoFragment;
import io.sonhar.wallet.util.StepIndicator;

public class RegistrarActivity extends AppCompatActivity implements View.OnClickListener{
    StepIndicator stepIndicator;
    private Button nextButton;
    private static final int NEXTBTN_ID =R.id.nextBtn;
    private ViewPager pager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        TabLayout tabs = findViewById(R.id.sliding_tabs);
        stepIndicator = findViewById(R.id.step_indicator);
        nextButton = findViewById(NEXTBTN_ID);
        nextButton.setOnClickListener(this);

        //Set the pager with an adapter
        pager = (ViewPager)findViewById(R.id.pager);
        pager.setAdapter(new OpenAccountPagerAdapter(getSupportFragmentManager()));


        tabs.setSelectedTabIndicatorColor(Color.TRANSPARENT);
        //Bind the step indicator to the adapter
        tabs.setupWithViewPager(pager);
        stepIndicator.setupWithViewPager(pager);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                //Disable button if its the last page
                //enableBtn(position < pager.getCurrentItem());
            }

            @Override
            public void onPageSelected(int position) {
                //Disable button if its the last page

                enableBtn(position < stepIndicator.getStepsCount()-1);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    private void enableBtn(boolean isEnable){
        nextButton.setEnabled(isEnable);

        //Change color according to button state
        Resources res =getResources();
        nextButton.setTextColor(isEnable?res.getColor(android.R.color.white):res.getColor(android.R.color.darker_gray));
    }

    private class OpenAccountPagerAdapter extends FragmentStatePagerAdapter {
        String title[];
        public OpenAccountPagerAdapter(FragmentManager fm) {
            super(fm);
            title = new String[]{"Personal", "Contact", "Upload"};
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0:
                    return DadosBasicoFragment.newInstance();
                case 1:
                    return EnderecoFragment.newInstance();
                case 2:
                    return DadosAcessoFragment.newInstance();
                default:
                    return null;
            }

        }


        @Override
        public CharSequence getPageTitle(int position){
            String mTitle =title[position];
            //Capitalize first letter of string
            return mTitle.substring(0,1).toUpperCase().concat(mTitle.substring(1, mTitle.length()).toLowerCase());
        }

        @Override
        public int getCount() {
            return stepIndicator.getStepsCount();
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case NEXTBTN_ID:
                pager.setCurrentItem(pager.getCurrentItem() + 1);

                break;
        }
    }
}

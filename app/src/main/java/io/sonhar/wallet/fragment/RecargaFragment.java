package io.sonhar.wallet.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import io.sonhar.wallet.R;
import io.sonhar.wallet.adapter.CardRowListAdapter;
import io.sonhar.wallet.app.CreateCardActivity;
import io.sonhar.wallet.app.MainActivity;
import io.sonhar.wallet.model.Card;
import io.sonhar.wallet.util.Action;
import io.sonhar.wallet.ws.RestServices;

/**
 * Created by PedroPontes1 on 18/06/2018. 5496 3000 0007 6392
 */

public class RecargaFragment extends Fragment implements Fragmento{

    FloatingActionButton fabNewCard;
    MainActivity main;
    ListView lvCards;
    List<Card> cards = new ArrayList<Card>();
    public RecargaFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cards, container, false);
//        lvCards = (ListView) view.findViewById(R.id.lvCards);

        fabNewCard = view.findViewById(R.id.fabNewCard);
        fabNewCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity().getApplicationContext(),CreateCardActivity.class));
            }
        });

        return view;
    }

    @Override
    public void reload(Context c) {
//        ActionTask actionCardsTask = new ActionTask(c, cardAction, "Aguarde..");
//        cardAction.param = App.getUser(c).getId().toString();
//        actionCardsTask.execute();
    }
    @Override
    public void setMainActivity(MainActivity main) {
        this.main = main;
    }

    Action cardAction = new Action() {
        @Override
        public void onPostExecute() {
            // TODO Auto-generated method stub
            Log.i("RETORNO", "OK");
            try {

                CardRowListAdapter adapter = new CardRowListAdapter(mCtx,cards);
                lvCards.setAdapter(adapter);

            }catch(Exception ex){
                ex.printStackTrace();
            }

        }
        @Override
        public void run () {
            try {

                RestServices rs = new RestServices();

                cards = rs.getCards(param);


            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };

    @Override
    public String getTittle() {
        return getActivity().getApplicationContext().getResources().getString(R.string.cartoes_txt);
    }
}

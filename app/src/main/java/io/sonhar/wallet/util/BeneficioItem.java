package io.sonhar.wallet.util;

/**
 * Created by wellison on 26/07/2018.
 */

public class BeneficioItem {
    private String tag;
    private String nome;
    private String descricao;
    private int image;
    private String link;

    public BeneficioItem(String tag, String nome, String descricao, int image, String link) {
        this.tag = tag;
        this.nome = nome;
        this.descricao = descricao;
        this.image = image;
        this.link = link;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}

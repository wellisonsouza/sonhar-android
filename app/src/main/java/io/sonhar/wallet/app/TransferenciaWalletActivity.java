package io.sonhar.wallet.app;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PointF;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.Date;
import java.util.List;

import io.sonhar.wallet.R;
import io.sonhar.wallet.model.ResponseServer;
import io.sonhar.wallet.model.ResultServer;
import io.sonhar.wallet.model.Saida;
import io.sonhar.wallet.model.User;
import io.sonhar.wallet.util.Action;
import io.sonhar.wallet.util.ActionFaceTask;
import io.sonhar.wallet.util.ActionTask;
import io.sonhar.wallet.util.ErrorHelper;
import io.sonhar.wallet.util.MoneyTextWatcher;
import io.sonhar.wallet.util.SessionHelper;
import io.sonhar.wallet.util.Utils;
import io.sonhar.wallet.ws.RestServices;

public class TransferenciaWalletActivity extends AppCompatActivity implements Validator.ValidationListener{
    @NotEmpty(messageResId = R.string.campo_obrigatorio)
    EditText edtNomeFavorecido, edtContaFavorecido, edtValor;
    Button btnCadastrar;
    Saida saida;
    ImageView btnQrCode;
    FrameLayout viewCamera;
    QRCodeReaderView qrCodeReaderView;
    AlertDialog alerta;
    Validator validator;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transferencia_wallet);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        validator = new Validator(this);
        validator.setValidationListener(this);
        checkCameraPermission();
        edtNomeFavorecido = findViewById(R.id.edtNomeFavorecido);
        edtContaFavorecido = findViewById(R.id.edtContaFavorecido);
        btnQrCode = findViewById(R.id.btnQrCode);
        viewCamera  = (FrameLayout) findViewById(R.id.viewCamera);
        qrCodeReaderView = findViewById(R.id.qrdecoderview);
        viewCamera.setVisibility(View.GONE);
        qrCodeReaderView.setVisibility(View.GONE);
        btnCadastrar = findViewById(R.id.btnCadastrar);

        edtValor = (EditText)findViewById(R.id.edtValor);
        edtValor.addTextChangedListener(new MoneyTextWatcher(edtValor));

        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validator.validate();
            }
        });

        btnQrCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                qrCodeReaderView.setOnQRCodeReadListener(new QRCodeReaderView.OnQRCodeReadListener() {
                    @Override
                    public void onQRCodeRead(String text, PointF[] points) {
                        String[] splText = text.split(":");
                        if(splText.length > 0){
                            try{
                                String conta_id = splText[1];
                                ActionTask actionGetConta = new ActionTask(TransferenciaWalletActivity.this, getContaById, getResources().getString(R.string.aguarde_txt));
                                getContaById.param = conta_id;
                                actionGetConta.execute();

                            }catch (Exception ex){
                                Toast.makeText(getApplicationContext(),getResources().getString(R.string.qrcode_invalido_txt),Toast.LENGTH_SHORT).show();
                            }

                        }else{
                            Toast.makeText(getApplicationContext(),getResources().getString(R.string.qrcode_invalido_txt),Toast.LENGTH_SHORT).show();
                        }
                        qrCodeReaderView.setVisibility(View.GONE);
                        viewCamera.setVisibility(View.GONE);
                        qrCodeReaderView.setQRDecodingEnabled(false);
                        qrCodeReaderView.stopCamera();
                    }
                });
                // Use this function to enable/disable decoding
                qrCodeReaderView.setQRDecodingEnabled(true);

                // Use this function to change the autofocus interval (default is 5 secs)
                qrCodeReaderView.setAutofocusInterval(500L);

                // Use this function to enable/disable Torch
                qrCodeReaderView.setTorchEnabled(false);

                // Use this function to set back camera preview
                qrCodeReaderView.setBackCamera();
                qrCodeReaderView.startCamera();
                viewCamera.setVisibility(View.VISIBLE);
                qrCodeReaderView.setVisibility(View.VISIBLE);
            }
        });


    }

    Action getContaById = new Action() {

        Saida saida;
        @Override
        public void onPostExecute() {
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            if(responseServer.hasError()){
                new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                SessionHelper.getInstance().setResponseServer(null);
            }else if (saida!= null && saida.getConta_favorecido() != null){
                edtContaFavorecido.setText(saida.getConta_favorecido());
                edtNomeFavorecido.setText(saida.getNome_favorecido());
            }else{
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.conta_nao_encontrada_txt), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void run() {
            try {
                RestServices rs = new RestServices(mCtx);
                saida = rs.getFavorecidoByConta(param);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };

    Action salvarTransferencia = new Action() {

        @Override
        public void onPostExecute() {
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            if(responseServer.hasError()){
                new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                SessionHelper.getInstance().setResponseServer(null);
            } else if (saida.getId() > 0){
                Utils.showAlertTransferSuccess(App.getUser(getApplicationContext()).getNome(), TransferenciaWalletActivity.this, TransferenciaWalletActivity.this);
            }else{
                Toast.makeText(getApplicationContext(), saida.getError_message(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void run() {
            try {
                RestServices rs = new RestServices(mCtx);
                saida = rs.salvarSaida(saida);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };

    public void checkCameraPermission(){
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.CAMERA)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        if (response.isPermanentlyDenied()) {
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onValidationSucceeded() {
        saida = new Saida();
        saida.setTipo_favorecido(1);
        String valorStr = edtValor.getText().toString().replaceAll("[.]", "");
        valorStr = valorStr.replaceAll(",",".");
        if (valorStr.isEmpty() )
            saida.setValor(0.0);
        else
            saida.setValor(Double.valueOf(valorStr));
        saida.setTipo_pagamento("transferencia");
        saida.setNome_favorecido(edtNomeFavorecido.getText().toString());
        saida.setConta_favorecido(edtContaFavorecido.getText().toString());
        saida.setData_agendamento(new Date());
        if(saida.getValor()>0) {
            ActionTask actionSalvarTransferencia = new ActionTask(TransferenciaWalletActivity.this, salvarTransferencia, "Aguarde...");
            actionSalvarTransferencia.execute();
        }else{
            Toast.makeText(getApplicationContext(),R.string.informe_o_valor_txt,Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}

package io.sonhar.wallet.app;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import io.sonhar.wallet.R;
import io.sonhar.wallet.adapter.CartaoListAdapter;
import io.sonhar.wallet.adapter.SolicitacaoCartaoListAdapter;
import io.sonhar.wallet.model.EmissaoBoleto;
import io.sonhar.wallet.model.ResponseServer;
import io.sonhar.wallet.model.ResultServer;
import io.sonhar.wallet.model.SolicitacaoCartao;
import io.sonhar.wallet.util.Action;
import io.sonhar.wallet.util.ActionTask;
import io.sonhar.wallet.util.ErrorHelper;
import io.sonhar.wallet.util.SessionHelper;
import io.sonhar.wallet.util.Utils;
import io.sonhar.wallet.ws.RestServices;

public class SolicitacaoCartaoActivity extends AppCompatActivity {
    ListView lvSolicitacoes;
    FloatingActionButton btnNovoCartao;
    ArrayList<SolicitacaoCartao> solicitacaoCartaos;
    LinearLayout layoutEmpty;
    EmissaoBoleto emissaoBoleto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solicitacao_cartao);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        lvSolicitacoes = findViewById(R.id.lvlSolicitacoes);
        btnNovoCartao = findViewById(R.id.fabNewCard);
        layoutEmpty = findViewById(R.id.layoutEmpty);
        ActionTask actionLoadCartoes = new ActionTask(SolicitacaoCartaoActivity.this, getSolicitacoes, getResources().getString(R.string.aguarde_txt));
        actionLoadCartoes.execute();
        btnNovoCartao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActionTask actionSolicitarCartao = new ActionTask(SolicitacaoCartaoActivity.this, solicitarCartao, getResources().getString(R.string.aguarde_txt));
                actionSolicitarCartao.execute();
            }
        });
        lvSolicitacoes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                SolicitacaoCartao solicitacaoCartao = solicitacaoCartaos.get(i);
                EmissaoBoleto emissaoBoleto = new EmissaoBoleto();
                emissaoBoleto.setCod_boleto(solicitacaoCartao.getCod_boleto());
                emissaoBoleto.setUrl_boleto(solicitacaoCartao.getUrl_boleto());
                emissaoBoleto.setFavorecido(App.getUser(getApplication()).getNome());
                emissaoBoleto.setData_transacao(solicitacaoCartao.getCreated_at());
                emissaoBoleto.setStatus(solicitacaoCartao.getStatus());
                final Dialog dialog = new Dialog(SolicitacaoCartaoActivity.this);
                Utils.showSucessoSolicitacaoCartao(SolicitacaoCartaoActivity.this, emissaoBoleto, dialog, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return (super.onOptionsItemSelected(menuItem));
    }

    Action solicitarCartao = new Action() {
        @Override
        public void onPostExecute() {
            // TODO Auto-generated method stub
            try {
                ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();

                if(responseServer.hasError()){
                    new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                    SessionHelper.getInstance().setResponseServer(null);
                }else if (emissaoBoleto.isSuccess()){
                    emissaoBoleto.setFavorecido(App.getUser(mCtx).getNome());
                    emissaoBoleto.setStatus("BOLETO GERADO COM SUCESSO");
                    final Dialog dialog = new Dialog(mCtx);
                    Utils.showSucessoSolicitacaoCartao(SolicitacaoCartaoActivity.this, emissaoBoleto, dialog, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                            ActionTask actionLoadCartoes = new ActionTask(SolicitacaoCartaoActivity.this, getSolicitacoes, getResources().getString(R.string.aguarde_txt));
                            actionLoadCartoes.execute();
                        }
                    });
                }else{
                    Toast.makeText(getApplicationContext(), emissaoBoleto.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }catch(Exception ex){
            }
        }

        @Override
        public void run() {
            RestServices rs = new RestServices(mCtx);
            emissaoBoleto = rs.solicitarCartao();
        }

    };

    Action getSolicitacoes = new Action() {
        Locale ptBr = new Locale("pt", "BR");
        NumberFormat formatter = NumberFormat.getCurrencyInstance(ptBr);


        @Override
        public void onPostExecute() {
            // TODO Auto-generated method stub
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            try {
                if(responseServer.hasError()){
                    new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                    SessionHelper.getInstance().setResponseServer(null);
                }

                SolicitacaoCartaoListAdapter adapter = new SolicitacaoCartaoListAdapter(mCtx, solicitacaoCartaos);
                lvSolicitacoes.setAdapter(adapter);
                adapter.notifyDataSetChanged();

                if(adapter.getCount() == 0)
                    layoutEmpty.setVisibility(View.VISIBLE);
                else
                    layoutEmpty.setVisibility(View.GONE);


            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        @Override
        public void run() {
            RestServices rs = new RestServices(mCtx);
            solicitacaoCartaos = rs.getSolicitacoesCartoes();
        }

    };

}

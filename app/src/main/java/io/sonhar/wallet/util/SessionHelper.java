package io.sonhar.wallet.util;

import io.sonhar.wallet.model.ResponseServer;

/**
 * Created by wellison on 30/01/2018.
 */

public class SessionHelper {
    private static SessionHelper instance = null;
    private static ResponseServer responseServer = null;

    public static SessionHelper getInstance() {
        if (instance == null) {
            responseServer = new ResponseServer();
            return instance = new SessionHelper();
        } else {
            return instance;
        }
    }

    public void setResponseServer(ResponseServer responseServer) {
        SessionHelper.responseServer = responseServer;
    }

    public ResponseServer getResponseServer() {
        return SessionHelper.responseServer;
    }


}

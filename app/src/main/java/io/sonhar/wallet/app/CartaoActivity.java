package io.sonhar.wallet.app;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.sonhar.wallet.R;
import io.sonhar.wallet.adapter.CartaoListAdapter;
import io.sonhar.wallet.model.Cartao;
import io.sonhar.wallet.model.ResponseServer;
import io.sonhar.wallet.model.ResultServer;
import io.sonhar.wallet.util.Action;
import io.sonhar.wallet.util.ActionTask;
import io.sonhar.wallet.util.ErrorHelper;
import io.sonhar.wallet.util.MaskEdit;
import io.sonhar.wallet.util.SessionHelper;
import io.sonhar.wallet.ws.RestServices;

public class CartaoActivity extends AppCompatActivity {

    ListView lvCartoes;
    Cartao cartao;
    ArrayList<Cartao> cartoes;
    FloatingActionButton btnNovoCartao;
    @BindView(R.id.bottom_sheet)
    LinearLayout layoutBottomSheet;
    BottomSheetBehavior sheetBehavior;
    EditText edtNumCard, edtValidade, edtCvv;
    TextView lblNumCard;
    Button btnCadastrar;
//    LinearLayout layoutEmpty;
    TextView textEmpty;
    String num_cartao, cvv, validade;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cartao);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        ButterKnife.bind(this);
        edtNumCard = findViewById(R.id.edtNumCard);
        edtValidade = findViewById(R.id.edtValidade);
        edtValidade.addTextChangedListener(MaskEdit.mask(edtValidade, MaskEdit.EXPIRATION_DATE2));

        edtCvv = findViewById(R.id.edtCvv);

        lblNumCard = (TextView) findViewById(R.id.lblNumCard);
        textEmpty = findViewById(R.id.textEmpty);
        edtNumCard.clearFocus();
        edtNumCard.addTextChangedListener(new CreditCardNumberFormattingTextWatcher());
        edtNumCard.setOnEditorActionListener(new DoneOnEditorActionListener());
        btnCadastrar = (Button) findViewById(R.id.btnCadastrar);
        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);

        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        btnNovoCartao = findViewById(R.id.fabNewCard);
        btnNovoCartao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                startActivity(new Intent(CartaoActivity.this, AdicionarCartaoActivity.class));
            }
        });

        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                num_cartao = edtNumCard.getText().toString().replaceAll("\\s+","");
                validade = edtValidade.getText().toString();
                cvv = edtCvv.getText().toString();
            }
        });

        lvCartoes = (ListView) findViewById(R.id.lvCartoes);
        ActionTask actionLoadCartoes = new ActionTask(CartaoActivity.this, getCartoes, getResources().getString(R.string.aguarde_txt));
        actionLoadCartoes.execute();
        lvCartoes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //Cartao c = cartoes.get(i);
                //showOptionsDialog(c.getNumero(), c.getNome_impresso(), c.getId());
                Toast.makeText(getApplicationContext(), "A opção para recarga estará disponível em breve", Toast.LENGTH_LONG).show();
            }
        });

        //registerForContextMenu(lvCartoes);



    }

    @Override
    public void onCreateContextMenu(ContextMenu menu,
                                    View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(
                R.menu.menu_cartao, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item){
        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        String cartao_id = String.valueOf(cartoes.get(lvCartoes.getSelectedItemPosition()).getId());

        switch (item.getItemId()) {
            case R.id.visualizar_extrato:
                Intent myIntent = new Intent(CartaoActivity.this, ExtratoCartaoActivity.class);
                myIntent.putExtra("cartao_id", cartao_id);
                startActivity(myIntent);
                return true;
            case R.id.recarregar_cartao:
                Toast.makeText(this, "Opção 2 - ", Toast.LENGTH_SHORT).show();
                return true;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return (super.onOptionsItemSelected(menuItem));
    }

    private void showOptionsDialog(String numCard, String holder, final long id){
        final Dialog dialog = new Dialog(CartaoActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_menu_cards);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes((WindowManager.LayoutParams) params);

        TextView lblNumCard = (TextView)dialog.findViewById(R.id.lblNumCard);
        TextView lblHolder = (TextView)dialog.findViewById(R.id.lblHolder);
        Button btnRecarga = (Button)dialog.findViewById(R.id.btnRecarga);
        Button btnExtrato = (Button)dialog.findViewById(R.id.btnExtrato);

        lblHolder.setText(holder);
        lblNumCard.setText(numCard);

        btnExtrato.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(CartaoActivity.this, ExtratoCartaoActivity.class);
                myIntent.putExtra("cartao_id", String.valueOf(id));
                startActivity(myIntent);
            }
        });

        btnRecarga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(CartaoActivity.this, TransferenciaCartaoActivity.class);
                myIntent.putExtra("cartao_id", String.valueOf(id));
                startActivity(myIntent);
            }
        });

        dialog.show();

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                ActionTask actionLoadCartoes = new ActionTask(CartaoActivity.this, getCartoes, getResources().getString(R.string.aguarde_txt));
                actionLoadCartoes.execute();
            }
        });
    }

    Action getCartoes = new Action() {
        Locale ptBr = new Locale("pt", "BR");
        NumberFormat formatter = NumberFormat.getCurrencyInstance(ptBr);


        @Override
        public void onPostExecute() {
            // TODO Auto-generated method stub
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            try {
                if(responseServer.hasError()){
                    new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                    SessionHelper.getInstance().setResponseServer(null);
                }
                CartaoListAdapter adapter = new CartaoListAdapter(mCtx, cartoes);
                lvCartoes.setAdapter(adapter);
                adapter.notifyDataSetChanged();

                if(cartoes.size() == 0)
                    textEmpty.setVisibility(View.VISIBLE);
                else
                    textEmpty.setVisibility(View.GONE);


            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        @Override
        public void run() {
            RestServices rs = new RestServices(mCtx);
            cartoes = rs.getCartoes();
        }

    };



    /**
     * Formatting a credit card number: #### #### #### #######
     */
    class CreditCardNumberFormattingTextWatcher implements TextWatcher {

        private boolean lock;

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            lblNumCard.setText(s.toString());
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (lock || s.length() > 16) {
                return;
            }
            lock = true;
            for (int i = 4; i < s.length(); i += 5) {
                if (s.toString().charAt(i) != ' ') {
                    s.insert(i, " ");
                }

            }
            lock = false;


        }
    }

    class DoneOnEditorActionListener implements TextView.OnEditorActionListener {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                InputMethodManager imm = (InputMethodManager)v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                return true;
            }
            return false;
        }
    }
}

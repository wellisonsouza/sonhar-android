package io.sonhar.wallet.app;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import io.sonhar.wallet.R;
import io.sonhar.wallet.adapter.ExtratoItemAdapter;
import io.sonhar.wallet.model.Extrato;
import io.sonhar.wallet.model.ResponseServer;
import io.sonhar.wallet.model.Saldo;
import io.sonhar.wallet.util.Action;
import io.sonhar.wallet.util.ActionTask;
import io.sonhar.wallet.util.ErrorHelper;
import io.sonhar.wallet.util.SessionHelper;
import io.sonhar.wallet.ws.RestServices;

public class TransacaoActivity extends AppCompatActivity {

    ListView lvExtratos;
    Saldo saldo;
    ArrayList<Extrato> extratos;
    TextView txtSaldoLiberado, txtSaldoAhLiberar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transacao);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        lvExtratos = (ListView) findViewById(R.id.lvExtratos);
        txtSaldoAhLiberar = (TextView) findViewById(R.id.txtSaldoAhLiberar);
        txtSaldoLiberado = (TextView) findViewById(R.id.txtSaldoLiberado);
        ActionTask actionLoadExtratos = new ActionTask(TransacaoActivity.this, getExtratos, getResources().getString(R.string.aguarde_txt));
        actionLoadExtratos.execute();
        ActionTask actionLoadSaldo = new ActionTask(getApplicationContext(), getSaldo, getResources().getString(R.string.aguarde_txt));
        actionLoadSaldo.execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return (super.onOptionsItemSelected(menuItem));
    }

    Action getExtratos = new Action() {
        Locale ptBr = new Locale("pt", "BR");
        NumberFormat formatter = NumberFormat.getCurrencyInstance(ptBr);

        @Override
        public void onPostExecute() {
            // TODO Auto-generated method stub
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            try {
                if(responseServer.hasError()){
                    new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                    SessionHelper.getInstance().setResponseServer(null);
                }
                ExtratoItemAdapter adapter = new ExtratoItemAdapter(mCtx, extratos);
                lvExtratos.setAdapter(adapter);
                adapter.notifyDataSetChanged();

            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        @Override
        public void run() {
            RestServices rs = new RestServices(mCtx);
            extratos = rs.getExtratoWallet(1);
        }

    };

    Action getSaldo = new Action() {
        Locale ptBr = new Locale("pt", "BR");
        NumberFormat formatter = NumberFormat.getCurrencyInstance(ptBr);

        @Override
        public void onPostExecute() {
            // TODO Auto-generated method stub
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            try {
                if(responseServer.hasError()){
                    new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                    SessionHelper.getInstance().setResponseServer(null);
                } else if (saldo != null){
                    txtSaldoAhLiberar.setText(formatter.format(saldo.getValorEmReais(saldo.getValor_receber_centavos())));
                    txtSaldoLiberado.setText(formatter.format(saldo.getValorEmReais(saldo.getValor_disponivel_centavos())));
                }else{
                    Toast.makeText(getApplicationContext(), "Não foi possível carregar o saldo", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        @Override
        public void run() {
            RestServices rs = new RestServices(mCtx);
            saldo = rs.getSaldo();
        }

    };
}

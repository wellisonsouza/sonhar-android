
package io.sonhar.wallet.app;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import io.sonhar.wallet.R;
import io.sonhar.wallet.model.EmissaoBoleto;
import io.sonhar.wallet.model.ResponseServer;
import io.sonhar.wallet.util.Action;
import io.sonhar.wallet.util.ActionTask;
import io.sonhar.wallet.util.ErrorHelper;
import io.sonhar.wallet.util.MoneyTextWatcher;
import io.sonhar.wallet.util.SessionHelper;
import io.sonhar.wallet.util.Utils;
import io.sonhar.wallet.ws.RestServices;


public class NovoDepositoActivity extends AppCompatActivity implements Validator.ValidationListener {

    @NotEmpty(messageResId = R.string.campo_obrigatorio)
    EditText edtValor;
    Button btnCadastrar;
    EmissaoBoleto emissaoBoleto;
    double valor;
    Validator validator;
    private String conta_token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novo_deposito);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        validator = new Validator(this);
        validator.setValidationListener(this);
        edtValor = (EditText)findViewById(R.id.edtValor);
        edtValor.addTextChangedListener(new MoneyTextWatcher(edtValor));
        btnCadastrar = findViewById(R.id.btnCadastrar);
        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validator.validate();
            }
        });
        ActionTask actionContaToken = new ActionTask(NovoDepositoActivity.this, getContaToken, getResources().getString(R.string.aguarde_txt));
        actionContaToken.execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return (super.onOptionsItemSelected(menuItem));
    }

    Action getContaToken = new Action() {

        @Override
        public void onPostExecute() {
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            if(responseServer.hasError()){
                new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                SessionHelper.getInstance().setResponseServer(null);
            }
        }

        @Override
        public void run() {
            try {
                RestServices rs = new RestServices(mCtx);
                conta_token = rs.getTokenByContaId(App.getUser(mCtx).getCurrent_conta_id());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };

    Action gerarBoleto = new Action() {
        @Override
        public void onPostExecute() {
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            if(responseServer.hasError()){
                new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                SessionHelper.getInstance().setResponseServer(null);
            }else if (emissaoBoleto.isSuccess()){
                emissaoBoleto.setFavorecido(App.getUser(mCtx).getNome());
                emissaoBoleto.setValor(valor);
                final Dialog dialog = new Dialog(mCtx);
                Utils.showSucessoDepositoBoleto(NovoDepositoActivity.this, emissaoBoleto, dialog, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        finish();
                    }
                });
            }else{
                Toast.makeText(getApplicationContext(), emissaoBoleto.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void run() {
            try {
                RestServices rs = new RestServices(mCtx);
                emissaoBoleto = rs.emitirBoletoDeposito(conta_token, valor);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };

    @Override
    public void onValidationSucceeded() {
        String valorStr = edtValor.getText().toString().replaceAll("[.]", "");
        valorStr = valorStr.replaceAll(",",".");

        if (valorStr.isEmpty() )
            valor = 0.0;
        else
            valor = Double.valueOf(valorStr);

        if(valor>=5) {
            ActionTask actionGerarBoleto = new ActionTask(NovoDepositoActivity.this, gerarBoleto, "Aguarde...");
            actionGerarBoleto.execute();
        }else{
            Toast.makeText(getApplicationContext(),R.string.informe_o_valor_txt,Toast.LENGTH_LONG).show();
        }


    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}

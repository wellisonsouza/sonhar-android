package io.sonhar.wallet.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.sonhar.wallet.R;
import io.sonhar.wallet.adapter.CardRowSpinnerAdapter;
import io.sonhar.wallet.app.App;
import io.sonhar.wallet.app.FaceGraphic;
import io.sonhar.wallet.app.MainActivity;
import io.sonhar.wallet.model.Card;
import io.sonhar.wallet.model.ResultServer;
import io.sonhar.wallet.model.User;
import io.sonhar.wallet.util.Action;
import io.sonhar.wallet.util.ActionTask;
import io.sonhar.wallet.util.CameraSourcePreview;
import io.sonhar.wallet.util.GraphicOverlay;
import io.sonhar.wallet.util.MoneyTextWatcher;
import io.sonhar.wallet.util.Utils;
import io.sonhar.wallet.ws.RestServices;

/**
 * Created by PedroPontes1 on 18/06/2018.
 */

public class MoreActionsFragment_modal extends Fragment implements Fragmento{

    Boolean verifingFace = false;

    Dialog dialogVerifyFace;
    LinearLayout btnTransferir;
    LinearLayout btnReceber;
    MainActivity main;
    public Context mCtxActivity;
    List<Card> cards = new ArrayList<Card>();
    Fragment f;
    public MoreActionsFragment_modal() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_transfer, container, false);

        f = this;

        btnTransferir = (LinearLayout) view.findViewById(R.id.btnTransferir);
        btnReceber = (LinearLayout) view.findViewById(R.id.btnReceber);

        btnReceber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActionTask actionCardsTask = new ActionTask(getActivity(), cardAction, "Aguarde..");
                cardAction.param = App.getUser(getActivity().getApplicationContext()).getId().toString();
                cardAction.param2 = "receive";
                actionCardsTask.execute();
            }
        });
        btnTransferir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActionTask actionCardsTask = new ActionTask(getActivity(), cardAction, "Aguarde..");
                cardAction.param = App.getUser(getActivity().getApplicationContext()).getId().toString();
                cardAction.param2 = "transfer";
                actionCardsTask.execute();
            }
        });

        return view;
    }

    @Override
    public void reload(Context c) {

    }
    @Override
    public void setMainActivity(MainActivity main) {
        this.main = main;
    }


    private void showReceiveMoneyDialog(){
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_receive_money);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.FILL_PARENT;
        dialog.getWindow().setAttributes((WindowManager.LayoutParams) params);

        Button btnCancel = (Button)dialog.findViewById(R.id.btnOk);

        final ImageView imgQrCode = (ImageView)dialog.findViewById(R.id.imgQrCode);
        Spinner spnCards = (Spinner) dialog.findViewById(R.id.spnCards);

        CardRowSpinnerAdapter adapter = new CardRowSpinnerAdapter(getActivity(),cards);
        spnCards.setAdapter(adapter);

        spnCards.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                try{
                    Card card = cards.get(pos);
                    Bitmap qrcode= Utils.qrcodeAsBitmap("C:" + card.getNumero_cartao(), 256);
                    imgQrCode.setImageBitmap(qrcode);
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        btnCancel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });



        dialog.show();
    }

    private Card cardSelected;
    private void showTransferMoneyDialog(){
        final Dialog dialog = new Dialog(getActivity());
        cardSelected = new Card();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_transfer_money);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.FILL_PARENT;
        dialog.getWindow().setAttributes((WindowManager.LayoutParams) params);

        Button btnCancel = (Button)dialog.findViewById(R.id.btnCancel);
        final Button btnCancelReader = (Button)dialog.findViewById(R.id.btnCancelReader);
        Button btnOk = (Button)dialog.findViewById(R.id.btnOk);
        final EditText edtNumCard = (EditText)dialog.findViewById(R.id.edtNumCard);
        final EditText edtValor = (EditText)dialog.findViewById(R.id.edtValor);
        ImageView btnQrCode = (ImageView)dialog.findViewById(R.id.btnQrCode);
        final FrameLayout viewCamera = (FrameLayout) dialog.findViewById(R.id.viewCamera);
        final QRCodeReaderView qrCodeReaderView = (QRCodeReaderView)dialog.findViewById(R.id.qrdecoderview);
        final ImageView imgQrCode = (ImageView)dialog.findViewById(R.id.imgQrCode);
        Spinner spnCards = (Spinner) dialog.findViewById(R.id.spnCards);

        edtValor.addTextChangedListener(new MoneyTextWatcher(edtValor));
        edtNumCard.addTextChangedListener(new CreditCardNumberFormattingTextWatcher());

        viewCamera.setVisibility(View.GONE);
        qrCodeReaderView.setVisibility(View.GONE);

        CardRowSpinnerAdapter adapter = new CardRowSpinnerAdapter(getActivity(),cards);
        spnCards.setAdapter(adapter);

        spnCards.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                try{
                    cardSelected = cards.get(pos);
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        btnQrCode.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                qrCodeReaderView.setOnQRCodeReadListener(new QRCodeReaderView.OnQRCodeReadListener() {
                    @Override
                    public void onQRCodeRead(String text, PointF[] points) {

                        String[] splText = text.split(":");

                        if(splText.length > 0){

                            try{
                                String numCard = splText[1];

                                Editable s = new SpannableStringBuilder(numCard);
                                for (int i = 4; i < s.length(); i += 5) {
                                    if (s.toString().charAt(i) != ' ') {
                                        s.insert(i, " ");
                                    }

                                }

                                edtNumCard.setText(s.toString());
                            }catch (Exception ex){
                                Toast.makeText(getActivity(),"QR-Code Inválido.",Toast.LENGTH_SHORT).show();
                            }

                        }else{
                            Toast.makeText(getActivity(),"QR-Code Inválido.",Toast.LENGTH_SHORT).show();
                        }

                        qrCodeReaderView.setVisibility(View.GONE);
                        viewCamera.setVisibility(View.GONE);
                        qrCodeReaderView.setQRDecodingEnabled(false);
                        qrCodeReaderView.stopCamera();


                    }
                });
                // Use this function to enable/disable decoding
                qrCodeReaderView.setQRDecodingEnabled(true);

                // Use this function to change the autofocus interval (default is 5 secs)
                qrCodeReaderView.setAutofocusInterval(500L);

                // Use this function to enable/disable Torch
                qrCodeReaderView.setTorchEnabled(false);

                // Use this function to set back camera preview
                qrCodeReaderView.setBackCamera();
                qrCodeReaderView.startCamera();
                viewCamera.setVisibility(View.VISIBLE);
                qrCodeReaderView.setVisibility(View.VISIBLE);
            }
        });

        btnCancelReader.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                qrCodeReaderView.setVisibility(View.GONE);
                viewCamera.setVisibility(View.GONE);
                qrCodeReaderView.setQRDecodingEnabled(false);
                qrCodeReaderView.stopCamera();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                // AEEEEE
                showVerifyFaceDialog();
            }
        });



        dialog.show();
    }


    Action cardAction = new Action() {
        @Override
        public void onPostExecute() {
            // TODO Auto-generated method stub
            Log.i("RETORNO", "OK");
            try {

                if(param2.equalsIgnoreCase("receive")){
                    showReceiveMoneyDialog();
                }else{
                    showTransferMoneyDialog();
                }
            }catch(Exception ex){
                ex.printStackTrace();
            }

        }
        @Override
        public void run () {
            try {

                RestServices rs = new RestServices();

                cards = rs.getCards(param);


            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };


    class CreditCardNumberFormattingTextWatcher implements TextWatcher {

        private boolean lock;

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (lock || s.length() > 16) {
                return;
            }
            lock = true;
            for (int i = 4; i < s.length(); i += 5) {
                if (s.toString().charAt(i) != ' ') {
                    s.insert(i, " ");
                }

            }
            lock = false;


        }
    }


    // VALIDACAO POR RECONHECIMENTO FACIAL

    Boolean findRealFace = false;
    private static final String TAG = "FaceTracker";

    private CameraSourcePreview mPreview;

    private CameraSource mCameraSource = null;
    private GraphicOverlay mGraphicOverlay;

    private static final int RC_HANDLE_GMS = 9001;
    // permission request codes need to be < 256
    private static final int RC_HANDLE_CAMERA_PERM = 2;

    private void showVerifyFaceDialog(){
        dialogVerifyFace = new Dialog(getActivity());
        dialogVerifyFace.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogVerifyFace.setContentView(R.layout.dialog_verify_face);
        WindowManager.LayoutParams params = dialogVerifyFace.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.FILL_PARENT;
        dialogVerifyFace.getWindow().setAttributes((WindowManager.LayoutParams) params);

        Button btnCancel = (Button)dialogVerifyFace.findViewById(R.id.btnOk);
        mPreview = (CameraSourcePreview) dialogVerifyFace.findViewById(R.id.preview);
        mGraphicOverlay = (GraphicOverlay) dialogVerifyFace.findViewById(R.id.faceOverlay);


        btnCancel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                dialogVerifyFace.dismiss();
            }
        });


        dialogVerifyFace.show();


        int rc = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
        if (rc == PackageManager.PERMISSION_GRANTED) {
            createCameraSource();
        } else {
            requestCameraPermission();
        }
    }

    private void requestCameraPermission() {
        Log.w(TAG, "Camera permission is not granted. Requesting permission");

        final String[] permissions = new String[]{Manifest.permission.CAMERA};

        if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(getActivity(), permissions, RC_HANDLE_CAMERA_PERM);
            return;
        }

        final Activity thisActivity = getActivity();

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityCompat.requestPermissions(thisActivity, permissions,
                        RC_HANDLE_CAMERA_PERM);
            }
        };

        Snackbar.make(mGraphicOverlay, R.string.permission_camera_rationale,
                Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.ok, listener)
                .show();
    }

    /**
     * Creates and starts the camera.  Note that this uses a higher resolution in comparison
     * to other detection examples to enable the barcode detector to detect small barcodes
     * at long distances.
     */
    private void createCameraSource() {

        Context context = getActivity().getApplicationContext();

        // You can use your own settings for your detector
        FaceDetector detector = new FaceDetector.Builder(context)
                .setClassificationType(FaceDetector.ACCURATE_MODE)
                .setProminentFaceOnly(true)
                .build();

        // This is how you merge myFaceDetector and google.vision detector
        MyFaceDetector myFaceDetector = new MyFaceDetector(detector);

        // You can use your own processor
        myFaceDetector.setProcessor(
                new MultiProcessor.Builder<>(new GraphicFaceTrackerFactory())
                        .build());

        if (!myFaceDetector.isOperational()) {
            Log.w(TAG, "Face detector dependencies are not yet available.");
        }

        // You can use your own settings for CameraSource
        mCameraSource = new CameraSource.Builder(context, myFaceDetector)
                .setRequestedPreviewSize(640, 480)
                .setFacing(CameraSource.CAMERA_FACING_FRONT)
                .setRequestedFps(30.0f)
                .build();

        startCameraSource();
    }

    //==============================================================================================
    // Camera Source Preview
    //==============================================================================================

    /**
     * Starts or restarts the camera source, if it exists.  If the camera source doesn't exist yet
     * (e.g., because onResume was called before the camera source was created), this will be called
     * again when the camera source is created.
     */
    private void startCameraSource() {

        // check that the device has play services available.
        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
                getActivity().getApplicationContext());
        if (code != ConnectionResult.SUCCESS) {
            Dialog dlg =
                    GoogleApiAvailability.getInstance().getErrorDialog(getActivity(), code, RC_HANDLE_GMS);
            dlg.show();
        }

        if (mCameraSource != null) {
            try {

                mPreview.start(mCameraSource, mGraphicOverlay);

                Camera mCamera = openFrontFacingCameraGingerbread();

                Camera.Parameters params = mCamera.getParameters();

                params.setExposureCompensation(params.getMaxExposureCompensation());

                if(params.isAutoExposureLockSupported()) {
                    params.setAutoExposureLock(false);
                }

                mCamera.setParameters(params);
            } catch (IOException e) {
                Log.e(TAG, "Unable to start camera source.", e);
                mCameraSource.release();
                mCameraSource = null;
            }
        }
    }

    //==============================================================================================
    // Graphic Face Tracker
    //==============================================================================================

    /**
     * Factory for creating a face tracker to be associated with a new face.  The multiprocessor
     * uses this factory to create face trackers as needed -- one for each individual.
     */
    private class GraphicFaceTrackerFactory implements MultiProcessor.Factory<Face> {
        @Override
        public Tracker<Face> create(Face face) {
            return new GraphicFaceTracker(mGraphicOverlay);
        }
    }

    /**
     * Face tracker for each detected individual. This maintains a face graphic within the app's
     * associated face overlay.
     */
    private class GraphicFaceTracker extends Tracker<Face> {
        private GraphicOverlay mOverlay;
        private FaceGraphic mFaceGraphic;

        GraphicFaceTracker(GraphicOverlay overlay) {
            mOverlay = overlay;
            mFaceGraphic = new FaceGraphic(overlay);
        }

        /**
         * Start tracking the detected face instance within the face overlay.
         */
        @Override
        public void onNewItem(int faceId, Face item) {
            mFaceGraphic.setId(faceId);
            findRealFace = true;
        }

        /**
         * Update the position/characteristics of the face within the overlay.
         */
        @Override
        public void onUpdate(FaceDetector.Detections<Face> detectionResults, Face face) {
            mOverlay.add(mFaceGraphic);
            mFaceGraphic.updateFace(face);
        }

        /**
         * Hide the graphic when the corresponding face was not detected.  This can happen for
         * intermediate frames temporarily (e.g., if the face was momentarily blocked from
         * view).
         */
        @Override
        public void onMissing(FaceDetector.Detections<Face> detectionResults) {
            mOverlay.remove(mFaceGraphic);
        }

        /**
         * Called when the face is assumed to be gone for good. Remove the graphic annotation from
         * the overlay.
         */
        @Override
        public void onDone() {
            mOverlay.remove(mFaceGraphic);
            findRealFace = false;
        }
    }

    class MyFaceDetector extends Detector<Face> {
        private Detector<Face> mDelegate;

        MyFaceDetector(Detector<Face> delegate) {
            mDelegate = delegate;
        }

        public SparseArray<Face> detect(Frame frame) {
            YuvImage yuvImage = new YuvImage(frame.getGrayscaleImageData().array(), ImageFormat.NV21, frame.getMetadata().getWidth(), frame.getMetadata().getHeight(), null);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            yuvImage.compressToJpeg(new Rect(0, 0, frame.getMetadata().getWidth(), frame.getMetadata().getHeight()), 100, byteArrayOutputStream);
            byte[] jpegArray = byteArrayOutputStream.toByteArray();
            Bitmap TempBitmap = BitmapFactory.decodeByteArray(jpegArray, 0, jpegArray.length);

            Matrix matrix = new Matrix();
            matrix.postRotate(-90);
            Bitmap rotatedBitmap = Bitmap.createBitmap(TempBitmap , 0, 0, TempBitmap.getWidth(), TempBitmap.getHeight(), matrix, true);
            ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();

            rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream2);
            byte[] byteArray = byteArrayOutputStream2.toByteArray();

            String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

            if (findRealFace && !verifingFace) {
                verifingFace = true;
                findRealFace = false;

                mPreview.stop();
                dialogVerifyFace.dismiss();
                verifyPhotoAction.param = encoded;
                ActionTask actionLoadGames = new ActionTask(mCtxActivity, verifyPhotoAction, "Autenticando..");
                actionLoadGames.execute();
            }

            return mDelegate.detect(frame);
        }

        public boolean isOperational() {
            return mDelegate.isOperational();
        }

        public boolean setFocus(int id) {
            return mDelegate.setFocus(id);
        }
    }

    private Camera openFrontFacingCameraGingerbread() {
        int cameraCount = 0;
        Camera cam = null;
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        cameraCount = Camera.getNumberOfCameras();
        for (int camIdx = 0; camIdx < cameraCount; camIdx++) {
            Camera.getCameraInfo(camIdx, cameraInfo);
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                try {
                    cam = Camera.open(camIdx);
                } catch (RuntimeException e) {
                    Log.e(TAG, "Camera failed to open: " + e.getLocalizedMessage());
                }
            }
        }

        return cam;
    }

    Action verifyPhotoAction = new Action() {
        User user = new User();
        ResultServer resultServer;
        @Override
        public void onPostExecute() {
            // TODO Auto-generated method stub
            Log.i("RETORNO", "OK");
            try {
                verifingFace = false;

                Toast.makeText(mCtx,resultServer.getMessage(),Toast.LENGTH_LONG).show();

//                    resizeListView(lvAulas);
            }catch(Exception ex){
                ex.printStackTrace();
            }

        }
        @Override
        public void run () {
            try {

                RestServices rs = new RestServices();
                user.setId(new Long(2));
//                rs.send_photo(param,user);
                resultServer = rs.vefify_face(param,user);


            } catch (Exception ex) {
                ex.printStackTrace();
                user.setId(null);
            }
        }
    };

    @Override
    public String getTittle() {
        return getActivity().getApplicationContext().getResources().getString(R.string.cartoes_txt);
    }

}

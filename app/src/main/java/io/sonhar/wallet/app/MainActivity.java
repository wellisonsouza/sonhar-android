package io.sonhar.wallet.app;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.crashlytics.android.Crashlytics;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import io.sonhar.wallet.BuildConfig;
import io.sonhar.wallet.R;
import io.sonhar.wallet.adapter.CartaoListAdapter;
import io.sonhar.wallet.adapter.SolicitacaoCartaoListAdapter;
import io.sonhar.wallet.fragment.CardsFragment;
import io.sonhar.wallet.fragment.EntradaFragment;
import io.sonhar.wallet.fragment.Fragmento;
import io.sonhar.wallet.fragment.HomeFragment;
import io.sonhar.wallet.fragment.PaymentFragment;
import io.sonhar.wallet.fragment.PontosRecargaFragment;
import io.sonhar.wallet.fragment.ProfileFragment;
import io.sonhar.wallet.fragment.RecargaFragment;
import io.sonhar.wallet.fragment.TimeLineFragment;
import io.sonhar.wallet.fragment.TransactionFragment;
import io.sonhar.wallet.fragment.TransferFragment;
import io.sonhar.wallet.model.Cartao;
import io.sonhar.wallet.model.ResponseServer;
import io.sonhar.wallet.model.ResultServer;
import io.sonhar.wallet.model.SolicitacaoCartao;
import io.sonhar.wallet.model.User;
import io.sonhar.wallet.util.Action;
import io.sonhar.wallet.util.ActionFaceTask;
import io.sonhar.wallet.util.ActionTask;
import io.sonhar.wallet.util.BeneficioItem;
import io.sonhar.wallet.util.ErrorHelper;
import io.sonhar.wallet.util.ListaBeneficioModal;
import io.sonhar.wallet.util.LoadTask;
import io.sonhar.wallet.util.SessionHelper;
import io.sonhar.wallet.util.Utils;
import io.sonhar.wallet.ws.RestServices;
import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity {

    AlertDialog alerta;


    LinearLayout btnLogout;

    LinearLayout btnHistorico;
    LinearLayout btnCards;
    LinearLayout btnTransfer;
    LinearLayout btnPayBoleto;
//    LinearLayout btnVendaDireta;
    LinearLayout btnRecargaTelefone;
    LinearLayout btnSolicitacoes;
    LinearLayout btnEntradas;
    LinearLayout btnDepositoBoleto;
    LinearLayout btnPontoRecarga;
    LinearLayout btnDocumentacao;

    LinearLayout btnDoctorAppoint;
    LinearLayout btnMedicalExam;
    LinearLayout btnDrRey;
    LinearLayout btnFoodAid;
    LinearLayout btnFamilyDoctor;
    LinearLayout btnMedicines;
    LinearLayout btnHospital;
    LinearLayout btn;

    TextView lblNome, lblVersao, txtNumConta;

    int lengthMenuUser=0;

    boolean hasCard = false;

    static final int PICK_FACE_REQUEST = 12;
    public static int type_faceCamera = 0;
    // types face camera
    // 0 - cadastro
    // 1 - verify
    // 2 - payment boleto
    // 3 - recharge phone

    // FRAGMENTS
    TimeLineFragment timeLineFragment;
    CardsFragment cardsFragment;
    TransferFragment transferFragment;
    PaymentFragment paymentFragment;
    RecargaFragment recargaFragment;
    ProfileFragment profileFragment;
    EntradaFragment entradaFragment;
    HomeFragment homeFragment;
    TransactionFragment transactionFragment;
    PontosRecargaFragment pontosRecargaFragment;
    FragmentManager fm;
    Fragmento actualFrag;
    Context mCtx;

    AHBottomNavigation bottomBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maiin);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        btnLogout = (LinearLayout) findViewById(R.id.btnLogout);
        Fabric.with(this, new Crashlytics());
        try {
            Crashlytics.setUserEmail(App.getUser(MainActivity.this).getEmail());
        } catch(NullPointerException e) {

        }
        if(App.getUser(getApplicationContext()).getNum_conta() == null){
            App.logout(getApplicationContext());
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }
        btnHistorico = (LinearLayout) findViewById(R.id.btnHistorico);
        btnCards = (LinearLayout) findViewById(R.id.btnCards);
        btnTransfer = (LinearLayout) findViewById(R.id.btnTransfer);
        btnPayBoleto = (LinearLayout) findViewById(R.id.btnPayBoleto);
        btnEntradas = (LinearLayout) findViewById(R.id.btnEntradas);
//        btnVendaDireta = (LinearLayout) findViewById(R.id.btnVendaDireta);
        btnRecargaTelefone = (LinearLayout) findViewById(R.id.btnRecargaTelefone);
        btnSolicitacoes = (LinearLayout) findViewById(R.id.btnSolicitacoes);
        btnDepositoBoleto = (LinearLayout) findViewById(R.id.btnDepositoBoleto);
        btnPontoRecarga = (LinearLayout) findViewById(R.id.btnPontoRecarga);
        btnDocumentacao = (LinearLayout) findViewById(R.id.btnDocumentacao);
        bottomBar = (AHBottomNavigation) findViewById(R.id.bottomBar);
        txtNumConta = findViewById(R.id.txtNumConta);
        lblNome = findViewById(R.id.lblNome);
        lblVersao  = findViewById(R.id.lblVersao);
        lblNome.setText(App.getUser(getApplicationContext()).getNome());
        lblVersao.setText("Versão: "+BuildConfig.VERSION_NAME);

        txtNumConta.setText("CONTA Nº " + App.getUser(getApplicationContext()).getNum_conta());

        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int sizeMenuUser = (size.y*110)/1280;


        lengthMenuUser = Utils.dpToPx(sizeMenuUser, getApplicationContext());

        initListeners();
        initFrags();
        initBottomNav();
        checkHasCard();

    }

    @Override
    public void onResume() {
        super.onResume();
        actualFrag.reload(MainActivity.this);

    }

    private void checkHasCard(){
        LoadTask action = new LoadTask(MainActivity.this, getCartoes);
        action.execute();
    }

    Action getCartoes = new Action() {
        ArrayList<Cartao> cartoes;
        @Override
        public void onPostExecute() {
            // TODO Auto-generated method stub
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            try {
                if(responseServer.hasError() == false && cartoes.size() == 0){
                    LoadTask action = new LoadTask(MainActivity.this, getSolicitacoes);
                    action.execute();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        @Override
        public void run() {
            RestServices rs = new RestServices(mCtx);
            cartoes = rs.getCartoes();
        }

    };

    Action getSolicitacoes = new Action() {
        ArrayList<SolicitacaoCartao> solicitacaoCartaos;
        @Override
        public void onPostExecute() {
            // TODO Auto-generated method stub
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            try {
                if(responseServer.hasError() == false && solicitacaoCartaos.size() == 0){
                    Utils.showSolicitarCartao(mCtx);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        @Override
        public void run() {
            RestServices rs = new RestServices(mCtx);
            solicitacaoCartaos = rs.getSolicitacoesCartoes();
        }

    };

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initFrags(){
        fm = getSupportFragmentManager();
        timeLineFragment = new TimeLineFragment();
        cardsFragment = new CardsFragment();
        transferFragment = new TransferFragment();
        profileFragment = new ProfileFragment();
        paymentFragment = new PaymentFragment();
        recargaFragment = new RecargaFragment();

        homeFragment = new HomeFragment();
        transactionFragment = new TransactionFragment();
        pontosRecargaFragment = new PontosRecargaFragment();


        setFragment(timeLineFragment);
    }

    private void setFragment(Fragment f){
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.fragmentMain, f);
        transaction.commitAllowingStateLoss();
        fm.executePendingTransactions();

        actualFrag = (Fragmento)f;
        actualFrag.setMainActivity(this);
        getSupportActionBar().setTitle(actualFrag.getTittle());
        closeDrawer();
        actualFrag.reload(MainActivity.this);
//        goToFragmentWithoutAnim(menuFragment);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (type_faceCamera == 1 || type_faceCamera == 2 || type_faceCamera == 3) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                String base64 = data.getStringExtra("face64");
                Log.i("FACE 64", base64);

                verifyPhotoAction.param = base64;

                ActionFaceTask actionLoadGames = new ActionFaceTask(MainActivity.this, verifyPhotoAction, "Autenticando..");
                actionLoadGames.execute();
            }
        } else {
            if (resultCode == RESULT_OK) {
                String base64 = data.getStringExtra("face64");
                Log.i("FACE 64", base64);

                sendPhotoAction.param = base64;


                ActionFaceTask actionLoadGames = new ActionFaceTask(MainActivity.this, sendPhotoAction, "Aguarde..");
                actionLoadGames.execute();
            }
        }
    }

    Action sendPhotoAction = new Action() {
        User user = new User();
        ResultServer resultServer;
        @Override
        public void onPostExecute() {
            // TODO Auto-generated method stub
            Log.i("RETORNO", "OK");
            try {


                Toast.makeText(mCtx,resultServer.getMessage(),Toast.LENGTH_LONG).show();

            }catch(Exception ex){
                ex.printStackTrace();
            }

        }
        @Override
        public void run () {
            try {

                RestServices rs = new RestServices();
                user = App.getUser(getApplicationContext());
//
                resultServer = rs.send_photo(param,user);

            } catch (Exception ex) {
                ex.printStackTrace();
                user.setId(null);
            }
        }
    };

    Action verifyPhotoAction = new Action() {
        User user = new User();
        ResultServer resultServer;
        @Override
        public void onPostExecute() {
            // TODO Auto-generated method stub
            Log.i("RETORNO", "OK");
            try {


                if (resultServer.getSuccess()) {

                    if (type_faceCamera == 1) {
                        final TransferFragment frag = (TransferFragment) actualFrag;


                        String msg = "Olá, " + App.getUser(getApplicationContext()).getNome();

                        final Dialog dialog = new Dialog(mCtx);
                        Utils.showAlertFaceSuccess(msg, dialog, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
//                                frag.dialogTransfer.dismiss();
                                // Utils.showAlertTransferSuccess(App.getUser(getApplicationContext()).getNome(), mCtx);
                            }
                        });
                    }else if (type_faceCamera == 2){
                        final PaymentFragment frag = (PaymentFragment) actualFrag;


                        String msg = "Olá, " + App.getUser(getApplicationContext()).getNome();

                        final Dialog dialog = new Dialog(mCtx);
                        Utils.showAlertFaceSuccess(msg, dialog, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                                frag.dialogBoleto.dismiss();
                                //Utils.showAlertTransferSuccess(App.getUser(getApplicationContext()).getNome(), mCtx);
                            }
                        });
                    }else if (type_faceCamera == 3){
                        final PaymentFragment frag = (PaymentFragment) actualFrag;


                        String msg = "Olá, " + App.getUser(getApplicationContext()).getNome();

                        final Dialog dialog = new Dialog(mCtx);
                        Utils.showAlertFaceSuccess(msg, dialog, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                                frag.dialogRecarga.dismiss();
                                //Utils.showAlertTransferSuccess(App.getUser(getApplicationContext()).getNome(), mCtx);
                            }
                        });
                    }
                }else{
                    String msg = "Infelizmente você não é "+App.getUser(getApplicationContext()).getNome();
                    Utils.showAlertFaceError(msg,mCtx);
                }

            }catch(Exception ex){
                ex.printStackTrace();
            }

        }
        @Override
        public void run () {
            try {

                RestServices rs = new RestServices();
                user = App.getUser(getApplicationContext());
//
                resultServer = rs.vefify_face(param,user);


            } catch (Exception ex) {
                ex.printStackTrace();
                user.setId(null);
            }
        }
    };

    public void goToTransacation(){
        transactionFragment = new TransactionFragment();
        setFragment(transactionFragment);
    }

    private void closeDrawer(){
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    private int fetchColor(@ColorRes int color) {
        return ContextCompat.getColor(this, color);
    }

    private void initBottomNav(){

        bottomBar.setAccentColor(fetchColor(R.color.colorAction));


        AHBottomNavigationItem item1 =
                new AHBottomNavigationItem("",
                        R.drawable.timeline);

        AHBottomNavigationItem item2 =
                new AHBottomNavigationItem("",
                        R.drawable.credit_card);

        AHBottomNavigationItem item3 =
                new AHBottomNavigationItem("",
                        R.drawable.transfer);



        AHBottomNavigationItem item4 =
                new AHBottomNavigationItem("",
                        R.drawable.payment);
        AHBottomNavigationItem item5 =
                new AHBottomNavigationItem("",
                        R.drawable.ic_recarga);


        bottomBar.addItem(item1);
        bottomBar.addItem(item2);
        bottomBar.addItem(item3);
        bottomBar.addItem(item4);
        bottomBar.addItem(item5);

        bottomBar.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {

            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
//                fragment.updateColor(Color.parseColor(colors[position]));
                if (position == 0) {
                    timeLineFragment = new TimeLineFragment();
                    setFragment(timeLineFragment);
                } else if (position == 1){
                    cardsFragment = new CardsFragment();
                    cardsFragment.setMainActivity(MainActivity.this);
                    setFragment(cardsFragment);
                }else if(position == 2) {
                    transferFragment = new TransferFragment();
                    transferFragment.setMainActivity(MainActivity.this);
                    setFragment(transferFragment);
                }else if(position == 3) {
                    Intent myIntent = new Intent(MainActivity.this, PagamentoBoletoActivity.class);
                    startActivity(myIntent);
                }else if(position == 4) {
                    Intent myIntent = new Intent(MainActivity.this, RecargaTelefoneActivity.class);
                    startActivity(myIntent);
                }



                return true;
            }
        });
    }

    private void initListeners(){

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle(getApplicationContext().getResources().getString(R.string.atencao_txt).toUpperCase());
                builder.setMessage(getApplicationContext().getResources().getString(R.string.deseja_sair_txt));

                builder.setPositiveButton(getApplicationContext().getResources().getString(R.string.sim_txt).toUpperCase(), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        alerta.dismiss();
                        App.logout(getApplicationContext());
                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }
                });
                builder.setNegativeButton(getApplicationContext().getResources().getString(R.string.nao_txt).toUpperCase(), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        alerta.dismiss();
                    }
                });
                alerta = builder.create();
                alerta.show();
            }
        });

        btnHistorico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomBar.setCurrentItem(0);
            }
        });
        btnEntradas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                entradaFragment = new EntradaFragment();
                entradaFragment.setMainActivity(MainActivity.this);
                setFragment(entradaFragment);
            }
        });
        btnPontoRecarga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pontosRecargaFragment = new PontosRecargaFragment();
                pontosRecargaFragment.setMainActivity(MainActivity.this);
                setFragment(pontosRecargaFragment);
            }
        });

        btnCards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomBar.setCurrentItem(1);
            }
        });
        btnTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomBar.setCurrentItem(2);
            }
        });
        btnPayBoleto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomBar.setCurrentItem(3);
            }
        });
        btnDocumentacao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, SolicitacaoAnaliseDocumentoActivity.class);
                startActivity(myIntent);
            }
        });
//        btnVendaDireta.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent myIntent = new Intent(MainActivity.this, VendaDiretaActivity.class);
//                startActivity(myIntent);
//            }
//        });
        btnRecargaTelefone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, RecargaTelefoneActivity.class);
                startActivity(myIntent);
            }
        });

        btnSolicitacoes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, SolicitacaoCartaoActivity.class);
                startActivity(myIntent);
            }
        });

        btnDepositoBoleto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, DepositoActivity.class);
                startActivity(myIntent);
            }
        });


    }

    public void goToBeneficts(View v) {
        BeneficioItem beneficioItem = new ListaBeneficioModal(getApplicationContext()).getBeneficioByTag(v.getTag().toString());
        Utils.showBeneficio(beneficioItem, MainActivity.this,MainActivity.this);
        //Intent i2=new Intent(Intent.ACTION_VIEW, Uri.parse("https://saudecolorida3d.azurewebsites.net/Account/Login?ReturnUrl=%2F"));
        //startActivity(i2);

    }
    public void goToCredit(View v) {
        Toast.makeText(getApplicationContext(), "Essa opção estará disponível em breve", Toast.LENGTH_SHORT).show();

    }
    public void talkDrRey(View v) {
        Utils.showSendMensagemDoctor(MainActivity.this);

    }


}

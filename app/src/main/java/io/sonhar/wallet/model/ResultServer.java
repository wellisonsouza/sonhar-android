package io.sonhar.wallet.model;

/**
 * Created by PedroPontes1 on 27/09/17.
 */

public class ResultServer {

    private Boolean success;
    private String message;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

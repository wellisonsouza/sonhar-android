package io.sonhar.wallet.app;

import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import io.sonhar.wallet.R;
import io.sonhar.wallet.model.ResponseServer;
import io.sonhar.wallet.model.ResultServer;
import io.sonhar.wallet.util.Action;
import io.sonhar.wallet.util.ActionTask;
import io.sonhar.wallet.util.ErrorHelper;
import io.sonhar.wallet.util.MaskEdit;
import io.sonhar.wallet.util.SessionHelper;
import io.sonhar.wallet.ws.RestServices;

public class AdicionarCartaoActivity extends AppCompatActivity {

    EditText edtNumCard, edtValidade, edtCvv, edtNomeImpresso;
    TextView lblNumCard, lblHolder;
    Button btnCadastrar;
    String num_cartao, cvv, validade, nomeImpresso;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adicionar_cartao);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        edtNumCard = findViewById(R.id.edtNumCard);
        edtValidade = findViewById(R.id.edtValidade);
        edtValidade.addTextChangedListener(MaskEdit.mask(edtValidade, MaskEdit.EXPIRATION_DATE2));

        edtCvv = findViewById(R.id.edtCvv);
        edtNomeImpresso = findViewById(R.id.edtNomeImpresso);
        lblHolder = findViewById(R.id.lblHolder);
        edtNomeImpresso.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                lblHolder.setText(edtNomeImpresso.getText().toString());
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        lblNumCard = (TextView) findViewById(R.id.lblNumCard);
        edtNumCard.addTextChangedListener(new AdicionarCartaoActivity.CreditCardNumberFormattingTextWatcher());
        btnCadastrar = (Button) findViewById(R.id.btnCadastrar);

        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                num_cartao = edtNumCard.getText().toString().replaceAll("\\s+","");
                validade = edtValidade.getText().toString();
                cvv = edtCvv.getText().toString();
                nomeImpresso = edtNomeImpresso.getText().toString();
                ActionTask actionCardsTask = new ActionTask(AdicionarCartaoActivity.this, salvarCartao, getResources().getString(R.string.aguarde_txt));
                actionCardsTask.execute();
            }
        });
    }

    Action salvarCartao = new Action() {
        ResultServer result = new ResultServer();
        @Override
        public void onPostExecute() {
            // TODO Auto-generated method stub
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            try {
                if(responseServer.hasError()){
                    new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                    SessionHelper.getInstance().setResponseServer(null);
                }else if(result.getSuccess()) {
                    Toast.makeText(getApplicationContext(),result.getMessage(),Toast.LENGTH_SHORT).show();
                    finish();
                }else
                    Toast.makeText(getApplicationContext(),result.getMessage(),Toast.LENGTH_SHORT).show();

            }catch(Exception ex){
                ex.printStackTrace();
            }

        }

        @Override
        public void run() {
            RestServices rs = new RestServices(mCtx);
            result = rs.salvarCartao(num_cartao, validade, cvv, nomeImpresso);
        }

    };

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return (super.onOptionsItemSelected(menuItem));
    }

    class CreditCardNumberFormattingTextWatcher implements TextWatcher {

        private boolean lock;

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            lblNumCard.setText(s.toString());
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (lock || s.length() > 16) {
                return;
            }
            lock = true;
            for (int i = 4; i < s.length(); i += 5) {
                if (s.toString().charAt(i) != ' ') {
                    s.insert(i, " ");
                }

            }
            lock = false;


        }
    }
}

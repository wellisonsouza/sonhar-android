package io.sonhar.wallet.util;

import android.content.Context;
import android.os.AsyncTask;

public class LoadTask extends AsyncTask<Void, Void, Boolean> {
	
	private final Action action;
	public final Context context;

	public LoadTask(Context context, Action action) {
		this.context = context;
		this.action = action;
	}
	public LoadTask(Action action) {
		this.action = action;
		context = null;
	}

	@Override
	protected void onPreExecute() {
		action.onPreExecute();
		super.onPreExecute();
	}
	
	@Override
	protected void onPostExecute(Boolean result) {
		action.onPostExecute();
		super.onPostExecute(result);
	}
	
	@Override
	protected Boolean doInBackground(Void... params) {
		try{
			action.run();
		} catch (Throwable e){
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	protected void onCancelled() {
		super.onCancelled();
	}
}

package io.sonhar.wallet.model;

/**
 * Created by wellison on 25/07/2018.
 */

public class Recarga {
    private String card_number, codigo_operadora, valor_recarga, ddd, telefone, data_validade, cvv, senha;

    public String getCard_number() {
        return card_number;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }

    public String getCodigo_operadora() {
        return codigo_operadora;
    }

    public void setCodigo_operadora(String codigo_operadora) {
        this.codigo_operadora = codigo_operadora;
    }

    public String getValor_recarga() {
        return valor_recarga;
    }

    public void setValor_recarga(String valor_recarga) {
        this.valor_recarga = valor_recarga;
    }

    public String getDdd() {
        return ddd;
    }

    public void setDdd(String ddd) {
        this.ddd = ddd;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getData_validade() {
        return data_validade;
    }

    public void setData_validade(String data_validade) {
        this.data_validade = data_validade;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}

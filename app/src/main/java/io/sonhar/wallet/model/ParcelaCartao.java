package io.sonhar.wallet.model;

/**
 * Created by wellison on 25/07/2018.
 */

public class ParcelaCartao {
    private int quantity;
    private double installmentAmount, totalAmount;
    private boolean interestFree;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getInstallmentAmount() {
        return installmentAmount;
    }

    public void setInstallmentAmount(double installmentAmount) {
        this.installmentAmount = installmentAmount;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public boolean isInterestFree() {
        return interestFree;
    }

    public void setInterestFree(boolean interestFree) {
        this.interestFree = interestFree;
    }

    public String getParcelaFormatada(){
        return quantity+"X de R$"+installmentAmount;
    }
}

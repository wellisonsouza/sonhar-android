package io.sonhar.wallet.model;

import java.util.Date;

/**
 * Created by wellison on 02/08/2018.
 */

public class EntradaParcela {
    private int id, numero, valor_bruto_centavos, valor_antecipacao_centavos;
    private String entrada_parc_status;
    private Date data_disponivel;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getValor_bruto_centavos() {
        return valor_bruto_centavos;
    }

    public void setValor_bruto_centavos(int valor_bruto_centavos) {
        this.valor_bruto_centavos = valor_bruto_centavos;
    }

    public String getEntrada_parc_status() {
        return entrada_parc_status;
    }

    public void setEntrada_parc_status(String entrada_parc_status) {
        this.entrada_parc_status = entrada_parc_status;
    }

    public Date getData_disponivel() {
        return data_disponivel;
    }

    public void setData_disponivel(Date data_disponivel) {
        this.data_disponivel = data_disponivel;
    }

    public int getValor_antecipacao_centavos() {
        return valor_antecipacao_centavos;
    }

    public void setValor_antecipacao_centavos(int valor_antecipacao_centavos) {
        this.valor_antecipacao_centavos = valor_antecipacao_centavos;
    }

    public Double getValorEmReais(){
        return (double) valor_bruto_centavos/100;
    }

}

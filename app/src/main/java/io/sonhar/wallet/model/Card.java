package io.sonhar.wallet.model;

/**
 * Created by PedroPontes1 on 17/06/2018.
 */

public class Card {
    private Long id;
    private String numero_conta_corrente;
    private String digito_cartao;
    private String contrato_id;
    private String saldo_cartao;
    private String nome_embossing;
    private String numero_cartao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero_conta_corrente() {
        return numero_conta_corrente;
    }

    public void setNumero_conta_corrente(String numero_conta_corrente) {
        this.numero_conta_corrente = numero_conta_corrente;
    }

    public String getDigito_cartao() {
        return digito_cartao;
    }

    public void setDigito_cartao(String digito_cartao) {
        this.digito_cartao = digito_cartao;
    }

    public String getContrato_id() {
        return contrato_id;
    }

    public void setContrato_id(String contrato_id) {
        this.contrato_id = contrato_id;
    }

    public String getNome_embossing() {
        return nome_embossing;
    }

    public void setNome_embossing(String nome_embossing) {
        this.nome_embossing = nome_embossing;
    }

    public String getNumero_cartao() {
        return numero_cartao;
    }

    public void setNumero_cartao(String numero_cartao) {
        this.numero_cartao = numero_cartao;
    }

    public String getSaldo_cartao() {
        return saldo_cartao;
    }

    public void setSaldo_cartao(String saldo_cartao) {
        this.saldo_cartao = saldo_cartao;
    }
}

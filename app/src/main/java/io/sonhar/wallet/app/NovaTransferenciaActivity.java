package io.sonhar.wallet.app;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.sonhar.wallet.R;
import io.sonhar.wallet.model.Banco;
import io.sonhar.wallet.model.ResponseServer;
import io.sonhar.wallet.model.Saida;
import io.sonhar.wallet.model.TipoTransferencia;
import io.sonhar.wallet.util.Action;
import io.sonhar.wallet.util.ActionTask;
import io.sonhar.wallet.util.ErrorHelper;
import io.sonhar.wallet.util.SessionHelper;
import io.sonhar.wallet.ws.RestServices;

public class NovaTransferenciaActivity extends AppCompatActivity {

    MaterialSpinner spnTipoSaida, spnBanco, spnTipoPessoa, spnTipoContaBancaria;
    private String[] arrayBancos;
    ArrayList<Banco> bancos;
    Button btnCadastrarWallet;
    Saida saida;
    EditText edtWalletContaFavorecido, edtWalletNomeFavorecido;
    @BindView(R.id.bottom_sheet_conta_bancaria)
    LinearLayout layoutBottomSheetContaBancaria;
    @BindView(R.id.bottom_sheet_conta_wallet)
    LinearLayout layoutBottomSheetContaWallet;
    @BindView(R.id.bottom_sheet_cartao)
    LinearLayout layoutBottomSheetCartao;


    BottomSheetBehavior sheetBehaviorContaBancaria;
    BottomSheetBehavior sheetBehaviorContaWallet;
    BottomSheetBehavior sheetBehaviorCartao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nova_transferencia);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        spnTipoSaida = findViewById(R.id.spnTipoSaida);
        spnBanco = findViewById(R.id.spnBanco);
        spnTipoPessoa = findViewById(R.id.spnTipoPessoa);
        spnTipoContaBancaria = findViewById(R.id.spnTipoContaBancaria);
        btnCadastrarWallet = findViewById(R.id.btnCadastrarWallet);
        edtWalletNomeFavorecido = findViewById(R.id.edtWalletNomeFavorecido);
        edtWalletContaFavorecido = findViewById(R.id.edtWalletContaFavorecido);

        btnCadastrarWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saida = new Saida();
                saida.setTipo_pagamento("transferencia");
                saida.setConta_favorecido(edtWalletContaFavorecido.getText().toString());
                saida.setNome_favorecido(edtWalletNomeFavorecido.getText().toString());
                saida.setData_agendamento(new Date());
                saida.setValor(1.0);
                ActionTask actionSalvarTransferencia = new ActionTask(NovaTransferenciaActivity.this, salvarTransferencia, "Aguarde...");
                actionSalvarTransferencia.execute();


            }
        });

        ArrayAdapter spinnerArrayAdapter = new ArrayAdapter(this,
                android.R.layout.simple_spinner_item, new TipoTransferencia[] {
                new TipoTransferencia( "Selecione o tipo de transferência","" ),
                new TipoTransferencia( "Conta Bancária","transferencia_conta_bancaria" ),
                new TipoTransferencia( "Conta Vivi Wallet","transferencia_conta_interna" ),
                new TipoTransferencia( "Transferência para Cartão","transferencia_cartao" )
        });

        spnTipoPessoa.setItems(new String[]{"Pessoa Física", "Pessoa Jurídica"});
        spnTipoContaBancaria.setItems(new String[]{"Conta Corrente", "Poupança"});
        spnTipoSaida.setAdapter(spinnerArrayAdapter);
        ButterKnife.bind(this);

        sheetBehaviorContaBancaria = BottomSheetBehavior.from(layoutBottomSheetContaBancaria);
        sheetBehaviorContaBancaria.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        sheetBehaviorContaWallet = BottomSheetBehavior.from(layoutBottomSheetContaWallet);
        sheetBehaviorContaWallet.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        sheetBehaviorCartao = BottomSheetBehavior.from(layoutBottomSheetCartao);
        sheetBehaviorCartao.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });


        spnTipoSaida.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                if (position == 1){
                    sheetBehaviorContaWallet.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    sheetBehaviorCartao.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    sheetBehaviorContaBancaria.setState(BottomSheetBehavior.STATE_EXPANDED);
                }else if (position == 2){
                    sheetBehaviorContaWallet.setState(BottomSheetBehavior.STATE_EXPANDED);
                    sheetBehaviorCartao.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    sheetBehaviorContaBancaria.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }else if (position == 3){
                    sheetBehaviorContaWallet.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    sheetBehaviorCartao.setState(BottomSheetBehavior.STATE_EXPANDED);
                    sheetBehaviorContaBancaria.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }else{
                    sheetBehaviorContaBancaria.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    sheetBehaviorContaWallet.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    sheetBehaviorCartao.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
            }

        });

        init();

    }

    public void init(){
        ActionTask actionLoadBancos = new ActionTask(NovaTransferenciaActivity.this, getBancos, "Aguarde...");
        actionLoadBancos.execute();

    }

    Action getBancos = new Action() {
        @Override
        public void onPostExecute() {
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            // TODO Auto-generated method stub
            try {
                if(responseServer.hasError()){
                    new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                    SessionHelper.getInstance().setResponseServer(null);
                }
                arrayBancos = new String[bancos.size()];
                for(int i = 0; i<bancos.size(); i++){
                    arrayBancos[i] = bancos.get(i).getNome();
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            spnBanco.setItems(arrayBancos);
        }

        @Override
        public void run() {
            RestServices rs = new RestServices(mCtx);
            bancos = rs.getBancos();
        }

    };

    Action salvarTransferencia = new Action() {

        @Override
        public void onPostExecute() {
            if (saida.getId() > 0){
                Toast.makeText(getApplicationContext(), "Parabéns", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(getApplicationContext(), saida.getError_message(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void run() {
            try {
                RestServices rs = new RestServices(mCtx);
                saida = rs.salvarSaida(saida);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return (super.onOptionsItemSelected(menuItem));
    }
}

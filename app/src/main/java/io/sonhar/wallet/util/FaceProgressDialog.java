package io.sonhar.wallet.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.widget.TextView;

import io.sonhar.wallet.R;


public class FaceProgressDialog extends ProgressDialog {

    TextView lblMsg;
    private String mensagem="";
	public FaceProgressDialog(Context context, String msg) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_progress_face);




	}
	public static ProgressDialog getProgress(Context context, String msg) {
		FaceProgressDialog dialog = new FaceProgressDialog(context,msg);
		dialog.setIndeterminate(true);
		dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		return dialog;
	}
}

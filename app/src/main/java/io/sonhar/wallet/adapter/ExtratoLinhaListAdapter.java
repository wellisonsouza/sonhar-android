package io.sonhar.wallet.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import io.sonhar.wallet.R;
import io.sonhar.wallet.model.ExtratoLinha;

/**
 * Created by PedroPontes1 on 17/06/2018.
 */

public class ExtratoLinhaListAdapter extends ArrayAdapter<ExtratoLinha> {
    private List<ExtratoLinha> linhas;
    private Context mCtx;

    public
    ExtratoLinhaListAdapter(Context ctx, List<ExtratoLinha> linhas) {
        super(ctx, R.layout.row_extrato_linha);
        this.mCtx = ctx;
        this.linhas = linhas;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        view = LayoutInflater.from(mCtx).inflate(R.layout.row_extrato_linha, parent, false);
        //view.setLayoutParams(new Carousel.LayoutParams(70, 70));

        ViewHolder holder = new ViewHolder();
        holder.lblDescricao = (TextView) view.findViewById(R.id.lblDescricao);
        holder.lblData = (TextView) view.findViewById(R.id.lblData);
        holder.lblValor = (TextView) view.findViewById(R.id.lblValor);
        holder.img = (ImageView) view.findViewById(R.id.img);
        view.setTag(holder);
        final ExtratoLinha linha = linhas.get(position);


        holder.lblDescricao.setText(linha.getDescricao());
        holder.lblData.setText(linha.getData());
        holder.lblValor.setText( linha.getValor());

        if (linha.getTipo() == 0){
            holder.img.setImageResource(R.drawable.arrow_up);
            holder.lblValor.setTextColor(Color.parseColor("#000000"));
            holder.lblDescricao.setTextColor(Color.parseColor("#000000"));
            holder.lblData.setTextColor(Color.parseColor("#000000"));
        }else{
            holder.img.setImageResource(R.drawable.arrow_down);
            holder.lblValor.setTextColor(Color.parseColor("#000000"));
            holder.lblDescricao.setTextColor(Color.parseColor("#000000"));
            holder.lblData.setTextColor(Color.parseColor("#000000"));
        }

        return view;
    }


    private class ViewHolder {
        TextView lblDescricao;
        TextView lblData;
        TextView lblValor;
        ImageView img;
    }

    @Override
    public ExtratoLinha getItem(int position) {
        return linhas.get(position);
    }

    @Override
    public int getCount() {
        return linhas.size();
    }

}


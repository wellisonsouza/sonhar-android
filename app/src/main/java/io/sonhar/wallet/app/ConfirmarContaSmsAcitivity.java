package io.sonhar.wallet.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import io.sonhar.wallet.R;
import io.sonhar.wallet.model.ResponseServer;
import io.sonhar.wallet.model.ResultServer;
import io.sonhar.wallet.util.Action;
import io.sonhar.wallet.util.ActionTask;
import io.sonhar.wallet.util.ErrorHelper;
import io.sonhar.wallet.util.SessionHelper;
import io.sonhar.wallet.ws.RestServices;

public class ConfirmarContaSmsAcitivity extends AppCompatActivity {

    EditText edtNum1, edtNum2, edtNum3, edtNum4, edtNum5, edtNum6;
    TextView lblNumTelefone;
    String telefone;
    Button btnOk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmar_conta_sms_acitivity);
        telefone = getIntent().getStringExtra("telefone");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        edtNum1 = findViewById(R.id.edtNum1);
        edtNum2 = findViewById(R.id.edtNum2);
        edtNum3 = findViewById(R.id.edtNum3);
        edtNum4 = findViewById(R.id.edtNum4);
        edtNum5 = findViewById(R.id.edtNum5);
        edtNum6 = findViewById(R.id.edtNum6);
        lblNumTelefone = findViewById(R.id.lblNumTelefone);
        lblNumTelefone.setText(telefone);

        edtNum1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(count == 1){
                    edtNum2.requestFocus();
                }
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        edtNum2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(count == 1){
                    edtNum3.requestFocus();
                }
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        edtNum3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(count == 1){
                    edtNum4.requestFocus();
                }
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        edtNum4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(count == 1){
                    edtNum5.requestFocus();
                }
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        edtNum5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(count == 1){
                    edtNum6.requestFocus();
                }
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        btnOk = findViewById(R.id.btnOk);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String codigo = (edtNum1.getText().toString()+edtNum2.getText().toString()+edtNum3.getText().toString()+edtNum4.getText().toString()+edtNum5.getText().toString()+edtNum6.getText().toString());
                if(codigo.length() == 6){
                    ActionTask action = new ActionTask(ConfirmarContaSmsAcitivity.this, verificarCodigo, getString(R.string.aguarde_txt));
                    verificarCodigo.param = codigo;
                    action.execute();
                }else{
                    Toast.makeText(ConfirmarContaSmsAcitivity.this, "Preencha todos os campos", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    Action verificarCodigo = new Action() {
        ResultServer resultServer;
        @Override
        public void onPostExecute() {
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            if(responseServer.hasError() && responseServer.getCode() != 422){
                new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                SessionHelper.getInstance().setResponseServer(null);
            }else if (resultServer.getSuccess()){
                Toast.makeText(ConfirmarContaSmsAcitivity.this, "Conta confirmada com sucesso", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }else{
                Toast.makeText(getApplicationContext(), resultServer.getMessage(), Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void run() {
            try {
                RestServices rs = new RestServices(mCtx);
                resultServer = rs.verificarCodigoEhAtivarConta(param, telefone);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return (super.onOptionsItemSelected(menuItem));
    }
}

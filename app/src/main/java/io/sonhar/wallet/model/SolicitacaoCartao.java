package io.sonhar.wallet.model;

import java.util.Date;

/**
 * Created by wellison on 03/08/2018.
 */

public class SolicitacaoCartao {
    private int id;
    private String status, url_boleto, cod_boleto;
    private Date created_at;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUrl_boleto() {
        return url_boleto;
    }

    public void setUrl_boleto(String url_boleto) {
        this.url_boleto = url_boleto;
    }

    public String getCod_boleto() {
        return cod_boleto;
    }

    public void setCod_boleto(String cod_boleto) {
        this.cod_boleto = cod_boleto;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }
}

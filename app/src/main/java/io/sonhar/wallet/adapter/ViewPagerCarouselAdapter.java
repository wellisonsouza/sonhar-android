package io.sonhar.wallet.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

import io.sonhar.wallet.fragment.ViewPagerCarouselFragment;
import io.sonhar.wallet.model.Card;

/**
 * Created by PedroPontes1 on 17/06/2018.
 */

public class ViewPagerCarouselAdapter extends FragmentStatePagerAdapter {
    private List<Card> cards;

    public ViewPagerCarouselAdapter(FragmentManager fm, List<Card> cards) {
        super(fm);
        this.cards = cards;
    }

    @Override
    public Fragment getItem(int position) {
        Card card = cards.get(position);

        Bundle bundle = new Bundle();
        bundle.putString("num_card", card.getNumero_cartao());
        bundle.putString("holder", card.getNome_embossing());
        ViewPagerCarouselFragment frag = new ViewPagerCarouselFragment();
        frag.setArguments(bundle);

        return frag;
    }

    @Override
    public int getCount() {
        return cards.size();
    }

}


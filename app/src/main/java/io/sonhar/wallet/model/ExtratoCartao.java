package io.sonhar.wallet.model;

import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by wellison on 25/07/2018.
 */

public class ExtratoCartao {
    private String Descricao, TpTransacao, Estabelecimento;
    private double ValorEmReais;
    private String DtTransacao;

    public String getDescricao() {
        return Descricao;
    }

    public void setDescricao(String descricao) {
        Descricao = descricao;
    }

    public String getTpTransacao() {
        return TpTransacao;
    }

    public void setTpTransacao(String tpTransacao) {
        TpTransacao = tpTransacao;
    }

    public String getEstabelecimento() {
        return Estabelecimento;
    }

    public void setEstabelecimento(String estabelecimento) {
        Estabelecimento = estabelecimento;
    }

    public double getValorEmReais() {
        return ValorEmReais;
    }

    public void setValorEmReais(double valorEmReais) {
        ValorEmReais = valorEmReais;
    }

    public Date getDataTransacao(){
        String data = this.DtTransacao.substring(6, 19);
        Date date = new Date(Long.parseLong(data));
        return date;
    }


}

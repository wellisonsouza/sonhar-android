package io.sonhar.wallet.model;

import java.util.Date;

/**
 * Created by wellison on 11/07/2018.
 */

public class Extrato {
    private Date created_at;
    private String descricao;
    private int valor_centavos;
    private String tipo;

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getValorCentavos() {
        return valor_centavos;
    }

    public void setValorCentavos(int valor) {
        this.valor_centavos = valor;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Double getValorEmReais(){
        return (double) valor_centavos/100;
    }
}

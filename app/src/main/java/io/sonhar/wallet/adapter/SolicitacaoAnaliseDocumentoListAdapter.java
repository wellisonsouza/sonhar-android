package io.sonhar.wallet.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import io.sonhar.wallet.R;
import io.sonhar.wallet.model.SolicitacaoAnaliseDocumento;
import io.sonhar.wallet.model.SolicitacaoCartao;

/**
 * Created by wellison on 11/07/2018.
 */

public class SolicitacaoAnaliseDocumentoListAdapter extends ArrayAdapter<SolicitacaoAnaliseDocumento> {
    private List<SolicitacaoAnaliseDocumento> solicitacaoAnaliseDocumentos;
    private Context mCtx;

    public SolicitacaoAnaliseDocumentoListAdapter(Context ctx, List<SolicitacaoAnaliseDocumento> solicitacaoAnaliseDocumentos) {
        super(ctx, R.layout.row_cartao);
        this.mCtx = ctx;
        this.solicitacaoAnaliseDocumentos = solicitacaoAnaliseDocumentos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        Locale ptBr = new Locale("pt", "BR");
        NumberFormat formatter = NumberFormat.getCurrencyInstance(ptBr);
        final SolicitacaoAnaliseDocumento solicitacaoAnaliseDocumento = solicitacaoAnaliseDocumentos.get(position);
        view = LayoutInflater.from(mCtx).inflate(R.layout.row_solicitacao_analise_documento, parent, false);

        TextView lblStatus = (TextView) view.findViewById(R.id.lblStatus);
        TextView lblData = (TextView) view.findViewById(R.id.lblData);

        lblStatus.setText(solicitacaoAnaliseDocumento.getStatus().replace("_"," ").toUpperCase());
        lblData.setText(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(solicitacaoAnaliseDocumento.getCreated_at()));

        return view;
    }

    @Override
    public SolicitacaoAnaliseDocumento getItem(int position) {
        return solicitacaoAnaliseDocumentos.get(position);
    }

    @Override
    public int getCount() {
        return solicitacaoAnaliseDocumentos.size();
    }

}

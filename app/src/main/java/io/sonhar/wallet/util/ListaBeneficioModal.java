package io.sonhar.wallet.util;

import android.content.Context;

import java.util.ArrayList;

import io.sonhar.wallet.R;

/**
 * Created by wellison on 26/07/2018.
 */

public class ListaBeneficioModal {
    private ArrayList<BeneficioItem> listaBeneficios = new ArrayList<BeneficioItem>();

    public ListaBeneficioModal(Context mCtx) {
        listaBeneficios.add(new BeneficioItem("consulta",mCtx.getResources().getString(R.string.doc_appointments_txt), mCtx.getResources().getString(R.string.consultas_desc),R.drawable.consulta, "https://saudecolorida3d.azurewebsites.net/Account/Login?ReturnUrl=%2F"));
        listaBeneficios.add(new BeneficioItem("exame",mCtx.getResources().getString(R.string.medical_exam_txt), mCtx.getResources().getString(R.string.exames_desc),R.drawable.exames, "https://saudecolorida3d.azurewebsites.net/Account/Login?ReturnUrl=%2F"));
        listaBeneficios.add(new BeneficioItem("medicamento",mCtx.getResources().getString(R.string.medicines_txt), mCtx.getResources().getString(R.string.medicamentos_desc),R.drawable.medicamentos, "https://saudecolorida3d.azurewebsites.net/Account/Login?ReturnUrl=%2F"));
        listaBeneficios.add(new BeneficioItem("auxilio_alimentacao",mCtx.getResources().getString(R.string.food_aid_txt), mCtx.getResources().getString(R.string.auxilio_alimentacao_desc),R.drawable.auxilio_alimentacao, "https://saudecolorida3d.azurewebsites.net/Account/Login?ReturnUrl=%2F"));
        listaBeneficios.add(new BeneficioItem("medico_24h",mCtx.getResources().getString(R.string.family_doctor_txt), mCtx.getResources().getString(R.string.medico_24h_desc),R.drawable.medico_familia, "https://saudecolorida3d.azurewebsites.net/Account/Login?ReturnUrl=%2F"));
        listaBeneficios.add(new BeneficioItem("nutricionista",mCtx.getResources().getString(R.string.nutricionist_txt), mCtx.getResources().getString(R.string.nutricionista_desc),R.drawable.nutricionista, "https://saudecolorida3d.azurewebsites.net/Account/Login?ReturnUrl=%2F"));
        listaBeneficios.add(new BeneficioItem("despesa_hospitalar",mCtx.getResources().getString(R.string.hospital_txt), mCtx.getResources().getString(R.string.despesas_hospitalares_desc),R.drawable.despesas_hospitalares, "https://saudecolorida3d.azurewebsites.net/Account/Login?ReturnUrl=%2F"));
        listaBeneficios.add(new BeneficioItem("coach",mCtx.getResources().getString(R.string.care_txt), mCtx.getResources().getString(R.string.coach_desc),R.drawable.couch_bem_estar, "https://saudecolorida3d.azurewebsites.net/Account/Login?ReturnUrl=%2F"));
        listaBeneficios.add(new BeneficioItem("invalidez_permanente",mCtx.getResources().getString(R.string.permanent_disability_txt), mCtx.getResources().getString(R.string.invalidez_permanente_desc),R.drawable.invalidez_permanente, "https://saudecolorida3d.azurewebsites.net/Account/Login?ReturnUrl=%2F"));
        listaBeneficios.add(new BeneficioItem("assistencia_funeral",mCtx.getResources().getString(R.string.funeral_txt), mCtx.getResources().getString(R.string.assistencia_funeral_desc),R.drawable.auxilio_funeral, "https://saudecolorida3d.azurewebsites.net/Account/Login?ReturnUrl=%2F"));
        listaBeneficios.add(new BeneficioItem("morte_acidental",mCtx.getResources().getString(R.string.acidente_death_txt), mCtx.getResources().getString(R.string.morte_acidental_desc),R.drawable.morte_acidental, "https://saudecolorida3d.azurewebsites.net/Account/Login?ReturnUrl=%2F"));
    }

    public BeneficioItem getBeneficioByTag(String tag){
        for(BeneficioItem beneficioItem : listaBeneficios){
            if (beneficioItem.getTag().equalsIgnoreCase(tag))
                return beneficioItem;
        }
        return null;
    }
}

package io.sonhar.wallet.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;


import io.sonhar.wallet.R;
import io.sonhar.wallet.model.EntradaParcela;

/**
 * Created by wellison on 11/07/2018.
 */

public class ParcelaItemAdapter extends ArrayAdapter<EntradaParcela> {
    private List<EntradaParcela> parcelas;
    private Context mCtx;

    public ParcelaItemAdapter(Context ctx, List<EntradaParcela> parcelas) {
        super(ctx, R.layout.row_entrada);
        this.mCtx = ctx;
        this.parcelas = parcelas;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        view = LayoutInflater.from(mCtx).inflate(R.layout.row_parcela, parent, false);
        //view.setLayoutParams(new Carousel.LayoutParams(70, 70));
        Locale ptBr = new Locale("pt", "BR");
        NumberFormat formatter = NumberFormat.getCurrencyInstance(ptBr);

        TextView lblValor = (TextView) view.findViewById(R.id.lblValor);
        TextView lblDataDisponivel = (TextView) view.findViewById(R.id.lblDataDisponivel);
        TextView lblStatusParcela = (TextView) view.findViewById(R.id.lblStatusParcela);
        TextView lblNumParcela = (TextView) view.findViewById(R.id.lblNumParcela);
        final EntradaParcela parcela = parcelas.get(position);

        lblNumParcela.setText("Nº " + String.valueOf(parcela.getNumero()));
        lblDataDisponivel.setText(new SimpleDateFormat("dd/MM/yyyy").format(parcela.getData_disponivel()));
        lblValor.setText(formatter.format(parcela.getValorEmReais()));
        lblStatusParcela.setText(parcela.getEntrada_parc_status());

        return view;
    }

    @Override
    public EntradaParcela getItem(int position) {
        return parcelas.get(position);
    }

    @Override
    public int getCount() {
        return parcelas.size();
    }
}

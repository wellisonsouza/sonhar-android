package io.sonhar.wallet.model;

/**
 * Created by wellison on 23/07/2018.
 */

public class VendaDireta {
    private String name, cpf, area_code, phone, email, street, number, district, posta_code, city, state;
    private String birthdate, description, card_number, card_brand, cvv, month, year;
    private String client_ip, conta_token, error_message, url_notificacao, transaction_code, assinatura_pagador, foto_pagador;
    private Double total_value, installments_value;
    private int installments;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getArea_code() {
        return area_code;
    }

    public void setArea_code(String area_code) {
        this.area_code = area_code;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPosta_code() {
        return posta_code;
    }

    public void setPosta_code(String posta_code) {
        this.posta_code = posta_code;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCard_number() {
        return card_number;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }

    public String getCard_brand() {
        return card_brand;
    }

    public void setCard_brand(String card_brand) {
        this.card_brand = card_brand;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Double getTotal_value() {
        return total_value;
    }

    public void setTotal_value(Double total_value) {
        this.total_value = total_value;
    }

    public int getInstallments() {
        return installments;
    }

    public void setInstallments(int installments) {
        this.installments = installments;
    }

    public Double getInstallments_value() {
        return installments_value;
    }

    public void setInstallments_value(Double installments_value) {
        this.installments_value = installments_value;
    }

    public String getTransaction_code() {
        return transaction_code;
    }

    public void setTransaction_code(String transaction_code) {
        this.transaction_code = transaction_code;
    }

    public String getClient_ip() {
        return client_ip;
    }

    public void setClient_ip(String client_ip) {
        this.client_ip = client_ip;
    }

    public String getConta_token() {
        return conta_token;
    }

    public void setConta_token(String conta_token) {
        this.conta_token = conta_token;
    }

    public String getError_message() {
        return error_message;
    }

    public void setError_message(String error_message) {
        this.error_message = error_message;
    }

    public String getUrl_notificacao() {
        return url_notificacao;
    }

    public void setUrl_notificacao(String url_notificacao) {
        this.url_notificacao = url_notificacao;
    }

    public String getAssinatura_pagador() {
        return assinatura_pagador;
    }

    public void setAssinatura_pagador(String assinatura_pagador) {
        this.assinatura_pagador = assinatura_pagador;
    }

    public String getFoto_pagador() {
        return foto_pagador;
    }

    public void setFoto_pagador(String foto_pagador) {
        this.foto_pagador = foto_pagador;
    }
}

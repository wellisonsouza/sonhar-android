package io.sonhar.wallet.app;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.sonhar.wallet.R;
import io.sonhar.wallet.model.Banco;
import io.sonhar.wallet.model.ResponseServer;
import io.sonhar.wallet.model.ResultServer;
import io.sonhar.wallet.model.Saida;
import io.sonhar.wallet.model.User;
import io.sonhar.wallet.util.Action;
import io.sonhar.wallet.util.ActionFaceTask;
import io.sonhar.wallet.util.ActionTask;
import io.sonhar.wallet.util.ErrorHelper;
import io.sonhar.wallet.util.MaskEdit;
import io.sonhar.wallet.util.MoneyTextWatcher;
import io.sonhar.wallet.util.SessionHelper;
import io.sonhar.wallet.util.Utils;
import io.sonhar.wallet.ws.RestServices;

public class TransferenciaBancariaActivity extends AppCompatActivity implements ValidationListener {
    private String[] arrayBancos;
    ArrayList<Banco> bancos;
    MaterialSpinner spnBanco, spnTipoPessoa, spnTipoContaBancaria;
    Saida saida;
    Button btnCadastrar;
    Validator validator;

    @NotEmpty(messageResId = R.string.campo_obrigatorio)
    EditText edtCpfCnpj, edtValor;
    EditText edtAgenciaDv,edtContaDv;

    @NotEmpty(messageResId = R.string.campo_obrigatorio)
    @Length(min = 4, max = 4, trim = false, message = "Formato inválido")
    EditText edtAgencia;

    @NotEmpty(messageResId = R.string.campo_obrigatorio)
    @Length(min = 4, max = 8, trim = false, message = "Formato inválido")
    EditText edtConta;

    @NotEmpty(messageResId = R.string.campo_obrigatorio)
    @Length(min = 10, max = 40, trim = false, message = "Insira o nome completo")
    EditText edtNomeFavorecido;

    TextWatcher mask;

    AlertDialog alerta;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transferencia_bancaria);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        validator = new Validator(this);
        validator.setValidationListener(this);

        spnBanco = findViewById(R.id.spnBanco);
        spnTipoPessoa = findViewById(R.id.spnTipoPessoa);
        spnTipoContaBancaria = findViewById(R.id.spnTipoContaBancaria);
        edtAgencia = findViewById(R.id.edtAgencia);
        edtAgenciaDv = findViewById(R.id.edtAgenciaDv);
        edtConta = findViewById(R.id.edtConta);
        edtContaDv = findViewById(R.id.edtContaDv);
        edtNomeFavorecido = findViewById(R.id.edtNomeFavorecido);
        edtCpfCnpj = findViewById(R.id.edtCpfCnpj);
        mask = MaskEdit.mask(edtCpfCnpj, MaskEdit.FORMAT_CPF);
        edtCpfCnpj.addTextChangedListener(mask);

        btnCadastrar = findViewById(R.id.btnCadastrar);

        edtValor = (EditText)findViewById(R.id.edtValor);
        edtValor.addTextChangedListener(new MoneyTextWatcher(edtValor));

        spnTipoPessoa.setItems(new String[]{getResources().getString(R.string.pessoa_fisica), getResources().getString(R.string.pessoa_juridica)});
        spnTipoPessoa.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                edtCpfCnpj.removeTextChangedListener(mask);
                if (position == 0)
                    mask = MaskEdit.mask(edtCpfCnpj, MaskEdit.FORMAT_CPF);
                else
                    mask = MaskEdit.mask(edtCpfCnpj, MaskEdit.FORMAT_CNPJ);
                edtCpfCnpj.addTextChangedListener(mask);
            }
        });
        String locale = getResources().getConfiguration().locale.getDisplayName();
        spnTipoContaBancaria.setItems(new String[]{getResources().getString(R.string.conta_corrente), getResources().getString(R.string.conta_poupanca)});

        init();
        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtConta.getText().toString().equalsIgnoreCase(edtAgencia.getText().toString())){
                    Toast.makeText(getApplicationContext(), "Agência e Conta não podem possuir o mesmo número", Toast.LENGTH_SHORT).show();
                }else{
                    validator.validate();
                }
            }

        });
    }

    public void init(){
        ActionTask actionLoadBancos = new ActionTask(TransferenciaBancariaActivity.this, getBancos, "Aguarde...");
        actionLoadBancos.execute();

    }

    Action getBancos = new Action() {

        @Override
        public void onPostExecute() {
            // TODO Auto-generated method stub
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            try {
                if(responseServer.hasError()){
                    new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                    SessionHelper.getInstance().setResponseServer(null);
                }
                arrayBancos = new String[bancos.size()];
                for(int i = 0; i<bancos.size(); i++){
                    arrayBancos[i] = bancos.get(i).getNome();
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            spnBanco.setItems(arrayBancos);
        }

        @Override
        public void run() {
            RestServices rs = new RestServices(mCtx);
            bancos = rs.getBancos();
        }

    };

    Action salvarTransferencia = new Action() {

        @Override
        public void onPostExecute() {
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            if(responseServer.hasError()){
                new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                SessionHelper.getInstance().setResponseServer(null);
            } else if (saida.getId() > 0){
                Utils.showAlertTransferSuccess(App.getUser(getApplicationContext()).getNome(), TransferenciaBancariaActivity.this, TransferenciaBancariaActivity.this);
            }else{
                Toast.makeText(getApplicationContext(), saida.getError_message(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void run() {
            try {
                RestServices rs = new RestServices(mCtx);
                saida = rs.salvarSaida(saida);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };



    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onValidationSucceeded() {
        saida = new Saida();
        saida.setTipo_favorecido(0);
        String valorStr = edtValor.getText().toString().replaceAll("[.]", "");
        valorStr = valorStr.replaceAll(",",".");
        if (valorStr.isEmpty() )
            saida.setValor(0.0);
        else
            saida.setValor(Double.valueOf(valorStr));
        saida.setAgencia(edtAgencia.getText().toString());
        saida.setConta_bancaria(edtConta.getText().toString());
        saida.setNome_favorecido(edtNomeFavorecido.getText().toString());
        saida.setData_agendamento(new Date());
        saida.setTipo_pagamento("transferencia");
        saida.setBanco_id(bancos.get(spnBanco.getSelectedIndex()).getId());
        saida.setCpf_cnpj(edtCpfCnpj.getText().toString());
        saida.setTipo_pessoa(spnTipoPessoa.getSelectedIndex() == 0 ? "pf" : "pj");
        saida.setTipo_conta(spnTipoContaBancaria.getSelectedIndex() == 0 ? "cc" : "cp");
        saida.setAgencia_dv(edtAgenciaDv.getText().toString());
        saida.setConta_dv(edtContaDv.getText().toString());
        if(saida.getValor()>0) {
            ActionTask actionSalvarTransferencia = new ActionTask(TransferenciaBancariaActivity.this, salvarTransferencia, "Aguarde...");
            actionSalvarTransferencia.execute();
        }else{
            Toast.makeText(getApplicationContext(),R.string.informe_o_valor_txt,Toast.LENGTH_LONG).show();
        }

    }


    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}

package io.sonhar.wallet.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import io.sonhar.wallet.R;
import io.sonhar.wallet.adapter.MenuItemAdapter;
import io.sonhar.wallet.app.App;
import io.sonhar.wallet.app.FaceValidationActivity;
import io.sonhar.wallet.app.LoginActivity;
import io.sonhar.wallet.app.MainActivity;
import io.sonhar.wallet.app.MainActivity_old;

import io.sonhar.wallet.model.User;
import io.sonhar.wallet.util.ListaMenu;
import io.sonhar.wallet.util.ListaProduto;
import io.sonhar.wallet.util.MenuItem;
import io.sonhar.wallet.util.Utils;
import me.crosswall.lib.coverflow.core.PagerContainer;

/**
 * Created by PedroPontes1 on 18/06/2018. 5496 3000 0007 6392
 */

public class HomeFragment extends Fragment implements Fragmento{

    MainActivity main;
    ListView lvMenu;
    ListaMenu listaMenu;
    PagerContainer pagerContainer;
    MenuItemAdapter menuItemAdapter;
    ArrayList<MenuItem> itensMenu;
    TextView txtUserName;
    User user;
    Animation animFadeIn;
    Animation animFadeOut;
    AlertDialog alerta;

    int selectedPositionPager;
    int widthContentPager;
    public HomeFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        selectedPositionPager = 1;
        listaMenu = new ListaMenu(getActivity().getApplicationContext());
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        user = App.getUser(getActivity());
        lvMenu = (ListView) view.findViewById(R.id.lvMenu);
        txtUserName = (TextView) view.findViewById(R.id.txtUserName);
        txtUserName.setText("Olá,\n"+user.getNome());
        itensMenu = listaMenu.getListaPeloMenu(1);
        menuItemAdapter = new MenuItemAdapter(getContext(), itensMenu);
        lvMenu.setAdapter(menuItemAdapter);
        lvMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(selectedPositionPager == 2){
                    if(i == 2){ //DOCTOR REY
                        Utils.showSendMensagemDoctor(getContext());
                    }else{
                        Intent i2=new Intent(Intent.ACTION_VIEW, Uri.parse("https://saudecolorida3d.azurewebsites.net/Account/Login?ReturnUrl=%2F"));
                        startActivity(i2);
                    }
                }else if(itensMenu.get(i).getActivity() != null) {
                    Intent myIntent = new Intent(main, itensMenu.get(i).getActivity());
                    main.startActivity(myIntent);
                }else{
                    if(itensMenu.get(i).getLogout()) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle(getActivity().getApplicationContext().getResources().getString(R.string.atencao_txt).toUpperCase());
                        builder.setMessage(getActivity().getApplicationContext().getResources().getString(R.string.deseja_sair_txt));

                        builder.setPositiveButton(getActivity().getApplicationContext().getResources().getString(R.string.sim_txt).toUpperCase(), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                                alerta.dismiss();
                                App.logout(getActivity().getApplicationContext());
                                Intent intent = new Intent(getActivity().getApplicationContext(), LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().finish();
                            }
                        });
                        builder.setNegativeButton(getActivity().getApplicationContext().getResources().getString(R.string.nao_txt).toUpperCase(), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                                alerta.dismiss();
                            }
                        });
                        alerta = builder.create();
                        alerta.show();
                    }else if (itensMenu.get(i).getFaceConf()){
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle(getActivity().getApplicationContext().getResources().getString(R.string.atencao_txt).toUpperCase());
                        builder.setMessage(getActivity().getApplicationContext().getResources().getString(R.string.reconhecimento_facial_instrucoes_txt));

                        builder.setPositiveButton(getActivity().getApplicationContext().getResources().getString(R.string.continuar_txt).toUpperCase(), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                                alerta.dismiss();
                                MainActivity_old.type_faceCamera = 0;
                                Intent pickFaceValidation = new Intent(getActivity().getApplicationContext(),FaceValidationActivity.class);
                                startActivityForResult(pickFaceValidation, 17);
                            }
                        });
                        builder.setNegativeButton(getActivity().getApplicationContext().getResources().getString(R.string.cancelar_txt).toUpperCase(), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                                alerta.dismiss();
                            }
                        });
                        alerta = builder.create();
                        alerta.show();
                    }

                }
            }
        });
        pagerContainer = (PagerContainer) view.findViewById(R.id.pager_container);

        final ViewPager pager = pagerContainer.getViewPager();
        pager.setAdapter(new MyPagerAdapter());
        pager.setClipChildren(false);
        pager.setOffscreenPageLimit(15);
        pager.setPageMargin(30);
        pager.setCurrentItem(1);

        animFadeIn  = AnimationUtils.loadAnimation(getActivity(), R.anim.fadein);
        animFadeIn.reset();
        animFadeOut  = AnimationUtils.loadAnimation(getActivity(), R.anim.fadeout);

        pager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
            @Override
            public void onPageSelected(int position) {
                selectedPositionPager = position;
                itensMenu = listaMenu.getListaPeloMenu(pager.getCurrentItem());
                menuItemAdapter = new MenuItemAdapter(getContext(), itensMenu);
                lvMenu.setAdapter(menuItemAdapter);
                animFadeIn.reset();
                lvMenu.clearAnimation();
                lvMenu.startAnimation(animFadeIn);

                Log.i("Wallet", "start animation fadein");
            }
        });


        animFadeIn.setAnimationListener(new Animation.AnimationListener(){
            @Override
            public void onAnimationStart(Animation arg0) {
            }
            @Override
            public void onAnimationRepeat(Animation arg0) {
            }
            @Override
            public void onAnimationEnd(Animation arg0) {

                lvMenu.setAlpha(Float.valueOf("1"));

            }
        });

        pager.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View view, int scrollX, int i1, int i2, int i3) {

                int realScrollX =scrollX;
                if(selectedPositionPager == 0) {
                    scrollX = scrollX + widthContentPager;

                }else if(selectedPositionPager == 2) {
                    scrollX = scrollX - widthContentPager;
                }

                if (scrollX < 0)
                    scrollX = scrollX * -1;

                float alpha = (1 - (scrollX * 0.0037f));
                lvMenu.setAlpha(alpha);

                Log.i("Wallet", "x: " + scrollX + "- real: "+realScrollX);
                Log.i("Wallet", "alpha: " + alpha);
                Log.i("Wallet", "selected: " + selectedPositionPager);
            }
        });


        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display. getSize(size);
        int width = size. x;

        widthContentPager = (width*570)/740;
        return view;
    }


    @Override
    public void reload(Context c) {

    }

    @Override
    public void setMainActivity(MainActivity main) {
        this.main = main;
    }

    private class MyPagerAdapter extends PagerAdapter {

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.item_cover,null);
            CardView cardView = (CardView) view.findViewById(R.id.cardview);
            cardView.setCardBackgroundColor(Color.TRANSPARENT);
            cardView.setCardElevation(0);
            ImageView imageView = (ImageView) view.findViewById(R.id.image_cover);
            imageView.setImageDrawable(getResources().getDrawable(ListaProduto.covers[position]));
            imageView.setBackgroundColor(Color.TRANSPARENT);
            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View)object);
        }

        @Override
        public int getCount() {
            return ListaProduto.covers.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view == object);
        }
    }

    @Override
    public String getTittle() {
        return getActivity().getApplicationContext().getResources().getString(R.string.cartoes_txt);
    }

}

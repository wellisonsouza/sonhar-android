package io.sonhar.wallet.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.llollox.androidtoggleswitch.widgets.ToggleSwitch;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


import io.sonhar.wallet.R;
import io.sonhar.wallet.adapter.EntradaItemAdapter;
import io.sonhar.wallet.app.MainActivity;
import io.sonhar.wallet.model.Entrada;
import io.sonhar.wallet.model.ResponseServer;
import io.sonhar.wallet.model.Saldo;
import io.sonhar.wallet.util.Action;
import io.sonhar.wallet.util.ActionTask;
import io.sonhar.wallet.util.ErrorHelper;
import io.sonhar.wallet.util.SessionHelper;
import io.sonhar.wallet.util.Utils;
import io.sonhar.wallet.ws.RestServices;

/**
 * Created by wellison on 02/08/2018.
 */

public class EntradaFragment extends Fragment implements Fragmento, AbsListView.OnScrollListener {
    MainActivity main;
    private boolean loading = false;
    private boolean loadedAllItens = false;
    Saldo saldo;
    private int page = 1;
    private Handler handler;
    EntradaItemAdapter adapter;
    ListView lvEntradas;
    ArrayList<Entrada> entradas;
    ArrayList<Entrada> todasEntradas;
    TextView txtTotalEntradas;
    ToggleSwitch tggDias;
    String filtro = null;
//    LinearLayout layoutEmpty;
    TextView textEmpty;

    public EntradaFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_entradas, container, false);
        todasEntradas = new ArrayList<Entrada>();
        lvEntradas = view.findViewById(R.id.lvEntradas);
        txtTotalEntradas = (TextView) view.findViewById(R.id.txtTotalEntradas);
        textEmpty = view.findViewById(R.id.textEmpty);
        lvEntradas.setOnScrollListener(this);
        lvEntradas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Entrada entrada = todasEntradas.get(i);
                Utils.showDetalheEntrada(getContext(), entrada);
            }
        });
        tggDias = view.findViewById(R.id.tggDias);
        tggDias.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                Date hoje = new Date();
                Date dtFiltro;
                if(i == 0){
                    dtFiltro = Utils.removeDays(hoje, 7);
                }else if(i == 1){
                    dtFiltro = Utils.removeDays(hoje, 15);
                }else{
                    dtFiltro = Utils.removeDays(hoje, 30);
                }
                filtro = "q[data_transacao_datelteq]="+Utils.formatDate(hoje, "yyyy-MM-dd")+"&q[data_transacao_dategteq]="+Utils.formatDate(dtFiltro, "yyyy-MM-dd");
                resetList();
                getEntradasService("Aguarde..");

            }
        });
        getEntradasService("Aguarde..");
        return view;
    }

    private void resetList(){
        page = 1;
        loading = false;
        loadedAllItens = false;
        entradas = new ArrayList<Entrada>();
        todasEntradas = new ArrayList<Entrada>();
    }

    Action getEntradas = new Action() {
        Locale ptBr = new Locale("pt", "BR");
        NumberFormat formatter = NumberFormat.getCurrencyInstance(ptBr);

        @Override
        public void onPostExecute() {
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();

            // TODO Auto-generated method stub
            try {
                if(responseServer.hasError()){
                    new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                    SessionHelper.getInstance().setResponseServer(new ResponseServer());
                }else{
                    if(entradas.size() < 10){
                        loadedAllItens = true;
                    }
                    if (page == 1){
                        todasEntradas.addAll(entradas);
                        adapter = new EntradaItemAdapter(mCtx, entradas);
                        lvEntradas.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }else{
                        todasEntradas.addAll(entradas);
                        adapter.add(entradas);
                        loading = false;
                    }

                    if(adapter.getCount() == 0)
                        textEmpty.setVisibility(View.VISIBLE);
                    else
                        textEmpty.setVisibility(View.GONE);
                    if(todasEntradas.size()>0){
                        txtTotalEntradas.setText(formatter.format(todasEntradas.get(0).getTotalPeriodo()));
                    }else{
                        txtTotalEntradas.setText(formatter.format(0.0));
                    }
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        @Override
        public void run() {
            RestServices rs = new RestServices(mCtx);
            entradas = rs.getEntradas(page, filtro);
        }

    };

    public void getEntradasService(String message){
        ActionTask actionLoadBancos = new ActionTask(main, getEntradas, message);
        actionLoadBancos.execute();
    }


    @Override
    public String getTittle() {
        return getActivity().getApplicationContext().getResources().getString(R.string.entradas_txt);
    }

    @Override
    public void reload(Context c) {

    }
    @Override
    public void setMainActivity(MainActivity main) {
        this.main = main;
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (lvEntradas.getAdapter() == null)
            return ;

        if (lvEntradas.getAdapter().getCount() == 0)
            return ;
        int l = visibleItemCount + firstVisibleItem;
        if (l >= (totalItemCount - 4) && !loading && !loadedAllItens) {
            loading = true;
            page++;
            getEntradasService(null);
        }
    }
}

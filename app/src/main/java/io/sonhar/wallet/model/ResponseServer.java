package io.sonhar.wallet.model;

/**
 * Created by wellison on 30/07/2018.
 */

public class ResponseServer {
    int code = 0;
    String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean hasError() {
        if(this.code == 200 || this.code == 201 || this.code == 202)
            return false;
        return true;
    }

}

package io.sonhar.wallet.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import io.sonhar.wallet.R;
import io.sonhar.wallet.util.MenuItem;

/**
 * Created by wellison on 11/07/2018.
 */

public class MenuItemAdapter extends ArrayAdapter<MenuItem> {
    private List<MenuItem> menuItems;
    private Context mCtx;

    public MenuItemAdapter(Context ctx, List<MenuItem> menuItems) {
        super(ctx, R.layout.row_menu_principal);
        this.mCtx = ctx;
        this.menuItems = menuItems;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final MenuItem menuItem = menuItems.get(position);
        view = LayoutInflater.from(mCtx).inflate(R.layout.row_menu_principal, parent, false);
        TextView txtMenuItem = (TextView) view.findViewById(R.id.menuItem);
        TextView menuItemDescricao = (TextView) view.findViewById(R.id.menuItemDescricao);
        ImageView imgMenuItem = (ImageView) view.findViewById(R.id.imgMenuItem);
        imgMenuItem.setImageDrawable(mCtx.getResources().getDrawable(menuItem.getImage()));
        txtMenuItem.setText(menuItem.getNome());
        menuItemDescricao.setText(menuItem.getDescricao());

        return view;
    }

    @Override
    public MenuItem getItem(int position) {
        return menuItems.get(position);
    }

    @Override
    public int getCount() {
        return menuItems.size();
    }
}

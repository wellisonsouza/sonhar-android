package io.sonhar.wallet.ws;

import android.provider.Settings;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyStore;
import java.util.List;

import io.sonhar.wallet.BuildConfig;
import io.sonhar.wallet.app.App;
import io.sonhar.wallet.app.MainActivity;
import io.sonhar.wallet.app.SplashActivity;
import io.sonhar.wallet.model.ResponseServer;
import io.sonhar.wallet.model.User;
import io.sonhar.wallet.util.Msg;
import io.sonhar.wallet.util.SessionHelper;


public class Rest {

    DefaultHttpClient httpClient;
    HttpContext localContext;
    private String ret;

    HttpParams myParams = null;
    HttpResponse response = null;
    HttpPost httpPost = null;
    HttpPut httpPut = null;
    HttpGet httpGet = null;
    String webServiceUrl;
    String android_id;
    private static String PUBLIC_TOKEN = "0e6dd541-1da9-4a65-a280-fcbd9ef17fee";

    //The serviceName should be the name of the Service you are going to be using.
    public Rest(String serviceName){
        myParams = new BasicHttpParams();

        HttpConnectionParams.setConnectionTimeout(myParams, 60 * 1000);
        HttpConnectionParams.setSoTimeout(myParams, 60 * 1000);
        httpClient = new DefaultHttpClient(myParams);
        localContext = new BasicHttpContext();
        webServiceUrl = serviceName;
        android_id = Settings.Secure.getString(App.context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public HttpClient getNewHttpClient() {
        DefaultHttpClient httpClient;
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore
                    .getDefaultType());
            trustStore.load(null, null);

            SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory
                    .getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));



            ClientConnectionManager ccm = new ThreadSafeClientConnManager(
                    params, registry);

            httpClient = new DefaultHttpClient(ccm, params);
        } catch (Exception e) {
            httpClient = new DefaultHttpClient();
        }
        return httpClient;
    }

    public String webInvoke(String methodName, String data, String contentType) throws IOException {
        ret = null;

        httpPost = new HttpPost(webServiceUrl + methodName);
        httpPost.setParams(myParams);
        response = null;

        StringEntity tmp = null;

        if (contentType != null) {
            httpPost.setHeader("Content-Type", contentType);
        } else {
            httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
        }

        try {
            tmp = new StringEntity(data);
        } catch (UnsupportedEncodingException e) {
            Log.e("HttpUtils", "UnsupportedEncodingException : "+e);
        }

        httpPost.setEntity(tmp);

        Log.d("Servico", webServiceUrl + "?" + data);

        try {
            response = httpClient.execute(httpPost,localContext);

            if (response != null) {
                ret = EntityUtils.toString(response.getEntity());
            }
        } catch (ClientProtocolException exception) {
            Log.e("ClientProtocolException", exception.getMessage());
        } catch (IOException exception){
            Log.e("IOException", exception.getMessage());
            throw exception;
        }

        return ret;
    }

    //Use this method to do a HttpGet/WebGet on the web service
    public String webGet(String url, User usrLogged) {
        int code = 0;
        httpGet = new HttpGet(url);
        Log.e("WebGetURL: ",url);

        try {
            httpGet.setHeader("User-Agent", "android");
            httpGet.setHeader("X-App-Version", String.valueOf(BuildConfig.VERSION_CODE));
            httpGet.setHeader("X-Device-Id", android_id);
            httpGet.setHeader("X-User-Email", usrLogged.getEmail());
            httpGet.setHeader("X-User-Token", usrLogged.getAuthentication_token());
            httpGet.setHeader("X-Conta-Token",PUBLIC_TOKEN);
            response = httpClient.execute(httpGet);
            code = response.getStatusLine().getStatusCode();

        } catch (Exception e) {
            //Log.e("HttpClient:", e.getMessage());
            e.printStackTrace();
        }
        ResponseServer responseServer = new ResponseServer();
        responseServer.setCode(code);
        SessionHelper.getInstance().setResponseServer(responseServer);

        // we assume that the response body contains the error message
        try {
            ret = EntityUtils.toString(response.getEntity());
        } catch (IOException e) {
            // Log.e("EntityUtils:", e.getMessage());
            e.printStackTrace();
        }

        return ret;
    }
    public String webGetAuth(String url, User usrLogged) {
        httpGet = new HttpGet(url);
        Log.e("WebGetURL: ",url);

        try {
            httpGet.setHeader("User-Agent", "android");
            httpGet.setHeader("X-App-Version", String.valueOf(BuildConfig.VERSION_CODE));
            httpGet.setHeader("X-Device-Id", android_id);
            httpGet.setHeader("X-User-Email", usrLogged.getEmail());
            httpGet.setHeader("X-User-Token", usrLogged.getAuthentication_token());
            httpGet.setHeader("X-Conta-Token",PUBLIC_TOKEN);
            response = httpClient.execute(httpGet);
        } catch (Exception e) {
            //Log.e("HttpClient:", e.getMessage());
            e.printStackTrace();
        }

        // we assume that the response body contains the error message
        try {
            ret = EntityUtils.toString(response.getEntity());
        } catch (IOException e) {
            // Log.e("EntityUtils:", e.getMessage());
            e.printStackTrace();
        }

        return ret;
    }

    //Use this method to do a HttpGet/WebGet on the web service
    public String doPost(String url, List<NameValuePair> params) {
        httpPost = new HttpPost(url);
        int code = 0;
        try {
            httpPost.setHeader("User-Agent", "android");
            httpPost.setHeader("X-App-Version", String.valueOf(BuildConfig.VERSION_CODE));
            httpPost.setHeader("X-Device-Id", android_id);
            httpPost.setHeader("X-Conta-Token",PUBLIC_TOKEN);
            httpPost.setHeader(HTTP.CONTENT_TYPE,
                    "application/x-www-form-urlencoded;charset=UTF-8");
            httpPost.setHeader("Accept", "application/json");

            // Add your data
            httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
        } catch (UnsupportedEncodingException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        Log.e("WebGetURL: ",url);

        try {
            response = httpClient.execute(httpPost);
            code = response.getStatusLine().getStatusCode();
        } catch (Exception e) {
            Log.e("Http Client:", e.getMessage());
        }
        ResponseServer responseServer = new ResponseServer();
        responseServer.setCode(code);
        SessionHelper.getInstance().setResponseServer(responseServer);
        // we assume that the response body contains the error message
        try {
            ret = EntityUtils.toString(response.getEntity());
        } catch (IOException e) {
            Log.e("EntityUtils:", e.getMessage());
        }
        return ret;
    }
    public String doPost(String url, List<NameValuePair> params, User usrLogged) {
        httpPost = new HttpPost(url);
        int code = 0;
        try {
            httpPost.setHeader("User-Agent", "android");
            httpPost.setHeader("X-App-Version", String.valueOf(BuildConfig.VERSION_CODE));
            httpPost.setHeader("X-Device-Id", android_id);
            httpPost.setHeader("X-User-Email", usrLogged.getEmail());
            httpPost.setHeader("X-User-Token", usrLogged.getAuthentication_token());
            httpPost.setHeader("X-Conta-Token",PUBLIC_TOKEN);
            httpPost.setHeader(HTTP.CONTENT_TYPE,
                    "application/x-www-form-urlencoded;charset=UTF-8");
            httpPost.setHeader("Accept", "application/json");
            // Add your data
            httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
        } catch (UnsupportedEncodingException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        Log.e("WebGetURL: ",url);

        try {
            response = httpClient.execute(httpPost);
            code = response.getStatusLine().getStatusCode();

        } catch (Exception e) {
            Log.e("Http Client:", e.getMessage());
        }
        ResponseServer responseServer = new ResponseServer();
        responseServer.setCode(code);
        SessionHelper.getInstance().setResponseServer(responseServer);
        // we assume that the response body contains the error message
        try {
            ret = EntityUtils.toString(response.getEntity());
        } catch (IOException e) {
            Log.e("EntityUtils:", e.getMessage());
        }

        return ret;
    }

    public String doPutNonAuth(String url, List<NameValuePair> params) {
        httpPut = new HttpPut(url);
        try {
            httpPut.setHeader("User-Agent", "android");
            httpPut.setHeader("X-App-Version", String.valueOf(BuildConfig.VERSION_CODE));
            httpPut.setHeader("X-Device-Id", android_id);
            httpPut.setHeader("Accept", "application/json");
            httpPut.setHeader(HTTP.CONTENT_TYPE,
                    "application/x-www-form-urlencoded;charset=UTF-8");
            httpPut.setHeader("X-Conta-Token",PUBLIC_TOKEN);
            // Add your data
            httpPut.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
        } catch (UnsupportedEncodingException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        Log.e("WebGetURL: ",url);

        try {
            response = httpClient.execute(httpPut);
        } catch (Exception e) {
            Log.e("Http Client:", e.getMessage());
        }
        // we assume that the response body contains the error message
        try {
            ret = EntityUtils.toString(response.getEntity());
        } catch (IOException e) {
            Log.e("EntityUtils:", e.getMessage());
        }
        return ret;
    }

    public String doPostUpload(String url, List<NameValuePair> params, ByteArrayBody bab) {
        httpClient = new DefaultHttpClient();
        httpPost = new HttpPost(url);
        try {

            httpPost.setHeader("User-Agent", "android");
            httpPost.setHeader("X-App-Version", String.valueOf(BuildConfig.VERSION_CODE));
            httpPost.setHeader("X-Device-Id", android_id);
            httpPost.setHeader(HTTP.CONTENT_TYPE,
                    "application/x-www-form-urlencoded;charset=UTF-8");
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("X-Conta-Token",PUBLIC_TOKEN);
            // Add your data
            MultipartEntity reqEntity = new MultipartEntity(
                    HttpMultipartMode.BROWSER_COMPATIBLE);
            reqEntity.addPart("uploaded", bab);
            reqEntity.addPart("photoCaption", new StringBody("sfsdfsdf"));
            httpPost.setEntity(reqEntity);
        } catch (UnsupportedEncodingException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        Log.e("WebGetURL: ",url);

        try {
            response = httpClient.execute(httpPost);
        } catch (Exception e) {
            Log.e("Http Client:", e.getMessage());
        }
        // we assume that the response body contains the error message
        try {
            ret = EntityUtils.toString(response.getEntity());
        } catch (IOException e) {
            Log.e("EntityUtils:", e.getMessage());
        }
        return ret;
    }

    //Use this method to do a HttpGet/WebGet on the web service
    public String doPostAuth(String url, List<NameValuePair> params, User usrLogged) {
        int code = 0;
        httpPost = new HttpPost(url);
        try {
            httpPost.setHeader("User-Agent", "android");
            httpPost.setHeader("X-App-Version", String.valueOf(BuildConfig.VERSION_CODE));
            httpPost.setHeader("X-Device-Id", android_id);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader(HTTP.CONTENT_TYPE,
                    "application/x-www-form-urlencoded;charset=UTF-8");
            httpPost.setHeader("X-User-Email", usrLogged.getEmail());
            httpPost.setHeader("X-User-Token", usrLogged.getAuthentication_token());
            httpPost.setHeader("X-Conta-Token",PUBLIC_TOKEN);
            // Add your data
            httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
        } catch (UnsupportedEncodingException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        Log.e("WebGetURL: ",url);

        try {
            response = httpClient.execute(httpPost);
            code = response.getStatusLine().getStatusCode();

        } catch (Exception e) {
            Log.e("Http Client:", e.getMessage());
        }
        ResponseServer responseServer = new ResponseServer();
        responseServer.setCode(code);
        SessionHelper.getInstance().setResponseServer(responseServer);
        // we assume that the response body contains the error message
        try {
            ret = EntityUtils.toString(response.getEntity());
        } catch (IOException e) {
            Log.e("EntityUtils:", e.getMessage());
        }

        return ret;
    }
    //Use this method to do a HttpGet/WebGet on the web service
    public String doPutAuth(String url, List<NameValuePair> params, User usrLogged) {
        httpPut = new HttpPut(url);
        try {
            httpPut.setHeader("User-Agent", "android");
            httpPut.setHeader("X-App-Version", String.valueOf(BuildConfig.VERSION_CODE));
            httpPut.setHeader("X-Device-Id", android_id);
            httpPut.setHeader("Accept", "application/json");
            httpPut.setHeader(HTTP.CONTENT_TYPE,
                    "application/x-www-form-urlencoded;charset=UTF-8");
            httpPut.setHeader("X-User-Email", usrLogged.getEmail());
            httpPut.setHeader("X-User-Token", usrLogged.getAuthentication_token());
            httpPut.setHeader("X-Conta-Token",PUBLIC_TOKEN);
            // Add your data
            httpPut.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
        } catch (UnsupportedEncodingException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        Log.e("WebGetURL: ",url);

        try {
            response = httpClient.execute(httpPut);
        } catch (Exception e) {
            Log.e("Http Client:", e.getMessage());
        }
        // we assume that the response body contains the error message
        try {
            ret = EntityUtils.toString(response.getEntity());
        } catch (IOException e) {
            Log.e("EntityUtils:", e.getMessage());
        }
        return ret;
    }

    public String doPost(String url, JSONObject json) {
        httpPost = new HttpPost(url);
        try {
            StringEntity se = new StringEntity(json.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            httpPost.setHeader("Content-type", "application/json");
            httpPost.setHeader("Accept", "application/json");

            httpPost.setEntity(se);
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        try {
            response = httpClient.execute(httpPost);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            ret = EntityUtils.toString(response.getEntity());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ret;
    }

    public String doPost(String url, String json) {
        httpPost = new HttpPost(url);
        try {
            StringEntity se = new StringEntity(json);
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            httpPost.setHeader("Content-type", "application/json");
            httpPost.setHeader("Accept", "application/json");
            httpPost.setEntity(se);
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        try {
            response = httpClient.execute(httpPost);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            ret = EntityUtils.toString(response.getEntity());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ret;
    }

    public String doPostForm(String url, UrlEncodedFormEntity formEntity) {
        httpPost = new HttpPost(url);
        httpPost.setEntity(formEntity);
        try {
            response = httpClient.execute(httpPost);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            ret = EntityUtils.toString(response.getEntity());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ret;
    }


    public InputStream getHttpStream(String urlString) throws IOException {
        InputStream in = null;
        int response = -1;

        URL url = new URL(urlString);
        URLConnection conn = url.openConnection();

        if (!(conn instanceof HttpURLConnection))
            throw new IOException("Not an HTTP connection");

        try{
            HttpURLConnection httpConn = (HttpURLConnection) conn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect();

            response = httpConn.getResponseCode();

            if (response == HttpURLConnection.HTTP_OK) {
                in = httpConn.getInputStream();
            }
        } catch (Exception e) {
            throw new IOException("Error connecting");
        } // end try-catch

        return in;
    }

    public void clearCookies() {
        //httpClient.getCookieStore().clear();
    }

    public void abort() {
        try {
            if (httpClient != null) {
                System.out.println("Abort.");
                httpPost.abort();
            }
        } catch (Exception e) {
            System.out.println("Your App Name Here" + e);
        }
    }

    public String webGetNonAuth(String url) {
        int code = 0;
        httpGet = new HttpGet(url);
        Log.e("WebGetURL: ",url);

        try {
            response = httpClient.execute(httpGet);
            code = response.getStatusLine().getStatusCode();
        } catch (Exception e) {
            //Log.e("HttpClient:", e.getMessage());
            e.printStackTrace();
        }
        ResponseServer responseServer = new ResponseServer();
        responseServer.setCode(code);
        SessionHelper.getInstance().setResponseServer(responseServer);

        // we assume that the response body contains the error message
        try {
            ret = EntityUtils.toString(response.getEntity());
        } catch (IOException e) {
            // Log.e("EntityUtils:", e.getMessage());
            e.printStackTrace();
        }

        return ret;
    }
}
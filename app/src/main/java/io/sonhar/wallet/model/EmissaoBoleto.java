package io.sonhar.wallet.model;

import java.util.Date;

/**
 * Created by wellison on 06/08/2018.
 */

public class EmissaoBoleto {
    private boolean success;
    private String message, cod_boleto, url_boleto, favorecido, status;
    private double valor;
    private Date data_transacao;


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCod_boleto() {
        return cod_boleto;
    }

    public void setCod_boleto(String cod_boleto) {
        this.cod_boleto = cod_boleto;
    }

    public String getUrl_boleto() {
        return url_boleto;
    }

    public void setUrl_boleto(String url_boleto) {
        this.url_boleto = url_boleto;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getFavorecido() {
        return favorecido;
    }

    public void setFavorecido(String favorecido) {
        this.favorecido = favorecido;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getData_transacao() {
        return data_transacao;
    }

    public void setData_transacao(Date data_transacao) {
        this.data_transacao = data_transacao;
    }
}

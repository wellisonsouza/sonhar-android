package io.sonhar.wallet.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by PedroPontes on 10/09/15.
 */
public class User {
    private Long id;
    private String nome;
    private String email;
    private String faceId;
    private String role;
    private String authentication_token;
    private int current_conta_id;
    private String conta_token;
    private String num_conta;
    private String message;
    private int status_conta;



    public User(){}
    public User(String json){
        Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();

        User user = gson.fromJson(json,User.class);

        this.id = user.getId();
        this.nome = user.getNome();
        this.email = user.getEmail();
        this.role = user.getRole();
        this.authentication_token = user.getAuthentication_token();
        this.message = user.getMessage();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getFaceId() {
        return faceId;
    }

    public void setFaceId(String faceId) {
        this.faceId = faceId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAuthentication_token() {
        return authentication_token;
    }

    public void setAuthentication_token(String authentication_token) {
        this.authentication_token = authentication_token;
    }

    public int getCurrent_conta_id() {
        return current_conta_id;
    }

    public void setCurrent_conta_id(int current_conta_id) {
        this.current_conta_id = current_conta_id;
    }

    public String getConta_token() {
        return conta_token;
    }

    public void setConta_token(String conta_token) {
        this.conta_token = conta_token;
    }

    public String toJson(){
        Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
        String json = gson.toJson(this);

        return json;
    }

    public String getNum_conta() {
        return num_conta;
    }

    public void setNum_conta(String num_conta) {
        this.num_conta = num_conta;
    }

    public int getStatus_conta() {
        return status_conta;
    }

    public void setStatus_conta(int status_conta) {
        this.status_conta = status_conta;
    }
}

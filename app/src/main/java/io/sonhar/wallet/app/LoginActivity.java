package io.sonhar.wallet.app;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import io.sonhar.wallet.R;
import io.sonhar.wallet.model.PagamentoBoleto;
import io.sonhar.wallet.model.ResponseServer;
import io.sonhar.wallet.model.ResultServer;
import io.sonhar.wallet.model.User;
import io.sonhar.wallet.util.Action;
import io.sonhar.wallet.util.ActionTask;
import io.sonhar.wallet.util.ErrorHelper;
import io.sonhar.wallet.util.SessionHelper;
import io.sonhar.wallet.util.Utils;
import io.sonhar.wallet.ws.RestServices;

public class LoginActivity extends AppCompatActivity {

    EditText edtEmail;
    EditText edtPass;
    Button btnLogin;
    Button btnRecuperarSenha;
    Dialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        btnLogin = (Button) findViewById(R.id.btnLogin);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtPass = (EditText) findViewById(R.id.edtSenha);
        btnRecuperarSenha = findViewById(R.id.btnRecuperarSenha);


        btnRecuperarSenha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog = new Dialog(LoginActivity.this);
                Utils.showRecuperarSenha(dialog, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        EditText edtEmail = dialog.findViewById(R.id.edtEmail);
                        ActionTask actionRecuperarSenha = new ActionTask(LoginActivity.this, recuperarSenhaAction, getResources().getString(R.string.aguarde_txt));
                        recuperarSenhaAction.param = edtEmail.getText().toString();
                        actionRecuperarSenha.execute();
                    }
                });
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActionTask actionLoadGames = new ActionTask(LoginActivity.this, loginAction, getResources().getString(R.string.autenticando_txt));
                actionLoadGames.execute();
//                User user = new User();
//
//                user.setNome("Pedro Pontes");
//                user.setId(new Long(002));
//                user.setEmail("pedro.pontes92@gmail.com.br");
//
//                App.saveUser(getApplicationContext(), user);
//                userLogged(user);
            }
        });

        User user=App.getUser(getApplicationContext());
        if(user!=null){
            userLogged(user);
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }


    Action loginAction = new Action() {

        User user = new User();
        @Override
        public void onPostExecute() {
            // TODO Auto-generated method stub
            Log.i("RETORNO", "OK");
            try {

                if (user.getAuthentication_token() != null && user.getStatus_conta() == 0) {
                    App.saveUser(mCtx, user);
                    userLogged(user);
                } else if(user.getStatus_conta() == 3){
                    Intent myIntent = new Intent(LoginActivity.this, ConfirmarContaActivity.class);
                    myIntent.putExtra("conta_token", user.getConta_token());
                    startActivity(myIntent);
                }else{
                    Toast.makeText(mCtx,user.getMessage(),Toast.LENGTH_LONG).show();
                    edtPass.setText("");
                }

//                    resizeListView(lvAulas);
            }catch(Exception ex){
                ex.printStackTrace();
            }

        }
        @Override
        public void run () {
            try {

                RestServices rs = new RestServices();

                user = rs.auth(edtEmail.getText().toString(), edtPass.getText().toString());


            } catch (Exception ex) {
                ex.printStackTrace();
                user.setId(null);
            }
        }
    };

    Action recuperarSenhaAction = new Action() {
        ResultServer resultServer;
        @Override
        public void onPostExecute() {
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            try {
                if(responseServer.hasError() && responseServer.getCode() != 404){
                    new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                    SessionHelper.getInstance().setResponseServer(null);
                }else if(resultServer.getSuccess()) {
                    Toast.makeText(getApplicationContext(),resultServer.getMessage(),Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }else
                    Toast.makeText(getApplicationContext(),resultServer.getMessage(),Toast.LENGTH_LONG).show();
            }catch(Exception ex){
                ex.printStackTrace();
            }

        }
        @Override
        public void run () {
            try {
                RestServices rs = new RestServices();
                resultServer = rs.recuperarSenha(param);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };


    private void userLogged(User user){
        Toast.makeText(getApplicationContext(), getResources().getString(R.string.bem_vindo_txt) + " " +user.getNome(), Toast.LENGTH_LONG).show();
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();
    }

}

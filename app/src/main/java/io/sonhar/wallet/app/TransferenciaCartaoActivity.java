package io.sonhar.wallet.app;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.Date;
import java.util.List;

import io.sonhar.wallet.R;
import io.sonhar.wallet.model.Cartao;
import io.sonhar.wallet.model.ResponseServer;
import io.sonhar.wallet.model.ResultServer;
import io.sonhar.wallet.model.Saida;
import io.sonhar.wallet.model.User;
import io.sonhar.wallet.util.Action;
import io.sonhar.wallet.util.ActionFaceTask;
import io.sonhar.wallet.util.ActionTask;
import io.sonhar.wallet.util.ErrorHelper;
import io.sonhar.wallet.util.MoneyTextWatcher;
import io.sonhar.wallet.util.SessionHelper;
import io.sonhar.wallet.util.Utils;
import io.sonhar.wallet.ws.RestServices;

public class TransferenciaCartaoActivity extends AppCompatActivity implements Validator.ValidationListener {
    Cartao cartao;
    String proxy, holder, numero_cartao ;
    Button btnCadastrar;
    @NotEmpty(messageResId = R.string.campo_obrigatorio)
    EditText edtValor;
    Saida saida;
    AlertDialog alerta;
    Validator validator;
    TextView lblNumCard, lblHolder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trasnferencia_cartao);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        validator = new Validator(this);
        validator.setValidationListener(this);
        lblNumCard = findViewById(R.id.lblNumCard);
        lblHolder = findViewById(R.id.lblHolder);
        proxy = getIntent().getStringExtra("proxy");
        holder = getIntent().getStringExtra("holder");
        numero_cartao = getIntent().getStringExtra("numCard");

        lblHolder.setText(holder);
        lblNumCard.setText(numero_cartao);

        btnCadastrar = findViewById(R.id.btnCadastrar);

        edtValor = (EditText)findViewById(R.id.edtValor);
        edtValor.addTextChangedListener(new MoneyTextWatcher(edtValor));

        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validator.validate();
            }
        });
    }

    Action recarregarCartao = new Action() {
        ResultServer resultServer;
        @Override
        public void onPostExecute() {
            ResponseServer responseServer = SessionHelper.getInstance().getResponseServer();
            if(responseServer.hasError()){
                new ErrorHelper().showErrorMessage(mCtx, responseServer.getCode());
                SessionHelper.getInstance().setResponseServer(null);
            } else if (resultServer.getSuccess()){
                Utils.showAlertTransferSuccess(App.getUser(getApplicationContext()).getNome(), TransferenciaCartaoActivity.this, TransferenciaCartaoActivity.this);
            }else{
                Toast.makeText(getApplicationContext(), resultServer.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void run() {
            try {
                RestServices rs = new RestServices(mCtx);
                resultServer = rs.recarregarCartao(proxy, saida.getValor());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onValidationSucceeded() {
        final User user = App.getUser(getApplicationContext());
        saida = new Saida();
        saida.setTipo_favorecido(2);
        String valorStr = edtValor.getText().toString().replaceAll("[.]", "");
        valorStr = valorStr.replaceAll(",",".");
        if (valorStr.isEmpty())
            saida.setValor(0.0);
        else
            saida.setValor(Double.valueOf(valorStr));

        if(saida.getValor()>0) {
            ActionTask action = new ActionTask(TransferenciaCartaoActivity.this, recarregarCartao, "Aguarde...");
            action.execute();

        }else{
            Toast.makeText(getApplicationContext(),R.string.informe_o_valor_txt,Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

}

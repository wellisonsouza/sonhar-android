package io.sonhar.wallet.model;

/**
 * Created by wellison on 12/07/2018.
 */

public class TipoTransferencia {
    private String id, descricao;

    public String getId() {
        return id;
    }

    public TipoTransferencia(String descricao, String id) {
        this.id = id;
        this.descricao = descricao;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String toString()
    {
        return(descricao);
    }
}
